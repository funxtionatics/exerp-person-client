<?php
/**
 * This file aims to show you how to use this generated package.
 * In addition, the goal is to show which methods are available and the first needed parameter(s)
 * You have to use an associative array such as:
 * - the key must be a constant beginning with WSDL_ from AbstractSoapClientBase class (each generated ServiceType class extends this class)
 * - the value must be the corresponding key value (each option matches a {@link http://www.php.net/manual/en/soapclient.soapclient.php} option)
 * $options = [
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'https://goodlife-test.exerp.com/api/v5/PersonAPI?wsdl',
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_TRACE => true,
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_LOGIN => 'you_secret_login',
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_PASSWORD => 'you_secret_password',
 * ];
 * etc...
 */
require_once __DIR__ . '/vendor/autoload.php';
/**
 * Minimal options
 */
$options = [
    WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'https://goodlife-test.exerp.com/api/v5/PersonAPI?wsdl',
    WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_CLASSMAP => \Exerp\Person\ClassMap::get(),
];
/**
 * Samples for Get ServiceType
 */
$get = new \Exerp\Person\ServiceType\Get($options);
/**
 * Sample call for getCommunicationDetails operation/method
 */
if ($get->getCommunicationDetails(new \Exerp\Person\StructType\ApiPersonKey()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getPersonPreferredCenters operation/method
 */
if ($get->getPersonPreferredCenters(new \Exerp\Person\StructType\ApiPersonKey()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getPersonRelations operation/method
 */
if ($get->getPersonRelations(new \Exerp\Person\StructType\PersonRelationParameters()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getTypeAndStatus operation/method
 */
if ($get->getTypeAndStatus(new \Exerp\Person\StructType\ApiPersonKey()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getUnsignedJournalNoteDocuments operation/method
 */
if ($get->getUnsignedJournalNoteDocuments(new \Exerp\Person\StructType\ApiPersonKey()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getPassportCountries operation/method
 */
if ($get->getPassportCountries() !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getPersonDetail operation/method
 */
if ($get->getPersonDetail(new \Exerp\Person\StructType\ApiPersonKey()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getAvailableLanguagesForMembers operation/method
 */
if ($get->getAvailableLanguagesForMembers(new \Exerp\Person\StructType\GetAvailableLanguagesForMembersParameters()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getExtendedAttributeText operation/method
 */
if ($get->getExtendedAttributeText(new \Exerp\Person\StructType\ApiPersonKey(), $attributeName) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getAvailableSalutations operation/method
 */
if ($get->getAvailableSalutations(new \Exerp\Person\StructType\GetAvailableSalutationsParameters()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getPersonDetailByLogin operation/method
 */
if ($get->getPersonDetailByLogin($email, $password) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getJournalNotes operation/method
 */
if ($get->getJournalNotes(new \Exerp\Person\StructType\JournalNoteParameters()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getScope operation/method
 */
if ($get->getScope($scopeType, $scopeId) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getJournalNoteDocument operation/method
 */
if ($get->getJournalNoteDocument($journalNoteId) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getCurrentPersonId operation/method
 */
if ($get->getCurrentPersonId(new \Exerp\Person\StructType\ApiPersonKey()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getCitiesForZipcode operation/method
 */
if ($get->getCitiesForZipcode($countryId, $zipCode) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getDetails operation/method
 */
if ($get->getDetails(new \Exerp\Person\StructType\ApiPersonKey()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getExtendedAttributeMIME operation/method
 */
if ($get->getExtendedAttributeMIME(new \Exerp\Person\StructType\ApiPersonKey(), $attributeName) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Samples for Update ServiceType
 */
$update = new \Exerp\Person\ServiceType\Update($options);
/**
 * Sample call for updateDetails operation/method
 */
if ($update->updateDetails(new \Exerp\Person\StructType\Person()) !== false) {
    print_r($update->getResult());
} else {
    print_r($update->getLastError());
}
/**
 * Sample call for updateExtendedAttributeMIME operation/method
 */
if ($update->updateExtendedAttributeMIME(new \Exerp\Person\StructType\ApiPersonKey(), $attributeName, new \Exerp\Person\StructType\MimeDocument()) !== false) {
    print_r($update->getResult());
} else {
    print_r($update->getLastError());
}
/**
 * Sample call for updateExtendedAttributeText operation/method
 */
if ($update->updateExtendedAttributeText(new \Exerp\Person\StructType\ApiPersonKey(), $attributeName, $value) !== false) {
    print_r($update->getResult());
} else {
    print_r($update->getLastError());
}
/**
 * Sample call for updateCommunicationDetails operation/method
 */
if ($update->updateCommunicationDetails(new \Exerp\Person\StructType\PersonCommunication()) !== false) {
    print_r($update->getResult());
} else {
    print_r($update->getLastError());
}
/**
 * Samples for Remove ServiceType
 */
$remove = new \Exerp\Person\ServiceType\Remove($options);
/**
 * Sample call for removePersonPreferredCenter operation/method
 */
if ($remove->removePersonPreferredCenter(new \Exerp\Person\StructType\ApiPersonKey(), $centerKey) !== false) {
    print_r($remove->getResult());
} else {
    print_r($remove->getLastError());
}
/**
 * Sample call for removePersonReferrer operation/method
 */
if ($remove->removePersonReferrer(new \Exerp\Person\StructType\ApiPersonKey()) !== false) {
    print_r($remove->getResult());
} else {
    print_r($remove->getLastError());
}
/**
 * Samples for Create ServiceType
 */
$create = new \Exerp\Person\ServiceType\Create($options);
/**
 * Sample call for createToDo operation/method
 */
if ($create->createToDo(new \Exerp\Person\StructType\CreateToDoParameter()) !== false) {
    print_r($create->getResult());
} else {
    print_r($create->getLastError());
}
/**
 * Sample call for createCustomJournalDocument operation/method
 */
if ($create->createCustomJournalDocument(new \Exerp\Person\StructType\CreateCustomJournalDocumentParameters()) !== false) {
    print_r($create->getResult());
} else {
    print_r($create->getLastError());
}
/**
 * Sample call for createDoctorNote operation/method
 */
if ($create->createDoctorNote(new \Exerp\Person\StructType\ApiPersonKey(), new \Exerp\Person\StructType\JournalNote()) !== false) {
    print_r($create->getResult());
} else {
    print_r($create->getLastError());
}
/**
 * Sample call for createLead operation/method
 */
if ($create->createLead(new \Exerp\Person\StructType\CreateLeadParameters()) !== false) {
    print_r($create->getResult());
} else {
    print_r($create->getLastError());
}
/**
 * Sample call for createJournalNote operation/method
 */
if ($create->createJournalNote(new \Exerp\Person\StructType\ApiPersonKey(), new \Exerp\Person\StructType\JournalNote()) !== false) {
    print_r($create->getResult());
} else {
    print_r($create->getLastError());
}
/**
 * Sample call for createPerson operation/method
 */
if ($create->createPerson($centerId, new \Exerp\Person\StructType\CreatePersonDetails()) !== false) {
    print_r($create->getResult());
} else {
    print_r($create->getLastError());
}
/**
 * Samples for Send ServiceType
 */
$send = new \Exerp\Person\ServiceType\Send($options);
/**
 * Sample call for sendPasswordToken operation/method
 */
if ($send->sendPasswordToken(new \Exerp\Person\StructType\ApiPersonKey()) !== false) {
    print_r($send->getResult());
} else {
    print_r($send->getLastError());
}
/**
 * Sample call for sendPassword operation/method
 */
if ($send->sendPassword(new \Exerp\Person\StructType\ApiPersonKey()) !== false) {
    print_r($send->getResult());
} else {
    print_r($send->getLastError());
}
/**
 * Samples for Delete ServiceType
 */
$delete = new \Exerp\Person\ServiceType\Delete($options);
/**
 * Sample call for deleteExtendedAttribute operation/method
 */
if ($delete->deleteExtendedAttribute(new \Exerp\Person\StructType\ApiPersonKey(), $attributeName) !== false) {
    print_r($delete->getResult());
} else {
    print_r($delete->getLastError());
}
/**
 * Samples for Override ServiceType
 */
$override = new \Exerp\Person\ServiceType\Override($options);
/**
 * Sample call for overridePartnerBenefitActivationAuthorizationCode
 * operation/method
 */
if ($override->overridePartnerBenefitActivationAuthorizationCode(new \Exerp\Person\StructType\OverridePartnerBenefitActivationAuthorizationCodeParameters()) !== false) {
    print_r($override->getResult());
} else {
    print_r($override->getLastError());
}
/**
 * Samples for Transfer ServiceType
 */
$transfer = new \Exerp\Person\ServiceType\Transfer($options);
/**
 * Sample call for transferPerson operation/method
 */
if ($transfer->transferPerson(new \Exerp\Person\StructType\TransferPersonParameters()) !== false) {
    print_r($transfer->getResult());
} else {
    print_r($transfer->getLastError());
}
/**
 * Samples for Change ServiceType
 */
$change = new \Exerp\Person\ServiceType\Change($options);
/**
 * Sample call for changePasswordWithToken operation/method
 */
if ($change->changePasswordWithToken(new \Exerp\Person\StructType\ApiPersonKey(), $newPassword, $token) !== false) {
    print_r($change->getResult());
} else {
    print_r($change->getLastError());
}
/**
 * Sample call for changeSuspensionStatus operation/method
 */
if ($change->changeSuspensionStatus(new \Exerp\Person\StructType\ChangeSuspensionStatusParameters()) !== false) {
    print_r($change->getResult());
} else {
    print_r($change->getLastError());
}
/**
 * Sample call for changePersonType operation/method
 */
if ($change->changePersonType(new \Exerp\Person\StructType\ChangePersonTypeParameter()) !== false) {
    print_r($change->getResult());
} else {
    print_r($change->getLastError());
}
/**
 * Samples for Set ServiceType
 */
$set = new \Exerp\Person\ServiceType\Set($options);
/**
 * Sample call for setPersonReferrer operation/method
 */
if ($set->setPersonReferrer(new \Exerp\Person\StructType\ApiPersonKey(), new \Exerp\Person\StructType\ApiPersonKey()) !== false) {
    print_r($set->getResult());
} else {
    print_r($set->getLastError());
}
/**
 * Samples for Add ServiceType
 */
$add = new \Exerp\Person\ServiceType\Add($options);
/**
 * Sample call for addSignedDocumentToJournalNote operation/method
 */
if ($add->addSignedDocumentToJournalNote(new \Exerp\Person\StructType\AddSignedDocumentToJournalNoteParameters()) !== false) {
    print_r($add->getResult());
} else {
    print_r($add->getLastError());
}
/**
 * Sample call for addPersonPreferredCenter operation/method
 */
if ($add->addPersonPreferredCenter(new \Exerp\Person\StructType\ApiPersonKey(), $centerKey) !== false) {
    print_r($add->getResult());
} else {
    print_r($add->getLastError());
}
/**
 * Samples for Find ServiceType
 */
$find = new \Exerp\Person\ServiceType\Find($options);
/**
 * Sample call for findPersons operation/method
 */
if ($find->findPersons(new \Exerp\Person\StructType\FindPerson()) !== false) {
    print_r($find->getResult());
} else {
    print_r($find->getLastError());
}
/**
 * Samples for Validate ServiceType
 */
$validate = new \Exerp\Person\ServiceType\Validate($options);
/**
 * Sample call for validatePhoneNumber operation/method
 */
if ($validate->validatePhoneNumber($countryId, $phoneNumber) !== false) {
    print_r($validate->getResult());
} else {
    print_r($validate->getLastError());
}
/**
 * Sample call for validatePerson operation/method
 */
if ($validate->validatePerson($centerId, new \Exerp\Person\StructType\Person()) !== false) {
    print_r($validate->getResult());
} else {
    print_r($validate->getLastError());
}
