<?php

declare(strict_types=1);

namespace Exerp\Person\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for apiGender EnumType
 * @subpackage Enumerations
 */
class ApiGender extends AbstractStructEnumBase
{
    /**
     * Constant for value 'MALE'
     * @return string 'MALE'
     */
    const VALUE_MALE = 'MALE';
    /**
     * Constant for value 'FEMALE'
     * @return string 'FEMALE'
     */
    const VALUE_FEMALE = 'FEMALE';
    /**
     * Constant for value 'UNDEFINED'
     * @return string 'UNDEFINED'
     */
    const VALUE_UNDEFINED = 'UNDEFINED';
    /**
     * Return allowed values
     * @uses self::VALUE_MALE
     * @uses self::VALUE_FEMALE
     * @uses self::VALUE_UNDEFINED
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_MALE,
            self::VALUE_FEMALE,
            self::VALUE_UNDEFINED,
        ];
    }
}
