<?php

declare(strict_types=1);

namespace Exerp\Person\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for personBlacklistedStatus EnumType
 * @subpackage Enumerations
 */
class PersonBlacklistedStatus extends AbstractStructEnumBase
{
    /**
     * Constant for value 'None'
     * @return string 'None'
     */
    const VALUE_NONE = 'None';
    /**
     * Constant for value 'Blacklisted'
     * @return string 'Blacklisted'
     */
    const VALUE_BLACKLISTED = 'Blacklisted';
    /**
     * Constant for value 'Suspended'
     * @return string 'Suspended'
     */
    const VALUE_SUSPENDED = 'Suspended';
    /**
     * Constant for value 'Blocked'
     * @return string 'Blocked'
     */
    const VALUE_BLOCKED = 'Blocked';
    /**
     * Return allowed values
     * @uses self::VALUE_NONE
     * @uses self::VALUE_BLACKLISTED
     * @uses self::VALUE_SUSPENDED
     * @uses self::VALUE_BLOCKED
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_NONE,
            self::VALUE_BLACKLISTED,
            self::VALUE_SUSPENDED,
            self::VALUE_BLOCKED,
        ];
    }
}
