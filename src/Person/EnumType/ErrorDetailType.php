<?php

declare(strict_types=1);

namespace Exerp\Person\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for errorDetailType EnumType
 * @subpackage Enumerations
 */
class ErrorDetailType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'MISSING_ACCESS_GROUP'
     * @return string 'MISSING_ACCESS_GROUP'
     */
    const VALUE_MISSING_ACCESS_GROUP = 'MISSING_ACCESS_GROUP';
    /**
     * Constant for value 'EARLIEST_ALLOWED_BOOKING_DATE'
     * @return string 'EARLIEST_ALLOWED_BOOKING_DATE'
     */
    const VALUE_EARLIEST_ALLOWED_BOOKING_DATE = 'EARLIEST_ALLOWED_BOOKING_DATE';
    /**
     * Constant for value 'EARLIEST_ALLOWED_BOOKING_TIME'
     * @return string 'EARLIEST_ALLOWED_BOOKING_TIME'
     */
    const VALUE_EARLIEST_ALLOWED_BOOKING_TIME = 'EARLIEST_ALLOWED_BOOKING_TIME';
    /**
     * Constant for value 'MISSING_PRIVILEGES_COUNT'
     * @return string 'MISSING_PRIVILEGES_COUNT'
     */
    const VALUE_MISSING_PRIVILEGES_COUNT = 'MISSING_PRIVILEGES_COUNT';
    /**
     * Constant for value 'DEDUCTION_DAY_VALIDATION_FAILED'
     * @return string 'DEDUCTION_DAY_VALIDATION_FAILED'
     */
    const VALUE_DEDUCTION_DAY_VALIDATION_FAILED = 'DEDUCTION_DAY_VALIDATION_FAILED';
    /**
     * Constant for value 'ALLOWED_BOOKING_WINDOW_TIMESLOT'
     * @return string 'ALLOWED_BOOKING_WINDOW_TIMESLOT'
     */
    const VALUE_ALLOWED_BOOKING_WINDOW_TIMESLOT = 'ALLOWED_BOOKING_WINDOW_TIMESLOT';
    /**
     * Return allowed values
     * @uses self::VALUE_MISSING_ACCESS_GROUP
     * @uses self::VALUE_EARLIEST_ALLOWED_BOOKING_DATE
     * @uses self::VALUE_EARLIEST_ALLOWED_BOOKING_TIME
     * @uses self::VALUE_MISSING_PRIVILEGES_COUNT
     * @uses self::VALUE_DEDUCTION_DAY_VALIDATION_FAILED
     * @uses self::VALUE_ALLOWED_BOOKING_WINDOW_TIMESLOT
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_MISSING_ACCESS_GROUP,
            self::VALUE_EARLIEST_ALLOWED_BOOKING_DATE,
            self::VALUE_EARLIEST_ALLOWED_BOOKING_TIME,
            self::VALUE_MISSING_PRIVILEGES_COUNT,
            self::VALUE_DEDUCTION_DAY_VALIDATION_FAILED,
            self::VALUE_ALLOWED_BOOKING_WINDOW_TIMESLOT,
        ];
    }
}
