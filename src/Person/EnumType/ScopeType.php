<?php

declare(strict_types=1);

namespace Exerp\Person\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for scopeType EnumType
 * @subpackage Enumerations
 */
class ScopeType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'Global'
     * @return string 'Global'
     */
    const VALUE_GLOBAL = 'Global';
    /**
     * Constant for value 'Tree'
     * @return string 'Tree'
     */
    const VALUE_TREE = 'Tree';
    /**
     * Constant for value 'Area'
     * @return string 'Area'
     */
    const VALUE_AREA = 'Area';
    /**
     * Constant for value 'Center'
     * @return string 'Center'
     */
    const VALUE_CENTER = 'Center';
    /**
     * Return allowed values
     * @uses self::VALUE_GLOBAL
     * @uses self::VALUE_TREE
     * @uses self::VALUE_AREA
     * @uses self::VALUE_CENTER
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_GLOBAL,
            self::VALUE_TREE,
            self::VALUE_AREA,
            self::VALUE_CENTER,
        ];
    }
}
