<?php

declare(strict_types=1);

namespace Exerp\Person\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for sponsorshipType EnumType
 * @subpackage Enumerations
 */
class SponsorshipType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'NONE'
     * @return string 'NONE'
     */
    const VALUE_NONE = 'NONE';
    /**
     * Constant for value 'FULL'
     * @return string 'FULL'
     */
    const VALUE_FULL = 'FULL';
    /**
     * Constant for value 'FIXED'
     * @return string 'FIXED'
     */
    const VALUE_FIXED = 'FIXED';
    /**
     * Constant for value 'PERCENTAGE'
     * @return string 'PERCENTAGE'
     */
    const VALUE_PERCENTAGE = 'PERCENTAGE';
    /**
     * Return allowed values
     * @uses self::VALUE_NONE
     * @uses self::VALUE_FULL
     * @uses self::VALUE_FIXED
     * @uses self::VALUE_PERCENTAGE
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_NONE,
            self::VALUE_FULL,
            self::VALUE_FIXED,
            self::VALUE_PERCENTAGE,
        ];
    }
}
