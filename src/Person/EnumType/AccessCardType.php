<?php

declare(strict_types=1);

namespace Exerp\Person\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for accessCardType EnumType
 * @subpackage Enumerations
 */
class AccessCardType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'BARCODE'
     * @return string 'BARCODE'
     */
    const VALUE_BARCODE = 'BARCODE';
    /**
     * Constant for value 'MAGNETICCARD'
     * @return string 'MAGNETICCARD'
     */
    const VALUE_MAGNETICCARD = 'MAGNETICCARD';
    /**
     * Constant for value 'RFCARD'
     * @return string 'RFCARD'
     */
    const VALUE_RFCARD = 'RFCARD';
    /**
     * Constant for value 'PIN'
     * @return string 'PIN'
     */
    const VALUE_PIN = 'PIN';
    /**
     * Constant for value 'ANTIDROWN'
     * @return string 'ANTIDROWN'
     */
    const VALUE_ANTIDROWN = 'ANTIDROWN';
    /**
     * Constant for value 'QRCODE'
     * @return string 'QRCODE'
     */
    const VALUE_QRCODE = 'QRCODE';
    /**
     * Constant for value 'EXTERNAL_SYSTEM'
     * @return string 'EXTERNAL_SYSTEM'
     */
    const VALUE_EXTERNAL_SYSTEM = 'EXTERNAL_SYSTEM';
    /**
     * Constant for value 'APPLE_PASS'
     * @return string 'APPLE_PASS'
     */
    const VALUE_APPLE_PASS = 'APPLE_PASS';
    /**
     * Return allowed values
     * @uses self::VALUE_BARCODE
     * @uses self::VALUE_MAGNETICCARD
     * @uses self::VALUE_RFCARD
     * @uses self::VALUE_PIN
     * @uses self::VALUE_ANTIDROWN
     * @uses self::VALUE_QRCODE
     * @uses self::VALUE_EXTERNAL_SYSTEM
     * @uses self::VALUE_APPLE_PASS
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_BARCODE,
            self::VALUE_MAGNETICCARD,
            self::VALUE_RFCARD,
            self::VALUE_PIN,
            self::VALUE_ANTIDROWN,
            self::VALUE_QRCODE,
            self::VALUE_EXTERNAL_SYSTEM,
            self::VALUE_APPLE_PASS,
        ];
    }
}
