<?php

declare(strict_types=1);

namespace Exerp\Person\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for personType EnumType
 * @subpackage Enumerations
 */
class PersonType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'PRIVATE'
     * @return string 'PRIVATE'
     */
    const VALUE_PRIVATE = 'PRIVATE';
    /**
     * Constant for value 'STUDENT'
     * @return string 'STUDENT'
     */
    const VALUE_STUDENT = 'STUDENT';
    /**
     * Constant for value 'STAFF'
     * @return string 'STAFF'
     */
    const VALUE_STAFF = 'STAFF';
    /**
     * Constant for value 'FRIEND'
     * @return string 'FRIEND'
     */
    const VALUE_FRIEND = 'FRIEND';
    /**
     * Constant for value 'CORPORATE'
     * @return string 'CORPORATE'
     */
    const VALUE_CORPORATE = 'CORPORATE';
    /**
     * Constant for value 'ONEMANCORPORATE'
     * @return string 'ONEMANCORPORATE'
     */
    const VALUE_ONEMANCORPORATE = 'ONEMANCORPORATE';
    /**
     * Constant for value 'FAMILY'
     * @return string 'FAMILY'
     */
    const VALUE_FAMILY = 'FAMILY';
    /**
     * Constant for value 'SENIOR'
     * @return string 'SENIOR'
     */
    const VALUE_SENIOR = 'SENIOR';
    /**
     * Constant for value 'GUEST'
     * @return string 'GUEST'
     */
    const VALUE_GUEST = 'GUEST';
    /**
     * Constant for value 'CHILD'
     * @return string 'CHILD'
     */
    const VALUE_CHILD = 'CHILD';
    /**
     * Constant for value 'EXTERNAL_STAFF'
     * @return string 'EXTERNAL_STAFF'
     */
    const VALUE_EXTERNAL_STAFF = 'EXTERNAL_STAFF';
    /**
     * Return allowed values
     * @uses self::VALUE_PRIVATE
     * @uses self::VALUE_STUDENT
     * @uses self::VALUE_STAFF
     * @uses self::VALUE_FRIEND
     * @uses self::VALUE_CORPORATE
     * @uses self::VALUE_ONEMANCORPORATE
     * @uses self::VALUE_FAMILY
     * @uses self::VALUE_SENIOR
     * @uses self::VALUE_GUEST
     * @uses self::VALUE_CHILD
     * @uses self::VALUE_EXTERNAL_STAFF
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_PRIVATE,
            self::VALUE_STUDENT,
            self::VALUE_STAFF,
            self::VALUE_FRIEND,
            self::VALUE_CORPORATE,
            self::VALUE_ONEMANCORPORATE,
            self::VALUE_FAMILY,
            self::VALUE_SENIOR,
            self::VALUE_GUEST,
            self::VALUE_CHILD,
            self::VALUE_EXTERNAL_STAFF,
        ];
    }
}
