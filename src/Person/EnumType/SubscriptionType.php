<?php

declare(strict_types=1);

namespace Exerp\Person\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for subscriptionType EnumType
 * @subpackage Enumerations
 */
class SubscriptionType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'CASH'
     * @return string 'CASH'
     */
    const VALUE_CASH = 'CASH';
    /**
     * Constant for value 'EFT'
     * @return string 'EFT'
     */
    const VALUE_EFT = 'EFT';
    /**
     * Constant for value 'RECURRING_CLIPCARD'
     * @return string 'RECURRING_CLIPCARD'
     */
    const VALUE_RECURRING_CLIPCARD = 'RECURRING_CLIPCARD';
    /**
     * Constant for value 'COURSE'
     * @return string 'COURSE'
     */
    const VALUE_COURSE = 'COURSE';
    /**
     * Return allowed values
     * @uses self::VALUE_CASH
     * @uses self::VALUE_EFT
     * @uses self::VALUE_RECURRING_CLIPCARD
     * @uses self::VALUE_COURSE
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_CASH,
            self::VALUE_EFT,
            self::VALUE_RECURRING_CLIPCARD,
            self::VALUE_COURSE,
        ];
    }
}
