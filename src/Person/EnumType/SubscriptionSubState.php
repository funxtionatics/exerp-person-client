<?php

declare(strict_types=1);

namespace Exerp\Person\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for subscriptionSubState EnumType
 * @subpackage Enumerations
 */
class SubscriptionSubState extends AbstractStructEnumBase
{
    /**
     * Constant for value 'NONE'
     * @return string 'NONE'
     */
    const VALUE_NONE = 'NONE';
    /**
     * Constant for value 'AWAITING_ACTIVATION'
     * @return string 'AWAITING_ACTIVATION'
     */
    const VALUE_AWAITING_ACTIVATION = 'AWAITING_ACTIVATION';
    /**
     * Constant for value 'UPGRADED'
     * @return string 'UPGRADED'
     */
    const VALUE_UPGRADED = 'UPGRADED';
    /**
     * Constant for value 'DOWNGRADED'
     * @return string 'DOWNGRADED'
     */
    const VALUE_DOWNGRADED = 'DOWNGRADED';
    /**
     * Constant for value 'EXTENDED'
     * @return string 'EXTENDED'
     */
    const VALUE_EXTENDED = 'EXTENDED';
    /**
     * Constant for value 'TRANSFERRED'
     * @return string 'TRANSFERRED'
     */
    const VALUE_TRANSFERRED = 'TRANSFERRED';
    /**
     * Constant for value 'REGRETTED'
     * @return string 'REGRETTED'
     */
    const VALUE_REGRETTED = 'REGRETTED';
    /**
     * Constant for value 'CANCELLED'
     * @return string 'CANCELLED'
     */
    const VALUE_CANCELLED = 'CANCELLED';
    /**
     * Constant for value 'BLOCKED'
     * @return string 'BLOCKED'
     */
    const VALUE_BLOCKED = 'BLOCKED';
    /**
     * Constant for value 'CHANGED'
     * @return string 'CHANGED'
     */
    const VALUE_CHANGED = 'CHANGED';
    /**
     * Return allowed values
     * @uses self::VALUE_NONE
     * @uses self::VALUE_AWAITING_ACTIVATION
     * @uses self::VALUE_UPGRADED
     * @uses self::VALUE_DOWNGRADED
     * @uses self::VALUE_EXTENDED
     * @uses self::VALUE_TRANSFERRED
     * @uses self::VALUE_REGRETTED
     * @uses self::VALUE_CANCELLED
     * @uses self::VALUE_BLOCKED
     * @uses self::VALUE_CHANGED
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_NONE,
            self::VALUE_AWAITING_ACTIVATION,
            self::VALUE_UPGRADED,
            self::VALUE_DOWNGRADED,
            self::VALUE_EXTENDED,
            self::VALUE_TRANSFERRED,
            self::VALUE_REGRETTED,
            self::VALUE_CANCELLED,
            self::VALUE_BLOCKED,
            self::VALUE_CHANGED,
        ];
    }
}
