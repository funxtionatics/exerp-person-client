<?php

declare(strict_types=1);

namespace Exerp\Person\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for quickChannel EnumType
 * @subpackage Enumerations
 */
class QuickChannel extends AbstractStructEnumBase
{
    /**
     * Constant for value 'NONE'
     * @return string 'NONE'
     */
    const VALUE_NONE = 'NONE';
    /**
     * Constant for value 'EMAIL'
     * @return string 'EMAIL'
     */
    const VALUE_EMAIL = 'EMAIL';
    /**
     * Constant for value 'SMS'
     * @return string 'SMS'
     */
    const VALUE_SMS = 'SMS';
    /**
     * Return allowed values
     * @uses self::VALUE_NONE
     * @uses self::VALUE_EMAIL
     * @uses self::VALUE_SMS
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_NONE,
            self::VALUE_EMAIL,
            self::VALUE_SMS,
        ];
    }
}
