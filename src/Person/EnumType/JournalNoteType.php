<?php

declare(strict_types=1);

namespace Exerp\Person\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for journalNoteType EnumType
 * @subpackage Enumerations
 */
class JournalNoteType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'SYSTEM'
     * @return string 'SYSTEM'
     */
    const VALUE_SYSTEM = 'SYSTEM';
    /**
     * Constant for value 'SUBSCRIPTION_CONTRACT'
     * @return string 'SUBSCRIPTION_CONTRACT'
     */
    const VALUE_SUBSCRIPTION_CONTRACT = 'SUBSCRIPTION_CONTRACT';
    /**
     * Constant for value 'DOCUMENTATION'
     * @return string 'DOCUMENTATION'
     */
    const VALUE_DOCUMENTATION = 'DOCUMENTATION';
    /**
     * Constant for value 'NOTE'
     * @return string 'NOTE'
     */
    const VALUE_NOTE = 'NOTE';
    /**
     * Constant for value 'STATUS'
     * @return string 'STATUS'
     */
    const VALUE_STATUS = 'STATUS';
    /**
     * Constant for value 'PAYMENT_AGREEMENT_CONTRACT'
     * @return string 'PAYMENT_AGREEMENT_CONTRACT'
     */
    const VALUE_PAYMENT_AGREEMENT_CONTRACT = 'PAYMENT_AGREEMENT_CONTRACT';
    /**
     * Constant for value 'CASHOUT'
     * @return string 'CASHOUT'
     */
    const VALUE_CASHOUT = 'CASHOUT';
    /**
     * Constant for value 'FREEZE_CREATION'
     * @return string 'FREEZE_CREATION'
     */
    const VALUE_FREEZE_CREATION = 'FREEZE_CREATION';
    /**
     * Constant for value 'FREEZE_CANCELLATION'
     * @return string 'FREEZE_CANCELLATION'
     */
    const VALUE_FREEZE_CANCELLATION = 'FREEZE_CANCELLATION';
    /**
     * Constant for value 'FREEZE_CHANGE'
     * @return string 'FREEZE_CHANGE'
     */
    const VALUE_FREEZE_CHANGE = 'FREEZE_CHANGE';
    /**
     * Constant for value 'OTHER_PAYER_START'
     * @return string 'OTHER_PAYER_START'
     */
    const VALUE_OTHER_PAYER_START = 'OTHER_PAYER_START';
    /**
     * Constant for value 'OTHER_PAYER_STOP'
     * @return string 'OTHER_PAYER_STOP'
     */
    const VALUE_OTHER_PAYER_STOP = 'OTHER_PAYER_STOP';
    /**
     * Constant for value 'SUBSCRIPTION_TERMINATION'
     * @return string 'SUBSCRIPTION_TERMINATION'
     */
    const VALUE_SUBSCRIPTION_TERMINATION = 'SUBSCRIPTION_TERMINATION';
    /**
     * Constant for value 'SUBSCRIPTION_TERMINATION_CANCELLATION'
     * @return string 'SUBSCRIPTION_TERMINATION_CANCELLATION'
     */
    const VALUE_SUBSCRIPTION_TERMINATION_CANCELLATION = 'SUBSCRIPTION_TERMINATION_CANCELLATION';
    /**
     * Constant for value 'PAYMENT_NOTE'
     * @return string 'PAYMENT_NOTE'
     */
    const VALUE_PAYMENT_NOTE = 'PAYMENT_NOTE';
    /**
     * Constant for value 'ACCOUNT_PAYMENT_NOTE'
     * @return string 'ACCOUNT_PAYMENT_NOTE'
     */
    const VALUE_ACCOUNT_PAYMENT_NOTE = 'ACCOUNT_PAYMENT_NOTE';
    /**
     * Constant for value 'SAVED_FREE_DAYS_USE'
     * @return string 'SAVED_FREE_DAYS_USE'
     */
    const VALUE_SAVED_FREE_DAYS_USE = 'SAVED_FREE_DAYS_USE';
    /**
     * Constant for value 'FREE_PERIOD_ASSIGNMENT'
     * @return string 'FREE_PERIOD_ASSIGNMENT'
     */
    const VALUE_FREE_PERIOD_ASSIGNMENT = 'FREE_PERIOD_ASSIGNMENT';
    /**
     * Constant for value 'FREE_PERIOD_CANCELLATION'
     * @return string 'FREE_PERIOD_CANCELLATION'
     */
    const VALUE_FREE_PERIOD_CANCELLATION = 'FREE_PERIOD_CANCELLATION';
    /**
     * Constant for value 'CASH_ACCOUNT_CREDIT'
     * @return string 'CASH_ACCOUNT_CREDIT'
     */
    const VALUE_CASH_ACCOUNT_CREDIT = 'CASH_ACCOUNT_CREDIT';
    /**
     * Constant for value 'ADDON_TERMINATION'
     * @return string 'ADDON_TERMINATION'
     */
    const VALUE_ADDON_TERMINATION = 'ADDON_TERMINATION';
    /**
     * Constant for value 'ADDON_TERMINATION_CANCELLATION'
     * @return string 'ADDON_TERMINATION_CANCELLATION'
     */
    const VALUE_ADDON_TERMINATION_CANCELLATION = 'ADDON_TERMINATION_CANCELLATION';
    /**
     * Constant for value 'CHILD_RELATION_CONTRACT'
     * @return string 'CHILD_RELATION_CONTRACT'
     */
    const VALUE_CHILD_RELATION_CONTRACT = 'CHILD_RELATION_CONTRACT';
    /**
     * Constant for value 'DOCTOR_NOTE'
     * @return string 'DOCTOR_NOTE'
     */
    const VALUE_DOCTOR_NOTE = 'DOCTOR_NOTE';
    /**
     * Constant for value 'ADDON_CONTRACT'
     * @return string 'ADDON_CONTRACT'
     */
    const VALUE_ADDON_CONTRACT = 'ADDON_CONTRACT';
    /**
     * Constant for value 'HEALTH_CERTIFICATE'
     * @return string 'HEALTH_CERTIFICATE'
     */
    const VALUE_HEALTH_CERTIFICATE = 'HEALTH_CERTIFICATE';
    /**
     * Constant for value 'CREDITCARD_AGREEMENT_CONTRACT'
     * @return string 'CREDITCARD_AGREEMENT_CONTRACT'
     */
    const VALUE_CREDITCARD_AGREEMENT_CONTRACT = 'CREDITCARD_AGREEMENT_CONTRACT';
    /**
     * Constant for value 'CLIPCARD_BUYOUT'
     * @return string 'CLIPCARD_BUYOUT'
     */
    const VALUE_CLIPCARD_BUYOUT = 'CLIPCARD_BUYOUT';
    /**
     * Constant for value 'CLIPCARD_CONTRACT'
     * @return string 'CLIPCARD_CONTRACT'
     */
    const VALUE_CLIPCARD_CONTRACT = 'CLIPCARD_CONTRACT';
    /**
     * Constant for value 'REASSIGN_SUBSCRIPTION_CONTRACT'
     * @return string 'REASSIGN_SUBSCRIPTION_CONTRACT'
     */
    const VALUE_REASSIGN_SUBSCRIPTION_CONTRACT = 'REASSIGN_SUBSCRIPTION_CONTRACT';
    /**
     * Constant for value 'AGGREGATED_SUBSCRIPTION_CONTRACT'
     * @return string 'AGGREGATED_SUBSCRIPTION_CONTRACT'
     */
    const VALUE_AGGREGATED_SUBSCRIPTION_CONTRACT = 'AGGREGATED_SUBSCRIPTION_CONTRACT';
    /**
     * Constant for value 'FREE_PERIOD_STOP_DATE_CHANGE'
     * @return string 'FREE_PERIOD_STOP_DATE_CHANGE'
     */
    const VALUE_FREE_PERIOD_STOP_DATE_CHANGE = 'FREE_PERIOD_STOP_DATE_CHANGE';
    /**
     * Constant for value 'QUESTIONNAIRE_COMPLETED'
     * @return string 'QUESTIONNAIRE_COMPLETED'
     */
    const VALUE_QUESTIONNAIRE_COMPLETED = 'QUESTIONNAIRE_COMPLETED';
    /**
     * Constant for value 'CUSTOM_JOURNAL_DOCUMENT_TYPE'
     * @return string 'CUSTOM_JOURNAL_DOCUMENT_TYPE'
     */
    const VALUE_CUSTOM_JOURNAL_DOCUMENT_TYPE = 'CUSTOM_JOURNAL_DOCUMENT_TYPE';
    /**
     * Constant for value 'MULTIPLE_CLIPCARD_BUYOUT'
     * @return string 'MULTIPLE_CLIPCARD_BUYOUT'
     */
    const VALUE_MULTIPLE_CLIPCARD_BUYOUT = 'MULTIPLE_CLIPCARD_BUYOUT';
    /**
     * Constant for value 'SAVED_FREE_DAYS_ADDED'
     * @return string 'SAVED_FREE_DAYS_ADDED'
     */
    const VALUE_SAVED_FREE_DAYS_ADDED = 'SAVED_FREE_DAYS_ADDED';
    /**
     * Constant for value 'SUBSCRIPTION_REASSIGNED'
     * @return string 'SUBSCRIPTION_REASSIGNED'
     */
    const VALUE_SUBSCRIPTION_REASSIGNED = 'SUBSCRIPTION_REASSIGNED';
    /**
     * Return allowed values
     * @uses self::VALUE_SYSTEM
     * @uses self::VALUE_SUBSCRIPTION_CONTRACT
     * @uses self::VALUE_DOCUMENTATION
     * @uses self::VALUE_NOTE
     * @uses self::VALUE_STATUS
     * @uses self::VALUE_PAYMENT_AGREEMENT_CONTRACT
     * @uses self::VALUE_CASHOUT
     * @uses self::VALUE_FREEZE_CREATION
     * @uses self::VALUE_FREEZE_CANCELLATION
     * @uses self::VALUE_FREEZE_CHANGE
     * @uses self::VALUE_OTHER_PAYER_START
     * @uses self::VALUE_OTHER_PAYER_STOP
     * @uses self::VALUE_SUBSCRIPTION_TERMINATION
     * @uses self::VALUE_SUBSCRIPTION_TERMINATION_CANCELLATION
     * @uses self::VALUE_PAYMENT_NOTE
     * @uses self::VALUE_ACCOUNT_PAYMENT_NOTE
     * @uses self::VALUE_SAVED_FREE_DAYS_USE
     * @uses self::VALUE_FREE_PERIOD_ASSIGNMENT
     * @uses self::VALUE_FREE_PERIOD_CANCELLATION
     * @uses self::VALUE_CASH_ACCOUNT_CREDIT
     * @uses self::VALUE_ADDON_TERMINATION
     * @uses self::VALUE_ADDON_TERMINATION_CANCELLATION
     * @uses self::VALUE_CHILD_RELATION_CONTRACT
     * @uses self::VALUE_DOCTOR_NOTE
     * @uses self::VALUE_ADDON_CONTRACT
     * @uses self::VALUE_HEALTH_CERTIFICATE
     * @uses self::VALUE_CREDITCARD_AGREEMENT_CONTRACT
     * @uses self::VALUE_CLIPCARD_BUYOUT
     * @uses self::VALUE_CLIPCARD_CONTRACT
     * @uses self::VALUE_REASSIGN_SUBSCRIPTION_CONTRACT
     * @uses self::VALUE_AGGREGATED_SUBSCRIPTION_CONTRACT
     * @uses self::VALUE_FREE_PERIOD_STOP_DATE_CHANGE
     * @uses self::VALUE_QUESTIONNAIRE_COMPLETED
     * @uses self::VALUE_CUSTOM_JOURNAL_DOCUMENT_TYPE
     * @uses self::VALUE_MULTIPLE_CLIPCARD_BUYOUT
     * @uses self::VALUE_SAVED_FREE_DAYS_ADDED
     * @uses self::VALUE_SUBSCRIPTION_REASSIGNED
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_SYSTEM,
            self::VALUE_SUBSCRIPTION_CONTRACT,
            self::VALUE_DOCUMENTATION,
            self::VALUE_NOTE,
            self::VALUE_STATUS,
            self::VALUE_PAYMENT_AGREEMENT_CONTRACT,
            self::VALUE_CASHOUT,
            self::VALUE_FREEZE_CREATION,
            self::VALUE_FREEZE_CANCELLATION,
            self::VALUE_FREEZE_CHANGE,
            self::VALUE_OTHER_PAYER_START,
            self::VALUE_OTHER_PAYER_STOP,
            self::VALUE_SUBSCRIPTION_TERMINATION,
            self::VALUE_SUBSCRIPTION_TERMINATION_CANCELLATION,
            self::VALUE_PAYMENT_NOTE,
            self::VALUE_ACCOUNT_PAYMENT_NOTE,
            self::VALUE_SAVED_FREE_DAYS_USE,
            self::VALUE_FREE_PERIOD_ASSIGNMENT,
            self::VALUE_FREE_PERIOD_CANCELLATION,
            self::VALUE_CASH_ACCOUNT_CREDIT,
            self::VALUE_ADDON_TERMINATION,
            self::VALUE_ADDON_TERMINATION_CANCELLATION,
            self::VALUE_CHILD_RELATION_CONTRACT,
            self::VALUE_DOCTOR_NOTE,
            self::VALUE_ADDON_CONTRACT,
            self::VALUE_HEALTH_CERTIFICATE,
            self::VALUE_CREDITCARD_AGREEMENT_CONTRACT,
            self::VALUE_CLIPCARD_BUYOUT,
            self::VALUE_CLIPCARD_CONTRACT,
            self::VALUE_REASSIGN_SUBSCRIPTION_CONTRACT,
            self::VALUE_AGGREGATED_SUBSCRIPTION_CONTRACT,
            self::VALUE_FREE_PERIOD_STOP_DATE_CHANGE,
            self::VALUE_QUESTIONNAIRE_COMPLETED,
            self::VALUE_CUSTOM_JOURNAL_DOCUMENT_TYPE,
            self::VALUE_MULTIPLE_CLIPCARD_BUYOUT,
            self::VALUE_SAVED_FREE_DAYS_ADDED,
            self::VALUE_SUBSCRIPTION_REASSIGNED,
        ];
    }
}
