<?php

declare(strict_types=1);

namespace Exerp\Person\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for companyAgreementStateType EnumType
 * @subpackage Enumerations
 */
class CompanyAgreementStateType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'UNDERTARGET'
     * @return string 'UNDERTARGET'
     */
    const VALUE_UNDERTARGET = 'UNDERTARGET';
    /**
     * Constant for value 'ACTIVE'
     * @return string 'ACTIVE'
     */
    const VALUE_ACTIVE = 'ACTIVE';
    /**
     * Constant for value 'STOPNEW'
     * @return string 'STOPNEW'
     */
    const VALUE_STOPNEW = 'STOPNEW';
    /**
     * Constant for value 'OLD'
     * @return string 'OLD'
     */
    const VALUE_OLD = 'OLD';
    /**
     * Constant for value 'AWAITING_ACTIVATION'
     * @return string 'AWAITING_ACTIVATION'
     */
    const VALUE_AWAITING_ACTIVATION = 'AWAITING_ACTIVATION';
    /**
     * Constant for value 'BLOCKED'
     * @return string 'BLOCKED'
     */
    const VALUE_BLOCKED = 'BLOCKED';
    /**
     * Constant for value 'DELETED'
     * @return string 'DELETED'
     */
    const VALUE_DELETED = 'DELETED';
    /**
     * Return allowed values
     * @uses self::VALUE_UNDERTARGET
     * @uses self::VALUE_ACTIVE
     * @uses self::VALUE_STOPNEW
     * @uses self::VALUE_OLD
     * @uses self::VALUE_AWAITING_ACTIVATION
     * @uses self::VALUE_BLOCKED
     * @uses self::VALUE_DELETED
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_UNDERTARGET,
            self::VALUE_ACTIVE,
            self::VALUE_STOPNEW,
            self::VALUE_OLD,
            self::VALUE_AWAITING_ACTIVATION,
            self::VALUE_BLOCKED,
            self::VALUE_DELETED,
        ];
    }
}
