<?php

declare(strict_types=1);

namespace Exerp\Person\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for personStatus EnumType
 * @subpackage Enumerations
 */
class PersonStatus extends AbstractStructEnumBase
{
    /**
     * Constant for value 'LEAD'
     * @return string 'LEAD'
     */
    const VALUE_LEAD = 'LEAD';
    /**
     * Constant for value 'ACTIVE'
     * @return string 'ACTIVE'
     */
    const VALUE_ACTIVE = 'ACTIVE';
    /**
     * Constant for value 'INACTIVE'
     * @return string 'INACTIVE'
     */
    const VALUE_INACTIVE = 'INACTIVE';
    /**
     * Constant for value 'TEMPORARYINACTIVE'
     * @return string 'TEMPORARYINACTIVE'
     */
    const VALUE_TEMPORARYINACTIVE = 'TEMPORARYINACTIVE';
    /**
     * Constant for value 'TRANSFERRED'
     * @return string 'TRANSFERRED'
     */
    const VALUE_TRANSFERRED = 'TRANSFERRED';
    /**
     * Constant for value 'DUPLICATE'
     * @return string 'DUPLICATE'
     */
    const VALUE_DUPLICATE = 'DUPLICATE';
    /**
     * Constant for value 'PROSPECT'
     * @return string 'PROSPECT'
     */
    const VALUE_PROSPECT = 'PROSPECT';
    /**
     * Constant for value 'CONTACT'
     * @return string 'CONTACT'
     */
    const VALUE_CONTACT = 'CONTACT';
    /**
     * Return allowed values
     * @uses self::VALUE_LEAD
     * @uses self::VALUE_ACTIVE
     * @uses self::VALUE_INACTIVE
     * @uses self::VALUE_TEMPORARYINACTIVE
     * @uses self::VALUE_TRANSFERRED
     * @uses self::VALUE_DUPLICATE
     * @uses self::VALUE_PROSPECT
     * @uses self::VALUE_CONTACT
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_LEAD,
            self::VALUE_ACTIVE,
            self::VALUE_INACTIVE,
            self::VALUE_TEMPORARYINACTIVE,
            self::VALUE_TRANSFERRED,
            self::VALUE_DUPLICATE,
            self::VALUE_PROSPECT,
            self::VALUE_CONTACT,
        ];
    }
}
