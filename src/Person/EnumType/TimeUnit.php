<?php

declare(strict_types=1);

namespace Exerp\Person\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for timeUnit EnumType
 * @subpackage Enumerations
 */
class TimeUnit extends AbstractStructEnumBase
{
    /**
     * Constant for value 'WEEK'
     * @return string 'WEEK'
     */
    const VALUE_WEEK = 'WEEK';
    /**
     * Constant for value 'DAY'
     * @return string 'DAY'
     */
    const VALUE_DAY = 'DAY';
    /**
     * Constant for value 'MONTH'
     * @return string 'MONTH'
     */
    const VALUE_MONTH = 'MONTH';
    /**
     * Constant for value 'YEAR'
     * @return string 'YEAR'
     */
    const VALUE_YEAR = 'YEAR';
    /**
     * Constant for value 'HOUR'
     * @return string 'HOUR'
     */
    const VALUE_HOUR = 'HOUR';
    /**
     * Constant for value 'MINUTE'
     * @return string 'MINUTE'
     */
    const VALUE_MINUTE = 'MINUTE';
    /**
     * Constant for value 'SECOND'
     * @return string 'SECOND'
     */
    const VALUE_SECOND = 'SECOND';
    /**
     * Return allowed values
     * @uses self::VALUE_WEEK
     * @uses self::VALUE_DAY
     * @uses self::VALUE_MONTH
     * @uses self::VALUE_YEAR
     * @uses self::VALUE_HOUR
     * @uses self::VALUE_MINUTE
     * @uses self::VALUE_SECOND
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_WEEK,
            self::VALUE_DAY,
            self::VALUE_MONTH,
            self::VALUE_YEAR,
            self::VALUE_HOUR,
            self::VALUE_MINUTE,
            self::VALUE_SECOND,
        ];
    }
}
