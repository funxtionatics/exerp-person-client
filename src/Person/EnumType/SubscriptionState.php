<?php

declare(strict_types=1);

namespace Exerp\Person\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for subscriptionState EnumType
 * @subpackage Enumerations
 */
class SubscriptionState extends AbstractStructEnumBase
{
    /**
     * Constant for value 'ACTIVE'
     * @return string 'ACTIVE'
     */
    const VALUE_ACTIVE = 'ACTIVE';
    /**
     * Constant for value 'ENDED'
     * @return string 'ENDED'
     */
    const VALUE_ENDED = 'ENDED';
    /**
     * Constant for value 'FROZEN'
     * @return string 'FROZEN'
     */
    const VALUE_FROZEN = 'FROZEN';
    /**
     * Constant for value 'WINDOW'
     * @return string 'WINDOW'
     */
    const VALUE_WINDOW = 'WINDOW';
    /**
     * Constant for value 'CREATED'
     * @return string 'CREATED'
     */
    const VALUE_CREATED = 'CREATED';
    /**
     * Return allowed values
     * @uses self::VALUE_ACTIVE
     * @uses self::VALUE_ENDED
     * @uses self::VALUE_FROZEN
     * @uses self::VALUE_WINDOW
     * @uses self::VALUE_CREATED
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_ACTIVE,
            self::VALUE_ENDED,
            self::VALUE_FROZEN,
            self::VALUE_WINDOW,
            self::VALUE_CREATED,
        ];
    }
}
