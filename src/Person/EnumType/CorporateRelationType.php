<?php

declare(strict_types=1);

namespace Exerp\Person\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for corporateRelationType EnumType
 * @subpackage Enumerations
 */
class CorporateRelationType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'EMPLOYEE'
     * @return string 'EMPLOYEE'
     */
    const VALUE_EMPLOYEE = 'EMPLOYEE';
    /**
     * Constant for value 'FAMILY_OF_EMPLOYEE'
     * @return string 'FAMILY_OF_EMPLOYEE'
     */
    const VALUE_FAMILY_OF_EMPLOYEE = 'FAMILY_OF_EMPLOYEE';
    /**
     * Constant for value 'OTHER'
     * @return string 'OTHER'
     */
    const VALUE_OTHER = 'OTHER';
    /**
     * Return allowed values
     * @uses self::VALUE_EMPLOYEE
     * @uses self::VALUE_FAMILY_OF_EMPLOYEE
     * @uses self::VALUE_OTHER
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_EMPLOYEE,
            self::VALUE_FAMILY_OF_EMPLOYEE,
            self::VALUE_OTHER,
        ];
    }
}
