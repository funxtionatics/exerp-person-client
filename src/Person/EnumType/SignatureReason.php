<?php

declare(strict_types=1);

namespace Exerp\Person\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for signatureReason EnumType
 * @subpackage Enumerations
 */
class SignatureReason extends AbstractStructEnumBase
{
    /**
     * Constant for value 'CUSTOMER'
     * @return string 'CUSTOMER'
     */
    const VALUE_CUSTOMER = 'CUSTOMER';
    /**
     * Constant for value 'OTHER_PAYER'
     * @return string 'OTHER_PAYER'
     */
    const VALUE_OTHER_PAYER = 'OTHER_PAYER';
    /**
     * Constant for value 'GUARDIAN'
     * @return string 'GUARDIAN'
     */
    const VALUE_GUARDIAN = 'GUARDIAN';
    /**
     * Constant for value 'EMPLOYEE'
     * @return string 'EMPLOYEE'
     */
    const VALUE_EMPLOYEE = 'EMPLOYEE';
    /**
     * Return allowed values
     * @uses self::VALUE_CUSTOMER
     * @uses self::VALUE_OTHER_PAYER
     * @uses self::VALUE_GUARDIAN
     * @uses self::VALUE_EMPLOYEE
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_CUSTOMER,
            self::VALUE_OTHER_PAYER,
            self::VALUE_GUARDIAN,
            self::VALUE_EMPLOYEE,
        ];
    }
}
