<?php

declare(strict_types=1);

namespace Exerp\Person\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for relationType EnumType
 * @subpackage Enumerations
 */
class RelationType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'MY_FRIEND'
     * @return string 'MY_FRIEND'
     */
    const VALUE_MY_FRIEND = 'MY_FRIEND';
    /**
     * Constant for value 'FRIENDS_OF_ME'
     * @return string 'FRIENDS_OF_ME'
     */
    const VALUE_FRIENDS_OF_ME = 'FRIENDS_OF_ME';
    /**
     * Constant for value 'MY_COMPANY'
     * @return string 'MY_COMPANY'
     */
    const VALUE_MY_COMPANY = 'MY_COMPANY';
    /**
     * Constant for value 'MY_COMPANY_AGREEMENT'
     * @return string 'MY_COMPANY_AGREEMENT'
     */
    const VALUE_MY_COMPANY_AGREEMENT = 'MY_COMPANY_AGREEMENT';
    /**
     * Constant for value 'MY_FAMILY'
     * @return string 'MY_FAMILY'
     */
    const VALUE_MY_FAMILY = 'MY_FAMILY';
    /**
     * Constant for value 'FAMILY_TO_ME'
     * @return string 'FAMILY_TO_ME'
     */
    const VALUE_FAMILY_TO_ME = 'FAMILY_TO_ME';
    /**
     * Constant for value 'MY_COUNSELLOR'
     * @return string 'MY_COUNSELLOR'
     */
    const VALUE_MY_COUNSELLOR = 'MY_COUNSELLOR';
    /**
     * Constant for value 'COUNSELLED_BY_ME'
     * @return string 'COUNSELLED_BY_ME'
     */
    const VALUE_COUNSELLED_BY_ME = 'COUNSELLED_BY_ME';
    /**
     * Constant for value 'MY_PAYER'
     * @return string 'MY_PAYER'
     */
    const VALUE_MY_PAYER = 'MY_PAYER';
    /**
     * Constant for value 'MY_REFERRER'
     * @return string 'MY_REFERRER'
     */
    const VALUE_MY_REFERRER = 'MY_REFERRER';
    /**
     * Constant for value 'REFERRED_BY_ME'
     * @return string 'REFERRED_BY_ME'
     */
    const VALUE_REFERRED_BY_ME = 'REFERRED_BY_ME';
    /**
     * Constant for value 'MY_CHILD'
     * @return string 'MY_CHILD'
     */
    const VALUE_MY_CHILD = 'MY_CHILD';
    /**
     * Constant for value 'MY_PARENT'
     * @return string 'MY_PARENT'
     */
    const VALUE_MY_PARENT = 'MY_PARENT';
    /**
     * Constant for value 'MY_BUDDIES'
     * @return string 'MY_BUDDIES'
     */
    const VALUE_MY_BUDDIES = 'MY_BUDDIES';
    /**
     * Constant for value 'PRIMARY_MEMBER'
     * @return string 'PRIMARY_MEMBER'
     */
    const VALUE_PRIMARY_MEMBER = 'PRIMARY_MEMBER';
    /**
     * Constant for value 'SECONDARY_MEMBER'
     * @return string 'SECONDARY_MEMBER'
     */
    const VALUE_SECONDARY_MEMBER = 'SECONDARY_MEMBER';
    /**
     * Constant for value 'CORPORATE_FAMILY'
     * @return string 'CORPORATE_FAMILY'
     */
    const VALUE_CORPORATE_FAMILY = 'CORPORATE_FAMILY';
    /**
     * Constant for value 'LINKED_CORPORATE_FAMILY'
     * @return string 'LINKED_CORPORATE_FAMILY'
     */
    const VALUE_LINKED_CORPORATE_FAMILY = 'LINKED_CORPORATE_FAMILY';
    /**
     * Constant for value 'LINKED_CORPORATE_EMPLOYEE'
     * @return string 'LINKED_CORPORATE_EMPLOYEE'
     */
    const VALUE_LINKED_CORPORATE_EMPLOYEE = 'LINKED_CORPORATE_EMPLOYEE';
    /**
     * Constant for value 'OTHER'
     * @return string 'OTHER'
     */
    const VALUE_OTHER = 'OTHER';
    /**
     * Constant for value 'PAID_FOR_BY_ME'
     * @return string 'PAID_FOR_BY_ME'
     */
    const VALUE_PAID_FOR_BY_ME = 'PAID_FOR_BY_ME';
    /**
     * Constant for value 'MY_FAMILY_RELATIVE'
     * @return string 'MY_FAMILY_RELATIVE'
     */
    const VALUE_MY_FAMILY_RELATIVE = 'MY_FAMILY_RELATIVE';
    /**
     * Constant for value 'FAMILY_RELATIVE_TO_ME'
     * @return string 'FAMILY_RELATIVE_TO_ME'
     */
    const VALUE_FAMILY_RELATIVE_TO_ME = 'FAMILY_RELATIVE_TO_ME';
    /**
     * Return allowed values
     * @uses self::VALUE_MY_FRIEND
     * @uses self::VALUE_FRIENDS_OF_ME
     * @uses self::VALUE_MY_COMPANY
     * @uses self::VALUE_MY_COMPANY_AGREEMENT
     * @uses self::VALUE_MY_FAMILY
     * @uses self::VALUE_FAMILY_TO_ME
     * @uses self::VALUE_MY_COUNSELLOR
     * @uses self::VALUE_COUNSELLED_BY_ME
     * @uses self::VALUE_MY_PAYER
     * @uses self::VALUE_MY_REFERRER
     * @uses self::VALUE_REFERRED_BY_ME
     * @uses self::VALUE_MY_CHILD
     * @uses self::VALUE_MY_PARENT
     * @uses self::VALUE_MY_BUDDIES
     * @uses self::VALUE_PRIMARY_MEMBER
     * @uses self::VALUE_SECONDARY_MEMBER
     * @uses self::VALUE_CORPORATE_FAMILY
     * @uses self::VALUE_LINKED_CORPORATE_FAMILY
     * @uses self::VALUE_LINKED_CORPORATE_EMPLOYEE
     * @uses self::VALUE_OTHER
     * @uses self::VALUE_PAID_FOR_BY_ME
     * @uses self::VALUE_MY_FAMILY_RELATIVE
     * @uses self::VALUE_FAMILY_RELATIVE_TO_ME
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_MY_FRIEND,
            self::VALUE_FRIENDS_OF_ME,
            self::VALUE_MY_COMPANY,
            self::VALUE_MY_COMPANY_AGREEMENT,
            self::VALUE_MY_FAMILY,
            self::VALUE_FAMILY_TO_ME,
            self::VALUE_MY_COUNSELLOR,
            self::VALUE_COUNSELLED_BY_ME,
            self::VALUE_MY_PAYER,
            self::VALUE_MY_REFERRER,
            self::VALUE_REFERRED_BY_ME,
            self::VALUE_MY_CHILD,
            self::VALUE_MY_PARENT,
            self::VALUE_MY_BUDDIES,
            self::VALUE_PRIMARY_MEMBER,
            self::VALUE_SECONDARY_MEMBER,
            self::VALUE_CORPORATE_FAMILY,
            self::VALUE_LINKED_CORPORATE_FAMILY,
            self::VALUE_LINKED_CORPORATE_EMPLOYEE,
            self::VALUE_OTHER,
            self::VALUE_PAID_FOR_BY_ME,
            self::VALUE_MY_FAMILY_RELATIVE,
            self::VALUE_FAMILY_RELATIVE_TO_ME,
        ];
    }
}
