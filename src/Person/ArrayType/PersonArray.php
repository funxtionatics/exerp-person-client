<?php

declare(strict_types=1);

namespace Exerp\Person\ArrayType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for personArray ArrayType
 * Meta information extracted from the WSDL
 * - final: #all
 * @subpackage Arrays
 */
class PersonArray extends AbstractStructArrayBase
{
    /**
     * The item
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \Exerp\Person\StructType\Person[]
     */
    protected ?array $item = null;
    /**
     * Constructor method for personArray
     * @uses PersonArray::setItem()
     * @param \Exerp\Person\StructType\Person[] $item
     */
    public function __construct(?array $item = null)
    {
        $this
            ->setItem($item);
    }
    /**
     * Get item value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Exerp\Person\StructType\Person[]
     */
    public function getItem(): ?array
    {
        return isset($this->item) ? $this->item : null;
    }
    /**
     * This method is responsible for validating the values passed to the setItem method
     * This method is willingly generated in order to preserve the one-line inline validation within the setItem method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateItemForArrayConstraintsFromSetItem(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $personArrayItemItem) {
            // validation for constraint: itemType
            if (!$personArrayItemItem instanceof \Exerp\Person\StructType\Person) {
                $invalidValues[] = is_object($personArrayItemItem) ? get_class($personArrayItemItem) : sprintf('%s(%s)', gettype($personArrayItemItem), var_export($personArrayItemItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The item property can only contain items of type \Exerp\Person\StructType\Person, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set item value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\Person[] $item
     * @return \Exerp\Person\ArrayType\PersonArray
     */
    public function setItem(?array $item = null): self
    {
        // validation for constraint: array
        if ('' !== ($itemArrayErrorMessage = self::validateItemForArrayConstraintsFromSetItem($item))) {
            throw new InvalidArgumentException($itemArrayErrorMessage, __LINE__);
        }
        if (is_null($item) || (is_array($item) && empty($item))) {
            unset($this->item);
        } else {
            $this->item = $item;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \Exerp\Person\StructType\Person|null
     */
    public function current(): ?\Exerp\Person\StructType\Person
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \Exerp\Person\StructType\Person|null
     */
    public function item($index): ?\Exerp\Person\StructType\Person
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \Exerp\Person\StructType\Person|null
     */
    public function first(): ?\Exerp\Person\StructType\Person
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \Exerp\Person\StructType\Person|null
     */
    public function last(): ?\Exerp\Person\StructType\Person
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \Exerp\Person\StructType\Person|null
     */
    public function offsetGet($offset): ?\Exerp\Person\StructType\Person
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\Person $item
     * @return \Exerp\Person\ArrayType\PersonArray
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Exerp\Person\StructType\Person) {
            throw new InvalidArgumentException(sprintf('The item property can only contain items of type \Exerp\Person\StructType\Person, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string item
     */
    public function getAttributeName(): string
    {
        return 'item';
    }
}
