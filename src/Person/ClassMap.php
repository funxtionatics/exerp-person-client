<?php

declare(strict_types=1);

namespace Exerp\Person;

/**
 * Class which returns the class map definition
 */
class ClassMap
{
    /**
     * Returns the mapping between the WSDL Structs and generated Structs' classes
     * This array is sent to the \SoapClient when calling the WS
     * @return string[]
     */
    final public static function get(): array
    {
        return [
            'apiPersonKey' => '\\Exerp\\Person\\StructType\\ApiPersonKey',
            'compositeKey' => '\\Exerp\\Person\\StructType\\CompositeKey',
            'personCommunication' => '\\Exerp\\Person\\StructType\\PersonCommunication',
            'errorDetail' => '\\Exerp\\Person\\StructType\\ErrorDetail',
            'person' => '\\Exerp\\Person\\StructType\\Person',
            'address' => '\\Exerp\\Person\\StructType\\Address',
            'passport' => '\\Exerp\\Person\\StructType\\Passport',
            'personPreferredCentersResponse' => '\\Exerp\\Person\\StructType\\PersonPreferredCentersResponse',
            'personPreferredCenters' => '\\Exerp\\Person\\StructType\\PersonPreferredCenters',
            'personRelationParameters' => '\\Exerp\\Person\\StructType\\PersonRelationParameters',
            'RelatedPerson' => '\\Exerp\\Person\\StructType\\RelatedPerson',
            'compositeSubKey' => '\\Exerp\\Person\\StructType\\CompositeSubKey',
            'mimeDocument' => '\\Exerp\\Person\\StructType\\MimeDocument',
            'createToDoParameter' => '\\Exerp\\Person\\StructType\\CreateToDoParameter',
            'toDo' => '\\Exerp\\Person\\StructType\\ToDo',
            'personTypeAndStatus' => '\\Exerp\\Person\\StructType\\PersonTypeAndStatus',
            'createCustomJournalDocumentParameters' => '\\Exerp\\Person\\StructType\\CreateCustomJournalDocumentParameters',
            'createCustomJournalDocumentResponse' => '\\Exerp\\Person\\StructType\\CreateCustomJournalDocumentResponse',
            'journalNoteResponse' => '\\Exerp\\Person\\StructType\\JournalNoteResponse',
            'journalNotes' => '\\Exerp\\Person\\StructType\\JournalNotes',
            'journalNote' => '\\Exerp\\Person\\StructType\\JournalNote',
            'requiredSignatures' => '\\Exerp\\Person\\StructType\\RequiredSignatures',
            'journalNoteDocumentSignature' => '\\Exerp\\Person\\StructType\\JournalNoteDocumentSignature',
            'signatureConfiguration' => '\\Exerp\\Person\\StructType\\SignatureConfiguration',
            'passportCountry' => '\\Exerp\\Person\\StructType\\PassportCountry',
            'overridePartnerBenefitActivationAuthorizationCodeParameters' => '\\Exerp\\Person\\StructType\\OverridePartnerBenefitActivationAuthorizationCodeParameters',
            'personDetail' => '\\Exerp\\Person\\StructType\\PersonDetail',
            'extendedAttributes' => '\\Exerp\\Person\\StructType\\ExtendedAttributes',
            'friends' => '\\Exerp\\Person\\StructType\\Friends',
            'memberCards' => '\\Exerp\\Person\\StructType\\MemberCards',
            'subscriptions' => '\\Exerp\\Person\\StructType\\Subscriptions',
            'personCompanyRelation' => '\\Exerp\\Person\\StructType\\PersonCompanyRelation',
            'companyAgreement' => '\\Exerp\\Person\\StructType\\CompanyAgreement',
            'allowedCorporateRelationTypes' => '\\Exerp\\Person\\StructType\\AllowedCorporateRelationTypes',
            'center' => '\\Exerp\\Person\\StructType\\Center',
            'privilegeSetAndSponsorship' => '\\Exerp\\Person\\StructType\\PrivilegeSetAndSponsorship',
            'extendedAttribute' => '\\Exerp\\Person\\StructType\\ExtendedAttribute',
            'friend' => '\\Exerp\\Person\\StructType\\Friend',
            'memberCard' => '\\Exerp\\Person\\StructType\\MemberCard',
            'personActivationDates' => '\\Exerp\\Person\\StructType\\PersonActivationDates',
            'subscription' => '\\Exerp\\Person\\StructType\\Subscription',
            'assignedAddOn' => '\\Exerp\\Person\\StructType\\AssignedAddOn',
            'subscriptionProduct' => '\\Exerp\\Person\\StructType\\SubscriptionProduct',
            'getAvailableLanguagesForMembersParameters' => '\\Exerp\\Person\\StructType\\GetAvailableLanguagesForMembersParameters',
            'availableLanguagesForMembersResponse' => '\\Exerp\\Person\\StructType\\AvailableLanguagesForMembersResponse',
            'languages' => '\\Exerp\\Person\\StructType\\Languages',
            'transferPersonParameters' => '\\Exerp\\Person\\StructType\\TransferPersonParameters',
            'transferSubscriptionInfo' => '\\Exerp\\Person\\StructType\\TransferSubscriptionInfo',
            'transferPersonInfo' => '\\Exerp\\Person\\StructType\\TransferPersonInfo',
            'availableProduct' => '\\Exerp\\Person\\StructType\\AvailableProduct',
            'missingSubscriptionInfo' => '\\Exerp\\Person\\StructType\\MissingSubscriptionInfo',
            'getAvailableSalutationsParameters' => '\\Exerp\\Person\\StructType\\GetAvailableSalutationsParameters',
            'apiScopeKey' => '\\Exerp\\Person\\StructType\\ApiScopeKey',
            'availableSalutationsResponse' => '\\Exerp\\Person\\StructType\\AvailableSalutationsResponse',
            'centers' => '\\Exerp\\Person\\StructType\\Centers',
            'centerWithSalutations' => '\\Exerp\\Person\\StructType\\CenterWithSalutations',
            'availableSalutations' => '\\Exerp\\Person\\StructType\\AvailableSalutations',
            'apiSalutation' => '\\Exerp\\Person\\StructType\\ApiSalutation',
            'changeSuspensionStatusParameters' => '\\Exerp\\Person\\StructType\\ChangeSuspensionStatusParameters',
            'journalNoteParameters' => '\\Exerp\\Person\\StructType\\JournalNoteParameters',
            'scope' => '\\Exerp\\Person\\StructType\\Scope',
            'journalNoteDocument' => '\\Exerp\\Person\\StructType\\JournalNoteDocument',
            'changePersonTypeParameter' => '\\Exerp\\Person\\StructType\\ChangePersonTypeParameter',
            'addSignedDocumentToJournalNoteParameters' => '\\Exerp\\Person\\StructType\\AddSignedDocumentToJournalNoteParameters',
            'signatureDatas' => '\\Exerp\\Person\\StructType\\SignatureDatas',
            'signatureData' => '\\Exerp\\Person\\StructType\\SignatureData',
            'findPerson' => '\\Exerp\\Person\\StructType\\FindPerson',
            'createLeadParameters' => '\\Exerp\\Person\\StructType\\CreateLeadParameters',
            'createPersonDetails' => '\\Exerp\\Person\\StructType\\CreatePersonDetails',
            'extendedAttributeMIMEs' => '\\Exerp\\Person\\StructType\\ExtendedAttributeMIMEs',
            'extendedAttributeMIME' => '\\Exerp\\Person\\StructType\\ExtendedAttributeMIME',
            'validationResult' => '\\Exerp\\Person\\StructType\\ValidationResult',
            'validationProblem' => '\\Exerp\\Person\\StructType\\ValidationProblem',
            'RelatedPersonArray' => '\\Exerp\\Person\\ArrayType\\RelatedPersonArray',
            'passportCountryArray' => '\\Exerp\\Person\\ArrayType\\PassportCountryArray',
            'journalNoteArray' => '\\Exerp\\Person\\ArrayType\\JournalNoteArray',
            'personArray' => '\\Exerp\\Person\\ArrayType\\PersonArray',
            'APIException' => '\\Exerp\\Person\\StructType\\APIException',
            'TransferException' => '\\Exerp\\Person\\StructType\\TransferException',
            'stringArray' => '\\Exerp\\Person\\ArrayType\\StringArray',
        ];
    }
}
