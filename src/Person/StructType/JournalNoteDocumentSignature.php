<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for journalNoteDocumentSignature StructType
 * @subpackage Structs
 */
class JournalNoteDocumentSignature extends AbstractStructBase
{
    /**
     * The signatureCaptured
     * @var bool|null
     */
    protected ?bool $signatureCaptured = null;
    /**
     * The signatureConfiguration
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\SignatureConfiguration|null
     */
    protected ?\Exerp\Person\StructType\SignatureConfiguration $signatureConfiguration = null;
    /**
     * Constructor method for journalNoteDocumentSignature
     * @uses JournalNoteDocumentSignature::setSignatureCaptured()
     * @uses JournalNoteDocumentSignature::setSignatureConfiguration()
     * @param bool $signatureCaptured
     * @param \Exerp\Person\StructType\SignatureConfiguration $signatureConfiguration
     */
    public function __construct(?bool $signatureCaptured = null, ?\Exerp\Person\StructType\SignatureConfiguration $signatureConfiguration = null)
    {
        $this
            ->setSignatureCaptured($signatureCaptured)
            ->setSignatureConfiguration($signatureConfiguration);
    }
    /**
     * Get signatureCaptured value
     * @return bool|null
     */
    public function getSignatureCaptured(): ?bool
    {
        return $this->signatureCaptured;
    }
    /**
     * Set signatureCaptured value
     * @param bool $signatureCaptured
     * @return \Exerp\Person\StructType\JournalNoteDocumentSignature
     */
    public function setSignatureCaptured(?bool $signatureCaptured = null): self
    {
        // validation for constraint: boolean
        if (!is_null($signatureCaptured) && !is_bool($signatureCaptured)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($signatureCaptured, true), gettype($signatureCaptured)), __LINE__);
        }
        $this->signatureCaptured = $signatureCaptured;
        
        return $this;
    }
    /**
     * Get signatureConfiguration value
     * @return \Exerp\Person\StructType\SignatureConfiguration|null
     */
    public function getSignatureConfiguration(): ?\Exerp\Person\StructType\SignatureConfiguration
    {
        return $this->signatureConfiguration;
    }
    /**
     * Set signatureConfiguration value
     * @param \Exerp\Person\StructType\SignatureConfiguration $signatureConfiguration
     * @return \Exerp\Person\StructType\JournalNoteDocumentSignature
     */
    public function setSignatureConfiguration(?\Exerp\Person\StructType\SignatureConfiguration $signatureConfiguration = null): self
    {
        $this->signatureConfiguration = $signatureConfiguration;
        
        return $this;
    }
}
