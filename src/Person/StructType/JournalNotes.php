<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for journalNotes StructType
 * @subpackage Structs
 */
class JournalNotes extends AbstractStructBase
{
    /**
     * The journalNote
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\JournalNote[]
     */
    protected ?array $journalNote = null;
    /**
     * Constructor method for journalNotes
     * @uses JournalNotes::setJournalNote()
     * @param \Exerp\Person\StructType\JournalNote[] $journalNote
     */
    public function __construct(?array $journalNote = null)
    {
        $this
            ->setJournalNote($journalNote);
    }
    /**
     * Get journalNote value
     * @return \Exerp\Person\StructType\JournalNote[]
     */
    public function getJournalNote(): ?array
    {
        return $this->journalNote;
    }
    /**
     * This method is responsible for validating the values passed to the setJournalNote method
     * This method is willingly generated in order to preserve the one-line inline validation within the setJournalNote method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateJournalNoteForArrayConstraintsFromSetJournalNote(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $journalNotesJournalNoteItem) {
            // validation for constraint: itemType
            if (!$journalNotesJournalNoteItem instanceof \Exerp\Person\StructType\JournalNote) {
                $invalidValues[] = is_object($journalNotesJournalNoteItem) ? get_class($journalNotesJournalNoteItem) : sprintf('%s(%s)', gettype($journalNotesJournalNoteItem), var_export($journalNotesJournalNoteItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The journalNote property can only contain items of type \Exerp\Person\StructType\JournalNote, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set journalNote value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\JournalNote[] $journalNote
     * @return \Exerp\Person\StructType\JournalNotes
     */
    public function setJournalNote(?array $journalNote = null): self
    {
        // validation for constraint: array
        if ('' !== ($journalNoteArrayErrorMessage = self::validateJournalNoteForArrayConstraintsFromSetJournalNote($journalNote))) {
            throw new InvalidArgumentException($journalNoteArrayErrorMessage, __LINE__);
        }
        $this->journalNote = $journalNote;
        
        return $this;
    }
    /**
     * Add item to journalNote value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\JournalNote $item
     * @return \Exerp\Person\StructType\JournalNotes
     */
    public function addToJournalNote(\Exerp\Person\StructType\JournalNote $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Exerp\Person\StructType\JournalNote) {
            throw new InvalidArgumentException(sprintf('The journalNote property can only contain items of type \Exerp\Person\StructType\JournalNote, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->journalNote[] = $item;
        
        return $this;
    }
}
