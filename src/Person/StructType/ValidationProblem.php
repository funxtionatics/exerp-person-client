<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for validationProblem StructType
 * Meta information extracted from the WSDL
 * - final: extension restriction
 * @subpackage Structs
 */
class ValidationProblem extends AbstractStructBase
{
    /**
     * The errorCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $errorCode = null;
    /**
     * The message
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $message = null;
    /**
     * The propertyNames
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var string[]
     */
    protected ?array $propertyNames = null;
    /**
     * Constructor method for validationProblem
     * @uses ValidationProblem::setErrorCode()
     * @uses ValidationProblem::setMessage()
     * @uses ValidationProblem::setPropertyNames()
     * @param string $errorCode
     * @param string $message
     * @param string[] $propertyNames
     */
    public function __construct(?string $errorCode = null, ?string $message = null, ?array $propertyNames = null)
    {
        $this
            ->setErrorCode($errorCode)
            ->setMessage($message)
            ->setPropertyNames($propertyNames);
    }
    /**
     * Get errorCode value
     * @return string|null
     */
    public function getErrorCode(): ?string
    {
        return $this->errorCode;
    }
    /**
     * Set errorCode value
     * @uses \Exerp\Person\EnumType\ErrorCode::valueIsValid()
     * @uses \Exerp\Person\EnumType\ErrorCode::getValidValues()
     * @throws InvalidArgumentException
     * @param string $errorCode
     * @return \Exerp\Person\StructType\ValidationProblem
     */
    public function setErrorCode(?string $errorCode = null): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Person\EnumType\ErrorCode::valueIsValid($errorCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Person\EnumType\ErrorCode', is_array($errorCode) ? implode(', ', $errorCode) : var_export($errorCode, true), implode(', ', \Exerp\Person\EnumType\ErrorCode::getValidValues())), __LINE__);
        }
        $this->errorCode = $errorCode;
        
        return $this;
    }
    /**
     * Get message value
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }
    /**
     * Set message value
     * @param string $message
     * @return \Exerp\Person\StructType\ValidationProblem
     */
    public function setMessage(?string $message = null): self
    {
        // validation for constraint: string
        if (!is_null($message) && !is_string($message)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($message, true), gettype($message)), __LINE__);
        }
        $this->message = $message;
        
        return $this;
    }
    /**
     * Get propertyNames value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string[]
     */
    public function getPropertyNames(): ?array
    {
        return isset($this->propertyNames) ? $this->propertyNames : null;
    }
    /**
     * This method is responsible for validating the values passed to the setPropertyNames method
     * This method is willingly generated in order to preserve the one-line inline validation within the setPropertyNames method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validatePropertyNamesForArrayConstraintsFromSetPropertyNames(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $validationProblemPropertyNamesItem) {
            // validation for constraint: itemType
            if (!is_string($validationProblemPropertyNamesItem)) {
                $invalidValues[] = is_object($validationProblemPropertyNamesItem) ? get_class($validationProblemPropertyNamesItem) : sprintf('%s(%s)', gettype($validationProblemPropertyNamesItem), var_export($validationProblemPropertyNamesItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The propertyNames property can only contain items of type string, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set propertyNames value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param string[] $propertyNames
     * @return \Exerp\Person\StructType\ValidationProblem
     */
    public function setPropertyNames(?array $propertyNames = null): self
    {
        // validation for constraint: array
        if ('' !== ($propertyNamesArrayErrorMessage = self::validatePropertyNamesForArrayConstraintsFromSetPropertyNames($propertyNames))) {
            throw new InvalidArgumentException($propertyNamesArrayErrorMessage, __LINE__);
        }
        if (is_null($propertyNames) || (is_array($propertyNames) && empty($propertyNames))) {
            unset($this->propertyNames);
        } else {
            $this->propertyNames = $propertyNames;
        }
        
        return $this;
    }
    /**
     * Add item to propertyNames value
     * @throws InvalidArgumentException
     * @param string $item
     * @return \Exerp\Person\StructType\ValidationProblem
     */
    public function addToPropertyNames(string $item): self
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new InvalidArgumentException(sprintf('The propertyNames property can only contain items of type string, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->propertyNames[] = $item;
        
        return $this;
    }
}
