<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for validationResult StructType
 * Meta information extracted from the WSDL
 * - final: extension restriction
 * @subpackage Structs
 */
class ValidationResult extends AbstractStructBase
{
    /**
     * The problems
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \Exerp\Person\StructType\ValidationProblem[]
     */
    protected ?array $problems = null;
    /**
     * The valid
     * @var bool|null
     */
    protected ?bool $valid = null;
    /**
     * Constructor method for validationResult
     * @uses ValidationResult::setProblems()
     * @uses ValidationResult::setValid()
     * @param \Exerp\Person\StructType\ValidationProblem[] $problems
     * @param bool $valid
     */
    public function __construct(?array $problems = null, ?bool $valid = null)
    {
        $this
            ->setProblems($problems)
            ->setValid($valid);
    }
    /**
     * Get problems value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Exerp\Person\StructType\ValidationProblem[]
     */
    public function getProblems(): ?array
    {
        return isset($this->problems) ? $this->problems : null;
    }
    /**
     * This method is responsible for validating the values passed to the setProblems method
     * This method is willingly generated in order to preserve the one-line inline validation within the setProblems method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateProblemsForArrayConstraintsFromSetProblems(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $validationResultProblemsItem) {
            // validation for constraint: itemType
            if (!$validationResultProblemsItem instanceof \Exerp\Person\StructType\ValidationProblem) {
                $invalidValues[] = is_object($validationResultProblemsItem) ? get_class($validationResultProblemsItem) : sprintf('%s(%s)', gettype($validationResultProblemsItem), var_export($validationResultProblemsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The problems property can only contain items of type \Exerp\Person\StructType\ValidationProblem, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set problems value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\ValidationProblem[] $problems
     * @return \Exerp\Person\StructType\ValidationResult
     */
    public function setProblems(?array $problems = null): self
    {
        // validation for constraint: array
        if ('' !== ($problemsArrayErrorMessage = self::validateProblemsForArrayConstraintsFromSetProblems($problems))) {
            throw new InvalidArgumentException($problemsArrayErrorMessage, __LINE__);
        }
        if (is_null($problems) || (is_array($problems) && empty($problems))) {
            unset($this->problems);
        } else {
            $this->problems = $problems;
        }
        
        return $this;
    }
    /**
     * Add item to problems value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\ValidationProblem $item
     * @return \Exerp\Person\StructType\ValidationResult
     */
    public function addToProblems(\Exerp\Person\StructType\ValidationProblem $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Exerp\Person\StructType\ValidationProblem) {
            throw new InvalidArgumentException(sprintf('The problems property can only contain items of type \Exerp\Person\StructType\ValidationProblem, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->problems[] = $item;
        
        return $this;
    }
    /**
     * Get valid value
     * @return bool|null
     */
    public function getValid(): ?bool
    {
        return $this->valid;
    }
    /**
     * Set valid value
     * @param bool $valid
     * @return \Exerp\Person\StructType\ValidationResult
     */
    public function setValid(?bool $valid = null): self
    {
        // validation for constraint: boolean
        if (!is_null($valid) && !is_bool($valid)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($valid, true), gettype($valid)), __LINE__);
        }
        $this->valid = $valid;
        
        return $this;
    }
}
