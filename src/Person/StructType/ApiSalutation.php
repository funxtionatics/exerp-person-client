<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for apiSalutation StructType
 * @subpackage Structs
 */
class ApiSalutation extends AbstractStructBase
{
    /**
     * The text
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $text = null;
    /**
     * The gender
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $gender = null;
    /**
     * Constructor method for apiSalutation
     * @uses ApiSalutation::setText()
     * @uses ApiSalutation::setGender()
     * @param string $text
     * @param string $gender
     */
    public function __construct(?string $text = null, ?string $gender = null)
    {
        $this
            ->setText($text)
            ->setGender($gender);
    }
    /**
     * Get text value
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }
    /**
     * Set text value
     * @param string $text
     * @return \Exerp\Person\StructType\ApiSalutation
     */
    public function setText(?string $text = null): self
    {
        // validation for constraint: string
        if (!is_null($text) && !is_string($text)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($text, true), gettype($text)), __LINE__);
        }
        $this->text = $text;
        
        return $this;
    }
    /**
     * Get gender value
     * @return string|null
     */
    public function getGender(): ?string
    {
        return $this->gender;
    }
    /**
     * Set gender value
     * @param string $gender
     * @return \Exerp\Person\StructType\ApiSalutation
     */
    public function setGender(?string $gender = null): self
    {
        // validation for constraint: string
        if (!is_null($gender) && !is_string($gender)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($gender, true), gettype($gender)), __LINE__);
        }
        $this->gender = $gender;
        
        return $this;
    }
}
