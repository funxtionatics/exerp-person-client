<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for personActivationDates StructType
 * @subpackage Structs
 */
class PersonActivationDates extends AbstractStructBase
{
    /**
     * The firstActiveStartDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $firstActiveStartDate = null;
    /**
     * The lastActiveEndDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $lastActiveEndDate = null;
    /**
     * The lastActiveStartDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $lastActiveStartDate = null;
    /**
     * Constructor method for personActivationDates
     * @uses PersonActivationDates::setFirstActiveStartDate()
     * @uses PersonActivationDates::setLastActiveEndDate()
     * @uses PersonActivationDates::setLastActiveStartDate()
     * @param string $firstActiveStartDate
     * @param string $lastActiveEndDate
     * @param string $lastActiveStartDate
     */
    public function __construct(?string $firstActiveStartDate = null, ?string $lastActiveEndDate = null, ?string $lastActiveStartDate = null)
    {
        $this
            ->setFirstActiveStartDate($firstActiveStartDate)
            ->setLastActiveEndDate($lastActiveEndDate)
            ->setLastActiveStartDate($lastActiveStartDate);
    }
    /**
     * Get firstActiveStartDate value
     * @return string|null
     */
    public function getFirstActiveStartDate(): ?string
    {
        return $this->firstActiveStartDate;
    }
    /**
     * Set firstActiveStartDate value
     * @param string $firstActiveStartDate
     * @return \Exerp\Person\StructType\PersonActivationDates
     */
    public function setFirstActiveStartDate(?string $firstActiveStartDate = null): self
    {
        // validation for constraint: string
        if (!is_null($firstActiveStartDate) && !is_string($firstActiveStartDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($firstActiveStartDate, true), gettype($firstActiveStartDate)), __LINE__);
        }
        $this->firstActiveStartDate = $firstActiveStartDate;
        
        return $this;
    }
    /**
     * Get lastActiveEndDate value
     * @return string|null
     */
    public function getLastActiveEndDate(): ?string
    {
        return $this->lastActiveEndDate;
    }
    /**
     * Set lastActiveEndDate value
     * @param string $lastActiveEndDate
     * @return \Exerp\Person\StructType\PersonActivationDates
     */
    public function setLastActiveEndDate(?string $lastActiveEndDate = null): self
    {
        // validation for constraint: string
        if (!is_null($lastActiveEndDate) && !is_string($lastActiveEndDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($lastActiveEndDate, true), gettype($lastActiveEndDate)), __LINE__);
        }
        $this->lastActiveEndDate = $lastActiveEndDate;
        
        return $this;
    }
    /**
     * Get lastActiveStartDate value
     * @return string|null
     */
    public function getLastActiveStartDate(): ?string
    {
        return $this->lastActiveStartDate;
    }
    /**
     * Set lastActiveStartDate value
     * @param string $lastActiveStartDate
     * @return \Exerp\Person\StructType\PersonActivationDates
     */
    public function setLastActiveStartDate(?string $lastActiveStartDate = null): self
    {
        // validation for constraint: string
        if (!is_null($lastActiveStartDate) && !is_string($lastActiveStartDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($lastActiveStartDate, true), gettype($lastActiveStartDate)), __LINE__);
        }
        $this->lastActiveStartDate = $lastActiveStartDate;
        
        return $this;
    }
}
