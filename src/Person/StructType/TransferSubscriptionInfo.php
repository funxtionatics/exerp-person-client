<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for transferSubscriptionInfo StructType
 * @subpackage Structs
 */
class TransferSubscriptionInfo extends AbstractStructBase
{
    /**
     * The productKey
     * @var \Exerp\Person\StructType\CompositeKey|null
     */
    protected ?\Exerp\Person\StructType\CompositeKey $productKey = null;
    /**
     * The subscriptionKey
     * @var \Exerp\Person\StructType\CompositeKey|null
     */
    protected ?\Exerp\Person\StructType\CompositeKey $subscriptionKey = null;
    /**
     * The terminateIfUnavailable
     * @var bool|null
     */
    protected ?bool $terminateIfUnavailable = null;
    /**
     * Constructor method for transferSubscriptionInfo
     * @uses TransferSubscriptionInfo::setProductKey()
     * @uses TransferSubscriptionInfo::setSubscriptionKey()
     * @uses TransferSubscriptionInfo::setTerminateIfUnavailable()
     * @param \Exerp\Person\StructType\CompositeKey $productKey
     * @param \Exerp\Person\StructType\CompositeKey $subscriptionKey
     * @param bool $terminateIfUnavailable
     */
    public function __construct(?\Exerp\Person\StructType\CompositeKey $productKey = null, ?\Exerp\Person\StructType\CompositeKey $subscriptionKey = null, ?bool $terminateIfUnavailable = null)
    {
        $this
            ->setProductKey($productKey)
            ->setSubscriptionKey($subscriptionKey)
            ->setTerminateIfUnavailable($terminateIfUnavailable);
    }
    /**
     * Get productKey value
     * @return \Exerp\Person\StructType\CompositeKey|null
     */
    public function getProductKey(): ?\Exerp\Person\StructType\CompositeKey
    {
        return $this->productKey;
    }
    /**
     * Set productKey value
     * @param \Exerp\Person\StructType\CompositeKey $productKey
     * @return \Exerp\Person\StructType\TransferSubscriptionInfo
     */
    public function setProductKey(?\Exerp\Person\StructType\CompositeKey $productKey = null): self
    {
        $this->productKey = $productKey;
        
        return $this;
    }
    /**
     * Get subscriptionKey value
     * @return \Exerp\Person\StructType\CompositeKey|null
     */
    public function getSubscriptionKey(): ?\Exerp\Person\StructType\CompositeKey
    {
        return $this->subscriptionKey;
    }
    /**
     * Set subscriptionKey value
     * @param \Exerp\Person\StructType\CompositeKey $subscriptionKey
     * @return \Exerp\Person\StructType\TransferSubscriptionInfo
     */
    public function setSubscriptionKey(?\Exerp\Person\StructType\CompositeKey $subscriptionKey = null): self
    {
        $this->subscriptionKey = $subscriptionKey;
        
        return $this;
    }
    /**
     * Get terminateIfUnavailable value
     * @return bool|null
     */
    public function getTerminateIfUnavailable(): ?bool
    {
        return $this->terminateIfUnavailable;
    }
    /**
     * Set terminateIfUnavailable value
     * @param bool $terminateIfUnavailable
     * @return \Exerp\Person\StructType\TransferSubscriptionInfo
     */
    public function setTerminateIfUnavailable(?bool $terminateIfUnavailable = null): self
    {
        // validation for constraint: boolean
        if (!is_null($terminateIfUnavailable) && !is_bool($terminateIfUnavailable)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($terminateIfUnavailable, true), gettype($terminateIfUnavailable)), __LINE__);
        }
        $this->terminateIfUnavailable = $terminateIfUnavailable;
        
        return $this;
    }
}
