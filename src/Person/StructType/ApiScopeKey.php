<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for apiScopeKey StructType
 * @subpackage Structs
 */
class ApiScopeKey extends AbstractStructBase
{
    /**
     * The type
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * The id
     * @var int|null
     */
    protected ?int $id = null;
    /**
     * Constructor method for apiScopeKey
     * @uses ApiScopeKey::setType()
     * @uses ApiScopeKey::setId()
     * @param string $type
     * @param int $id
     */
    public function __construct(?string $type = null, ?int $id = null)
    {
        $this
            ->setType($type)
            ->setId($id);
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @uses \Exerp\Person\EnumType\ScopeType::valueIsValid()
     * @uses \Exerp\Person\EnumType\ScopeType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $type
     * @return \Exerp\Person\StructType\ApiScopeKey
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Person\EnumType\ScopeType::valueIsValid($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Person\EnumType\ScopeType', is_array($type) ? implode(', ', $type) : var_export($type, true), implode(', ', \Exerp\Person\EnumType\ScopeType::getValidValues())), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
    /**
     * Get id value
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }
    /**
     * Set id value
     * @param int $id
     * @return \Exerp\Person\StructType\ApiScopeKey
     */
    public function setId(?int $id = null): self
    {
        // validation for constraint: int
        if (!is_null($id) && !(is_int($id) || ctype_digit($id))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($id, true), gettype($id)), __LINE__);
        }
        $this->id = $id;
        
        return $this;
    }
}
