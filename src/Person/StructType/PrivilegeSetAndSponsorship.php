<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for privilegeSetAndSponsorship StructType
 * Meta information extracted from the WSDL
 * - final: extension restriction
 * @subpackage Structs
 */
class PrivilegeSetAndSponsorship extends AbstractStructBase
{
    /**
     * The privilegeSet
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $privilegeSet = null;
    /**
     * The sponsorship
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $sponsorship = null;
    /**
     * The sponsorshipType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $sponsorshipType = null;
    /**
     * Constructor method for privilegeSetAndSponsorship
     * @uses PrivilegeSetAndSponsorship::setPrivilegeSet()
     * @uses PrivilegeSetAndSponsorship::setSponsorship()
     * @uses PrivilegeSetAndSponsorship::setSponsorshipType()
     * @param int $privilegeSet
     * @param float $sponsorship
     * @param string $sponsorshipType
     */
    public function __construct(?int $privilegeSet = null, ?float $sponsorship = null, ?string $sponsorshipType = null)
    {
        $this
            ->setPrivilegeSet($privilegeSet)
            ->setSponsorship($sponsorship)
            ->setSponsorshipType($sponsorshipType);
    }
    /**
     * Get privilegeSet value
     * @return int|null
     */
    public function getPrivilegeSet(): ?int
    {
        return $this->privilegeSet;
    }
    /**
     * Set privilegeSet value
     * @param int $privilegeSet
     * @return \Exerp\Person\StructType\PrivilegeSetAndSponsorship
     */
    public function setPrivilegeSet(?int $privilegeSet = null): self
    {
        // validation for constraint: int
        if (!is_null($privilegeSet) && !(is_int($privilegeSet) || ctype_digit($privilegeSet))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($privilegeSet, true), gettype($privilegeSet)), __LINE__);
        }
        $this->privilegeSet = $privilegeSet;
        
        return $this;
    }
    /**
     * Get sponsorship value
     * @return float|null
     */
    public function getSponsorship(): ?float
    {
        return $this->sponsorship;
    }
    /**
     * Set sponsorship value
     * @param float $sponsorship
     * @return \Exerp\Person\StructType\PrivilegeSetAndSponsorship
     */
    public function setSponsorship(?float $sponsorship = null): self
    {
        // validation for constraint: float
        if (!is_null($sponsorship) && !(is_float($sponsorship) || is_numeric($sponsorship))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($sponsorship, true), gettype($sponsorship)), __LINE__);
        }
        $this->sponsorship = $sponsorship;
        
        return $this;
    }
    /**
     * Get sponsorshipType value
     * @return string|null
     */
    public function getSponsorshipType(): ?string
    {
        return $this->sponsorshipType;
    }
    /**
     * Set sponsorshipType value
     * @uses \Exerp\Person\EnumType\SponsorshipType::valueIsValid()
     * @uses \Exerp\Person\EnumType\SponsorshipType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $sponsorshipType
     * @return \Exerp\Person\StructType\PrivilegeSetAndSponsorship
     */
    public function setSponsorshipType(?string $sponsorshipType = null): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Person\EnumType\SponsorshipType::valueIsValid($sponsorshipType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Person\EnumType\SponsorshipType', is_array($sponsorshipType) ? implode(', ', $sponsorshipType) : var_export($sponsorshipType, true), implode(', ', \Exerp\Person\EnumType\SponsorshipType::getValidValues())), __LINE__);
        }
        $this->sponsorshipType = $sponsorshipType;
        
        return $this;
    }
}
