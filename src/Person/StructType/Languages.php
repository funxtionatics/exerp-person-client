<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for languages StructType
 * @subpackage Structs
 */
class Languages extends AbstractStructBase
{
    /**
     * The language
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var string[]
     */
    protected ?array $language = null;
    /**
     * Constructor method for languages
     * @uses Languages::setLanguage()
     * @param string[] $language
     */
    public function __construct(?array $language = null)
    {
        $this
            ->setLanguage($language);
    }
    /**
     * Get language value
     * @return string[]
     */
    public function getLanguage(): ?array
    {
        return $this->language;
    }
    /**
     * This method is responsible for validating the values passed to the setLanguage method
     * This method is willingly generated in order to preserve the one-line inline validation within the setLanguage method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateLanguageForArrayConstraintsFromSetLanguage(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $languagesLanguageItem) {
            // validation for constraint: itemType
            if (!is_string($languagesLanguageItem)) {
                $invalidValues[] = is_object($languagesLanguageItem) ? get_class($languagesLanguageItem) : sprintf('%s(%s)', gettype($languagesLanguageItem), var_export($languagesLanguageItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The language property can only contain items of type string, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set language value
     * @throws InvalidArgumentException
     * @param string[] $language
     * @return \Exerp\Person\StructType\Languages
     */
    public function setLanguage(?array $language = null): self
    {
        // validation for constraint: array
        if ('' !== ($languageArrayErrorMessage = self::validateLanguageForArrayConstraintsFromSetLanguage($language))) {
            throw new InvalidArgumentException($languageArrayErrorMessage, __LINE__);
        }
        $this->language = $language;
        
        return $this;
    }
    /**
     * Add item to language value
     * @throws InvalidArgumentException
     * @param string $item
     * @return \Exerp\Person\StructType\Languages
     */
    public function addToLanguage(string $item): self
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new InvalidArgumentException(sprintf('The language property can only contain items of type string, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->language[] = $item;
        
        return $this;
    }
}
