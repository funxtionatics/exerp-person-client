<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for missingSubscriptionInfo StructType
 * @subpackage Structs
 */
class MissingSubscriptionInfo extends AbstractStructBase
{
    /**
     * The mainSubscription
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $mainSubscription = null;
    /**
     * The productBlockedInNewCenter
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $productBlockedInNewCenter = null;
    /**
     * The subscriptionKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\CompositeKey|null
     */
    protected ?\Exerp\Person\StructType\CompositeKey $subscriptionKey = null;
    /**
     * Constructor method for missingSubscriptionInfo
     * @uses MissingSubscriptionInfo::setMainSubscription()
     * @uses MissingSubscriptionInfo::setProductBlockedInNewCenter()
     * @uses MissingSubscriptionInfo::setSubscriptionKey()
     * @param bool $mainSubscription
     * @param bool $productBlockedInNewCenter
     * @param \Exerp\Person\StructType\CompositeKey $subscriptionKey
     */
    public function __construct(?bool $mainSubscription = null, ?bool $productBlockedInNewCenter = null, ?\Exerp\Person\StructType\CompositeKey $subscriptionKey = null)
    {
        $this
            ->setMainSubscription($mainSubscription)
            ->setProductBlockedInNewCenter($productBlockedInNewCenter)
            ->setSubscriptionKey($subscriptionKey);
    }
    /**
     * Get mainSubscription value
     * @return bool|null
     */
    public function getMainSubscription(): ?bool
    {
        return $this->mainSubscription;
    }
    /**
     * Set mainSubscription value
     * @param bool $mainSubscription
     * @return \Exerp\Person\StructType\MissingSubscriptionInfo
     */
    public function setMainSubscription(?bool $mainSubscription = null): self
    {
        // validation for constraint: boolean
        if (!is_null($mainSubscription) && !is_bool($mainSubscription)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($mainSubscription, true), gettype($mainSubscription)), __LINE__);
        }
        $this->mainSubscription = $mainSubscription;
        
        return $this;
    }
    /**
     * Get productBlockedInNewCenter value
     * @return bool|null
     */
    public function getProductBlockedInNewCenter(): ?bool
    {
        return $this->productBlockedInNewCenter;
    }
    /**
     * Set productBlockedInNewCenter value
     * @param bool $productBlockedInNewCenter
     * @return \Exerp\Person\StructType\MissingSubscriptionInfo
     */
    public function setProductBlockedInNewCenter(?bool $productBlockedInNewCenter = null): self
    {
        // validation for constraint: boolean
        if (!is_null($productBlockedInNewCenter) && !is_bool($productBlockedInNewCenter)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($productBlockedInNewCenter, true), gettype($productBlockedInNewCenter)), __LINE__);
        }
        $this->productBlockedInNewCenter = $productBlockedInNewCenter;
        
        return $this;
    }
    /**
     * Get subscriptionKey value
     * @return \Exerp\Person\StructType\CompositeKey|null
     */
    public function getSubscriptionKey(): ?\Exerp\Person\StructType\CompositeKey
    {
        return $this->subscriptionKey;
    }
    /**
     * Set subscriptionKey value
     * @param \Exerp\Person\StructType\CompositeKey $subscriptionKey
     * @return \Exerp\Person\StructType\MissingSubscriptionInfo
     */
    public function setSubscriptionKey(?\Exerp\Person\StructType\CompositeKey $subscriptionKey = null): self
    {
        $this->subscriptionKey = $subscriptionKey;
        
        return $this;
    }
}
