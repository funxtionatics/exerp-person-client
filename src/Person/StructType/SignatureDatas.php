<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for signatureDatas StructType
 * @subpackage Structs
 */
class SignatureDatas extends AbstractStructBase
{
    /**
     * The signatureData
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * @var \Exerp\Person\StructType\SignatureData[]
     */
    protected ?array $signatureData = null;
    /**
     * Constructor method for signatureDatas
     * @uses SignatureDatas::setSignatureData()
     * @param \Exerp\Person\StructType\SignatureData[] $signatureData
     */
    public function __construct(?array $signatureData = null)
    {
        $this
            ->setSignatureData($signatureData);
    }
    /**
     * Get signatureData value
     * @return \Exerp\Person\StructType\SignatureData[]
     */
    public function getSignatureData(): ?array
    {
        return $this->signatureData;
    }
    /**
     * This method is responsible for validating the values passed to the setSignatureData method
     * This method is willingly generated in order to preserve the one-line inline validation within the setSignatureData method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateSignatureDataForArrayConstraintsFromSetSignatureData(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $signatureDatasSignatureDataItem) {
            // validation for constraint: itemType
            if (!$signatureDatasSignatureDataItem instanceof \Exerp\Person\StructType\SignatureData) {
                $invalidValues[] = is_object($signatureDatasSignatureDataItem) ? get_class($signatureDatasSignatureDataItem) : sprintf('%s(%s)', gettype($signatureDatasSignatureDataItem), var_export($signatureDatasSignatureDataItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The signatureData property can only contain items of type \Exerp\Person\StructType\SignatureData, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set signatureData value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\SignatureData[] $signatureData
     * @return \Exerp\Person\StructType\SignatureDatas
     */
    public function setSignatureData(?array $signatureData = null): self
    {
        // validation for constraint: array
        if ('' !== ($signatureDataArrayErrorMessage = self::validateSignatureDataForArrayConstraintsFromSetSignatureData($signatureData))) {
            throw new InvalidArgumentException($signatureDataArrayErrorMessage, __LINE__);
        }
        $this->signatureData = $signatureData;
        
        return $this;
    }
    /**
     * Add item to signatureData value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\SignatureData $item
     * @return \Exerp\Person\StructType\SignatureDatas
     */
    public function addToSignatureData(\Exerp\Person\StructType\SignatureData $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Exerp\Person\StructType\SignatureData) {
            throw new InvalidArgumentException(sprintf('The signatureData property can only contain items of type \Exerp\Person\StructType\SignatureData, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->signatureData[] = $item;
        
        return $this;
    }
}
