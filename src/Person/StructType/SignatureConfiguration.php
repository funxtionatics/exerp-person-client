<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for signatureConfiguration StructType
 * @subpackage Structs
 */
class SignatureConfiguration extends AbstractStructBase
{
    /**
     * The name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $name = null;
    /**
     * The rank
     * @var int|null
     */
    protected ?int $rank = null;
    /**
     * The signatureReason
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $signatureReason = null;
    /**
     * Constructor method for signatureConfiguration
     * @uses SignatureConfiguration::setName()
     * @uses SignatureConfiguration::setRank()
     * @uses SignatureConfiguration::setSignatureReason()
     * @param string $name
     * @param int $rank
     * @param string $signatureReason
     */
    public function __construct(?string $name = null, ?int $rank = null, ?string $signatureReason = null)
    {
        $this
            ->setName($name)
            ->setRank($rank)
            ->setSignatureReason($signatureReason);
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \Exerp\Person\StructType\SignatureConfiguration
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        
        return $this;
    }
    /**
     * Get rank value
     * @return int|null
     */
    public function getRank(): ?int
    {
        return $this->rank;
    }
    /**
     * Set rank value
     * @param int $rank
     * @return \Exerp\Person\StructType\SignatureConfiguration
     */
    public function setRank(?int $rank = null): self
    {
        // validation for constraint: int
        if (!is_null($rank) && !(is_int($rank) || ctype_digit($rank))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($rank, true), gettype($rank)), __LINE__);
        }
        $this->rank = $rank;
        
        return $this;
    }
    /**
     * Get signatureReason value
     * @return string|null
     */
    public function getSignatureReason(): ?string
    {
        return $this->signatureReason;
    }
    /**
     * Set signatureReason value
     * @uses \Exerp\Person\EnumType\SignatureReason::valueIsValid()
     * @uses \Exerp\Person\EnumType\SignatureReason::getValidValues()
     * @throws InvalidArgumentException
     * @param string $signatureReason
     * @return \Exerp\Person\StructType\SignatureConfiguration
     */
    public function setSignatureReason(?string $signatureReason = null): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Person\EnumType\SignatureReason::valueIsValid($signatureReason)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Person\EnumType\SignatureReason', is_array($signatureReason) ? implode(', ', $signatureReason) : var_export($signatureReason, true), implode(', ', \Exerp\Person\EnumType\SignatureReason::getValidValues())), __LINE__);
        }
        $this->signatureReason = $signatureReason;
        
        return $this;
    }
}
