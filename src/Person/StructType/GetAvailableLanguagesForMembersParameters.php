<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for getAvailableLanguagesForMembersParameters StructType
 * @subpackage Structs
 */
class GetAvailableLanguagesForMembersParameters extends AbstractStructBase
{
    /**
     * The centerId
     * @var int|null
     */
    protected ?int $centerId = null;
    /**
     * Constructor method for getAvailableLanguagesForMembersParameters
     * @uses GetAvailableLanguagesForMembersParameters::setCenterId()
     * @param int $centerId
     */
    public function __construct(?int $centerId = null)
    {
        $this
            ->setCenterId($centerId);
    }
    /**
     * Get centerId value
     * @return int|null
     */
    public function getCenterId(): ?int
    {
        return $this->centerId;
    }
    /**
     * Set centerId value
     * @param int $centerId
     * @return \Exerp\Person\StructType\GetAvailableLanguagesForMembersParameters
     */
    public function setCenterId(?int $centerId = null): self
    {
        // validation for constraint: int
        if (!is_null($centerId) && !(is_int($centerId) || ctype_digit($centerId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($centerId, true), gettype($centerId)), __LINE__);
        }
        $this->centerId = $centerId;
        
        return $this;
    }
}
