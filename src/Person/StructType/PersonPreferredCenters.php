<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for personPreferredCenters StructType
 * @subpackage Structs
 */
class PersonPreferredCenters extends AbstractStructBase
{
    /**
     * The preferredCenter
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var int[]
     */
    protected ?array $preferredCenter = null;
    /**
     * Constructor method for personPreferredCenters
     * @uses PersonPreferredCenters::setPreferredCenter()
     * @param int[] $preferredCenter
     */
    public function __construct(?array $preferredCenter = null)
    {
        $this
            ->setPreferredCenter($preferredCenter);
    }
    /**
     * Get preferredCenter value
     * @return int[]
     */
    public function getPreferredCenter(): ?array
    {
        return $this->preferredCenter;
    }
    /**
     * This method is responsible for validating the values passed to the setPreferredCenter method
     * This method is willingly generated in order to preserve the one-line inline validation within the setPreferredCenter method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validatePreferredCenterForArrayConstraintsFromSetPreferredCenter(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $personPreferredCentersPreferredCenterItem) {
            // validation for constraint: itemType
            if (!(is_int($personPreferredCentersPreferredCenterItem) || ctype_digit($personPreferredCentersPreferredCenterItem))) {
                $invalidValues[] = is_object($personPreferredCentersPreferredCenterItem) ? get_class($personPreferredCentersPreferredCenterItem) : sprintf('%s(%s)', gettype($personPreferredCentersPreferredCenterItem), var_export($personPreferredCentersPreferredCenterItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The preferredCenter property can only contain items of type int, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set preferredCenter value
     * @throws InvalidArgumentException
     * @param int[] $preferredCenter
     * @return \Exerp\Person\StructType\PersonPreferredCenters
     */
    public function setPreferredCenter(?array $preferredCenter = null): self
    {
        // validation for constraint: array
        if ('' !== ($preferredCenterArrayErrorMessage = self::validatePreferredCenterForArrayConstraintsFromSetPreferredCenter($preferredCenter))) {
            throw new InvalidArgumentException($preferredCenterArrayErrorMessage, __LINE__);
        }
        $this->preferredCenter = $preferredCenter;
        
        return $this;
    }
    /**
     * Add item to preferredCenter value
     * @throws InvalidArgumentException
     * @param int $item
     * @return \Exerp\Person\StructType\PersonPreferredCenters
     */
    public function addToPreferredCenter(int $item): self
    {
        // validation for constraint: itemType
        if (!(is_int($item) || ctype_digit($item))) {
            throw new InvalidArgumentException(sprintf('The preferredCenter property can only contain items of type int, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->preferredCenter[] = $item;
        
        return $this;
    }
}
