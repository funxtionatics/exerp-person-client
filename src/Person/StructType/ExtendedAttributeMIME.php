<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for extendedAttributeMIME StructType
 * @subpackage Structs
 */
class ExtendedAttributeMIME extends AbstractStructBase
{
    /**
     * The id
     * @var string|null
     */
    protected ?string $id = null;
    /**
     * The document
     * @var \Exerp\Person\StructType\MimeDocument|null
     */
    protected ?\Exerp\Person\StructType\MimeDocument $document = null;
    /**
     * Constructor method for extendedAttributeMIME
     * @uses ExtendedAttributeMIME::setId()
     * @uses ExtendedAttributeMIME::setDocument()
     * @param string $id
     * @param \Exerp\Person\StructType\MimeDocument $document
     */
    public function __construct(?string $id = null, ?\Exerp\Person\StructType\MimeDocument $document = null)
    {
        $this
            ->setId($id)
            ->setDocument($document);
    }
    /**
     * Get id value
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }
    /**
     * Set id value
     * @param string $id
     * @return \Exerp\Person\StructType\ExtendedAttributeMIME
     */
    public function setId(?string $id = null): self
    {
        // validation for constraint: string
        if (!is_null($id) && !is_string($id)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($id, true), gettype($id)), __LINE__);
        }
        $this->id = $id;
        
        return $this;
    }
    /**
     * Get document value
     * @return \Exerp\Person\StructType\MimeDocument|null
     */
    public function getDocument(): ?\Exerp\Person\StructType\MimeDocument
    {
        return $this->document;
    }
    /**
     * Set document value
     * @param \Exerp\Person\StructType\MimeDocument $document
     * @return \Exerp\Person\StructType\ExtendedAttributeMIME
     */
    public function setDocument(?\Exerp\Person\StructType\MimeDocument $document = null): self
    {
        $this->document = $document;
        
        return $this;
    }
}
