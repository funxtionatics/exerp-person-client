<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for journalNoteResponse StructType
 * @subpackage Structs
 */
class JournalNoteResponse extends AbstractStructBase
{
    /**
     * The journalNotes
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\JournalNotes|null
     */
    protected ?\Exerp\Person\StructType\JournalNotes $journalNotes = null;
    /**
     * Constructor method for journalNoteResponse
     * @uses JournalNoteResponse::setJournalNotes()
     * @param \Exerp\Person\StructType\JournalNotes $journalNotes
     */
    public function __construct(?\Exerp\Person\StructType\JournalNotes $journalNotes = null)
    {
        $this
            ->setJournalNotes($journalNotes);
    }
    /**
     * Get journalNotes value
     * @return \Exerp\Person\StructType\JournalNotes|null
     */
    public function getJournalNotes(): ?\Exerp\Person\StructType\JournalNotes
    {
        return $this->journalNotes;
    }
    /**
     * Set journalNotes value
     * @param \Exerp\Person\StructType\JournalNotes $journalNotes
     * @return \Exerp\Person\StructType\JournalNoteResponse
     */
    public function setJournalNotes(?\Exerp\Person\StructType\JournalNotes $journalNotes = null): self
    {
        $this->journalNotes = $journalNotes;
        
        return $this;
    }
}
