<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for createCustomJournalDocumentResponse StructType
 * Meta information extracted from the WSDL
 * - final: extension restriction
 * @subpackage Structs
 */
class CreateCustomJournalDocumentResponse extends AbstractStructBase
{
    /**
     * The journalDocumentId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $journalDocumentId = null;
    /**
     * Constructor method for createCustomJournalDocumentResponse
     * @uses CreateCustomJournalDocumentResponse::setJournalDocumentId()
     * @param int $journalDocumentId
     */
    public function __construct(?int $journalDocumentId = null)
    {
        $this
            ->setJournalDocumentId($journalDocumentId);
    }
    /**
     * Get journalDocumentId value
     * @return int|null
     */
    public function getJournalDocumentId(): ?int
    {
        return $this->journalDocumentId;
    }
    /**
     * Set journalDocumentId value
     * @param int $journalDocumentId
     * @return \Exerp\Person\StructType\CreateCustomJournalDocumentResponse
     */
    public function setJournalDocumentId(?int $journalDocumentId = null): self
    {
        // validation for constraint: int
        if (!is_null($journalDocumentId) && !(is_int($journalDocumentId) || ctype_digit($journalDocumentId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($journalDocumentId, true), gettype($journalDocumentId)), __LINE__);
        }
        $this->journalDocumentId = $journalDocumentId;
        
        return $this;
    }
}
