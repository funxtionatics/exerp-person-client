<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for personTypeAndStatus StructType
 * @subpackage Structs
 */
class PersonTypeAndStatus extends AbstractStructBase
{
    /**
     * The blacklisted
     * @var bool|null
     */
    protected ?bool $blacklisted = null;
    /**
     * The companyAgreementId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\CompositeSubKey|null
     */
    protected ?\Exerp\Person\StructType\CompositeSubKey $companyAgreementId = null;
    /**
     * The internalComment
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $internalComment = null;
    /**
     * The messageToMember
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $messageToMember = null;
    /**
     * The personId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\ApiPersonKey|null
     */
    protected ?\Exerp\Person\StructType\ApiPersonKey $personId = null;
    /**
     * The personStatus
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $personStatus = null;
    /**
     * The personType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $personType = null;
    /**
     * The suspended
     * @var bool|null
     */
    protected ?bool $suspended = null;
    /**
     * Constructor method for personTypeAndStatus
     * @uses PersonTypeAndStatus::setBlacklisted()
     * @uses PersonTypeAndStatus::setCompanyAgreementId()
     * @uses PersonTypeAndStatus::setInternalComment()
     * @uses PersonTypeAndStatus::setMessageToMember()
     * @uses PersonTypeAndStatus::setPersonId()
     * @uses PersonTypeAndStatus::setPersonStatus()
     * @uses PersonTypeAndStatus::setPersonType()
     * @uses PersonTypeAndStatus::setSuspended()
     * @param bool $blacklisted
     * @param \Exerp\Person\StructType\CompositeSubKey $companyAgreementId
     * @param string $internalComment
     * @param string $messageToMember
     * @param \Exerp\Person\StructType\ApiPersonKey $personId
     * @param string $personStatus
     * @param string $personType
     * @param bool $suspended
     */
    public function __construct(?bool $blacklisted = null, ?\Exerp\Person\StructType\CompositeSubKey $companyAgreementId = null, ?string $internalComment = null, ?string $messageToMember = null, ?\Exerp\Person\StructType\ApiPersonKey $personId = null, ?string $personStatus = null, ?string $personType = null, ?bool $suspended = null)
    {
        $this
            ->setBlacklisted($blacklisted)
            ->setCompanyAgreementId($companyAgreementId)
            ->setInternalComment($internalComment)
            ->setMessageToMember($messageToMember)
            ->setPersonId($personId)
            ->setPersonStatus($personStatus)
            ->setPersonType($personType)
            ->setSuspended($suspended);
    }
    /**
     * Get blacklisted value
     * @return bool|null
     */
    public function getBlacklisted(): ?bool
    {
        return $this->blacklisted;
    }
    /**
     * Set blacklisted value
     * @param bool $blacklisted
     * @return \Exerp\Person\StructType\PersonTypeAndStatus
     */
    public function setBlacklisted(?bool $blacklisted = null): self
    {
        // validation for constraint: boolean
        if (!is_null($blacklisted) && !is_bool($blacklisted)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($blacklisted, true), gettype($blacklisted)), __LINE__);
        }
        $this->blacklisted = $blacklisted;
        
        return $this;
    }
    /**
     * Get companyAgreementId value
     * @return \Exerp\Person\StructType\CompositeSubKey|null
     */
    public function getCompanyAgreementId(): ?\Exerp\Person\StructType\CompositeSubKey
    {
        return $this->companyAgreementId;
    }
    /**
     * Set companyAgreementId value
     * @param \Exerp\Person\StructType\CompositeSubKey $companyAgreementId
     * @return \Exerp\Person\StructType\PersonTypeAndStatus
     */
    public function setCompanyAgreementId(?\Exerp\Person\StructType\CompositeSubKey $companyAgreementId = null): self
    {
        $this->companyAgreementId = $companyAgreementId;
        
        return $this;
    }
    /**
     * Get internalComment value
     * @return string|null
     */
    public function getInternalComment(): ?string
    {
        return $this->internalComment;
    }
    /**
     * Set internalComment value
     * @param string $internalComment
     * @return \Exerp\Person\StructType\PersonTypeAndStatus
     */
    public function setInternalComment(?string $internalComment = null): self
    {
        // validation for constraint: string
        if (!is_null($internalComment) && !is_string($internalComment)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($internalComment, true), gettype($internalComment)), __LINE__);
        }
        $this->internalComment = $internalComment;
        
        return $this;
    }
    /**
     * Get messageToMember value
     * @return string|null
     */
    public function getMessageToMember(): ?string
    {
        return $this->messageToMember;
    }
    /**
     * Set messageToMember value
     * @param string $messageToMember
     * @return \Exerp\Person\StructType\PersonTypeAndStatus
     */
    public function setMessageToMember(?string $messageToMember = null): self
    {
        // validation for constraint: string
        if (!is_null($messageToMember) && !is_string($messageToMember)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($messageToMember, true), gettype($messageToMember)), __LINE__);
        }
        $this->messageToMember = $messageToMember;
        
        return $this;
    }
    /**
     * Get personId value
     * @return \Exerp\Person\StructType\ApiPersonKey|null
     */
    public function getPersonId(): ?\Exerp\Person\StructType\ApiPersonKey
    {
        return $this->personId;
    }
    /**
     * Set personId value
     * @param \Exerp\Person\StructType\ApiPersonKey $personId
     * @return \Exerp\Person\StructType\PersonTypeAndStatus
     */
    public function setPersonId(?\Exerp\Person\StructType\ApiPersonKey $personId = null): self
    {
        $this->personId = $personId;
        
        return $this;
    }
    /**
     * Get personStatus value
     * @return string|null
     */
    public function getPersonStatus(): ?string
    {
        return $this->personStatus;
    }
    /**
     * Set personStatus value
     * @uses \Exerp\Person\EnumType\PersonStatus::valueIsValid()
     * @uses \Exerp\Person\EnumType\PersonStatus::getValidValues()
     * @throws InvalidArgumentException
     * @param string $personStatus
     * @return \Exerp\Person\StructType\PersonTypeAndStatus
     */
    public function setPersonStatus(?string $personStatus = null): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Person\EnumType\PersonStatus::valueIsValid($personStatus)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Person\EnumType\PersonStatus', is_array($personStatus) ? implode(', ', $personStatus) : var_export($personStatus, true), implode(', ', \Exerp\Person\EnumType\PersonStatus::getValidValues())), __LINE__);
        }
        $this->personStatus = $personStatus;
        
        return $this;
    }
    /**
     * Get personType value
     * @return string|null
     */
    public function getPersonType(): ?string
    {
        return $this->personType;
    }
    /**
     * Set personType value
     * @uses \Exerp\Person\EnumType\PersonType::valueIsValid()
     * @uses \Exerp\Person\EnumType\PersonType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $personType
     * @return \Exerp\Person\StructType\PersonTypeAndStatus
     */
    public function setPersonType(?string $personType = null): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Person\EnumType\PersonType::valueIsValid($personType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Person\EnumType\PersonType', is_array($personType) ? implode(', ', $personType) : var_export($personType, true), implode(', ', \Exerp\Person\EnumType\PersonType::getValidValues())), __LINE__);
        }
        $this->personType = $personType;
        
        return $this;
    }
    /**
     * Get suspended value
     * @return bool|null
     */
    public function getSuspended(): ?bool
    {
        return $this->suspended;
    }
    /**
     * Set suspended value
     * @param bool $suspended
     * @return \Exerp\Person\StructType\PersonTypeAndStatus
     */
    public function setSuspended(?bool $suspended = null): self
    {
        // validation for constraint: boolean
        if (!is_null($suspended) && !is_bool($suspended)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($suspended, true), gettype($suspended)), __LINE__);
        }
        $this->suspended = $suspended;
        
        return $this;
    }
}
