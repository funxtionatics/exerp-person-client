<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for RelatedPerson StructType
 * Meta information extracted from the WSDL
 * - type: tns:RelatedPerson
 * @subpackage Structs
 */
class RelatedPerson extends AbstractStructBase
{
    /**
     * The relationType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $relationType = null;
    /**
     * The relatedPersonKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\ApiPersonKey|null
     */
    protected ?\Exerp\Person\StructType\ApiPersonKey $relatedPersonKey = null;
    /**
     * The relatedCompanyAgreementKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\CompositeSubKey|null
     */
    protected ?\Exerp\Person\StructType\CompositeSubKey $relatedCompanyAgreementKey = null;
    /**
     * The relatedCompanyKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\CompositeKey|null
     */
    protected ?\Exerp\Person\StructType\CompositeKey $relatedCompanyKey = null;
    /**
     * Constructor method for RelatedPerson
     * @uses RelatedPerson::setRelationType()
     * @uses RelatedPerson::setRelatedPersonKey()
     * @uses RelatedPerson::setRelatedCompanyAgreementKey()
     * @uses RelatedPerson::setRelatedCompanyKey()
     * @param string $relationType
     * @param \Exerp\Person\StructType\ApiPersonKey $relatedPersonKey
     * @param \Exerp\Person\StructType\CompositeSubKey $relatedCompanyAgreementKey
     * @param \Exerp\Person\StructType\CompositeKey $relatedCompanyKey
     */
    public function __construct(?string $relationType = null, ?\Exerp\Person\StructType\ApiPersonKey $relatedPersonKey = null, ?\Exerp\Person\StructType\CompositeSubKey $relatedCompanyAgreementKey = null, ?\Exerp\Person\StructType\CompositeKey $relatedCompanyKey = null)
    {
        $this
            ->setRelationType($relationType)
            ->setRelatedPersonKey($relatedPersonKey)
            ->setRelatedCompanyAgreementKey($relatedCompanyAgreementKey)
            ->setRelatedCompanyKey($relatedCompanyKey);
    }
    /**
     * Get relationType value
     * @return string|null
     */
    public function getRelationType(): ?string
    {
        return $this->relationType;
    }
    /**
     * Set relationType value
     * @uses \Exerp\Person\EnumType\RelationType::valueIsValid()
     * @uses \Exerp\Person\EnumType\RelationType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $relationType
     * @return \Exerp\Person\StructType\RelatedPerson
     */
    public function setRelationType(?string $relationType = null): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Person\EnumType\RelationType::valueIsValid($relationType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Person\EnumType\RelationType', is_array($relationType) ? implode(', ', $relationType) : var_export($relationType, true), implode(', ', \Exerp\Person\EnumType\RelationType::getValidValues())), __LINE__);
        }
        $this->relationType = $relationType;
        
        return $this;
    }
    /**
     * Get relatedPersonKey value
     * @return \Exerp\Person\StructType\ApiPersonKey|null
     */
    public function getRelatedPersonKey(): ?\Exerp\Person\StructType\ApiPersonKey
    {
        return $this->relatedPersonKey;
    }
    /**
     * Set relatedPersonKey value
     * @param \Exerp\Person\StructType\ApiPersonKey $relatedPersonKey
     * @return \Exerp\Person\StructType\RelatedPerson
     */
    public function setRelatedPersonKey(?\Exerp\Person\StructType\ApiPersonKey $relatedPersonKey = null): self
    {
        $this->relatedPersonKey = $relatedPersonKey;
        
        return $this;
    }
    /**
     * Get relatedCompanyAgreementKey value
     * @return \Exerp\Person\StructType\CompositeSubKey|null
     */
    public function getRelatedCompanyAgreementKey(): ?\Exerp\Person\StructType\CompositeSubKey
    {
        return $this->relatedCompanyAgreementKey;
    }
    /**
     * Set relatedCompanyAgreementKey value
     * @param \Exerp\Person\StructType\CompositeSubKey $relatedCompanyAgreementKey
     * @return \Exerp\Person\StructType\RelatedPerson
     */
    public function setRelatedCompanyAgreementKey(?\Exerp\Person\StructType\CompositeSubKey $relatedCompanyAgreementKey = null): self
    {
        $this->relatedCompanyAgreementKey = $relatedCompanyAgreementKey;
        
        return $this;
    }
    /**
     * Get relatedCompanyKey value
     * @return \Exerp\Person\StructType\CompositeKey|null
     */
    public function getRelatedCompanyKey(): ?\Exerp\Person\StructType\CompositeKey
    {
        return $this->relatedCompanyKey;
    }
    /**
     * Set relatedCompanyKey value
     * @param \Exerp\Person\StructType\CompositeKey $relatedCompanyKey
     * @return \Exerp\Person\StructType\RelatedPerson
     */
    public function setRelatedCompanyKey(?\Exerp\Person\StructType\CompositeKey $relatedCompanyKey = null): self
    {
        $this->relatedCompanyKey = $relatedCompanyKey;
        
        return $this;
    }
}
