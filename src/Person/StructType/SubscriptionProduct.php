<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for subscriptionProduct StructType
 * @subpackage Structs
 */
class SubscriptionProduct extends AbstractStructBase
{
    /**
     * The description
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $description = null;
    /**
     * The externalId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $externalId = null;
    /**
     * The globalId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $globalId = null;
    /**
     * The mostPopular
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $mostPopular = null;
    /**
     * The name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $name = null;
    /**
     * The periodLength
     * @var int|null
     */
    protected ?int $periodLength = null;
    /**
     * The periodUnit
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $periodUnit = null;
    /**
     * The rank
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $rank = null;
    /**
     * The sellingPoints
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var string[]
     */
    protected ?array $sellingPoints = null;
    /**
     * The type
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * The webname
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $webname = null;
    /**
     * Constructor method for subscriptionProduct
     * @uses SubscriptionProduct::setDescription()
     * @uses SubscriptionProduct::setExternalId()
     * @uses SubscriptionProduct::setGlobalId()
     * @uses SubscriptionProduct::setMostPopular()
     * @uses SubscriptionProduct::setName()
     * @uses SubscriptionProduct::setPeriodLength()
     * @uses SubscriptionProduct::setPeriodUnit()
     * @uses SubscriptionProduct::setRank()
     * @uses SubscriptionProduct::setSellingPoints()
     * @uses SubscriptionProduct::setType()
     * @uses SubscriptionProduct::setWebname()
     * @param string $description
     * @param string $externalId
     * @param string $globalId
     * @param bool $mostPopular
     * @param string $name
     * @param int $periodLength
     * @param string $periodUnit
     * @param int $rank
     * @param string[] $sellingPoints
     * @param string $type
     * @param string $webname
     */
    public function __construct(?string $description = null, ?string $externalId = null, ?string $globalId = null, ?bool $mostPopular = null, ?string $name = null, ?int $periodLength = null, ?string $periodUnit = null, ?int $rank = null, ?array $sellingPoints = null, ?string $type = null, ?string $webname = null)
    {
        $this
            ->setDescription($description)
            ->setExternalId($externalId)
            ->setGlobalId($globalId)
            ->setMostPopular($mostPopular)
            ->setName($name)
            ->setPeriodLength($periodLength)
            ->setPeriodUnit($periodUnit)
            ->setRank($rank)
            ->setSellingPoints($sellingPoints)
            ->setType($type)
            ->setWebname($webname);
    }
    /**
     * Get description value
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }
    /**
     * Set description value
     * @param string $description
     * @return \Exerp\Person\StructType\SubscriptionProduct
     */
    public function setDescription(?string $description = null): self
    {
        // validation for constraint: string
        if (!is_null($description) && !is_string($description)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($description, true), gettype($description)), __LINE__);
        }
        $this->description = $description;
        
        return $this;
    }
    /**
     * Get externalId value
     * @return string|null
     */
    public function getExternalId(): ?string
    {
        return $this->externalId;
    }
    /**
     * Set externalId value
     * @param string $externalId
     * @return \Exerp\Person\StructType\SubscriptionProduct
     */
    public function setExternalId(?string $externalId = null): self
    {
        // validation for constraint: string
        if (!is_null($externalId) && !is_string($externalId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($externalId, true), gettype($externalId)), __LINE__);
        }
        $this->externalId = $externalId;
        
        return $this;
    }
    /**
     * Get globalId value
     * @return string|null
     */
    public function getGlobalId(): ?string
    {
        return $this->globalId;
    }
    /**
     * Set globalId value
     * @param string $globalId
     * @return \Exerp\Person\StructType\SubscriptionProduct
     */
    public function setGlobalId(?string $globalId = null): self
    {
        // validation for constraint: string
        if (!is_null($globalId) && !is_string($globalId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($globalId, true), gettype($globalId)), __LINE__);
        }
        $this->globalId = $globalId;
        
        return $this;
    }
    /**
     * Get mostPopular value
     * @return bool|null
     */
    public function getMostPopular(): ?bool
    {
        return $this->mostPopular;
    }
    /**
     * Set mostPopular value
     * @param bool $mostPopular
     * @return \Exerp\Person\StructType\SubscriptionProduct
     */
    public function setMostPopular(?bool $mostPopular = null): self
    {
        // validation for constraint: boolean
        if (!is_null($mostPopular) && !is_bool($mostPopular)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($mostPopular, true), gettype($mostPopular)), __LINE__);
        }
        $this->mostPopular = $mostPopular;
        
        return $this;
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \Exerp\Person\StructType\SubscriptionProduct
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        
        return $this;
    }
    /**
     * Get periodLength value
     * @return int|null
     */
    public function getPeriodLength(): ?int
    {
        return $this->periodLength;
    }
    /**
     * Set periodLength value
     * @param int $periodLength
     * @return \Exerp\Person\StructType\SubscriptionProduct
     */
    public function setPeriodLength(?int $periodLength = null): self
    {
        // validation for constraint: int
        if (!is_null($periodLength) && !(is_int($periodLength) || ctype_digit($periodLength))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($periodLength, true), gettype($periodLength)), __LINE__);
        }
        $this->periodLength = $periodLength;
        
        return $this;
    }
    /**
     * Get periodUnit value
     * @return string|null
     */
    public function getPeriodUnit(): ?string
    {
        return $this->periodUnit;
    }
    /**
     * Set periodUnit value
     * @uses \Exerp\Person\EnumType\TimeUnit::valueIsValid()
     * @uses \Exerp\Person\EnumType\TimeUnit::getValidValues()
     * @throws InvalidArgumentException
     * @param string $periodUnit
     * @return \Exerp\Person\StructType\SubscriptionProduct
     */
    public function setPeriodUnit(?string $periodUnit = null): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Person\EnumType\TimeUnit::valueIsValid($periodUnit)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Person\EnumType\TimeUnit', is_array($periodUnit) ? implode(', ', $periodUnit) : var_export($periodUnit, true), implode(', ', \Exerp\Person\EnumType\TimeUnit::getValidValues())), __LINE__);
        }
        $this->periodUnit = $periodUnit;
        
        return $this;
    }
    /**
     * Get rank value
     * @return int|null
     */
    public function getRank(): ?int
    {
        return $this->rank;
    }
    /**
     * Set rank value
     * @param int $rank
     * @return \Exerp\Person\StructType\SubscriptionProduct
     */
    public function setRank(?int $rank = null): self
    {
        // validation for constraint: int
        if (!is_null($rank) && !(is_int($rank) || ctype_digit($rank))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($rank, true), gettype($rank)), __LINE__);
        }
        $this->rank = $rank;
        
        return $this;
    }
    /**
     * Get sellingPoints value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string[]
     */
    public function getSellingPoints(): ?array
    {
        return isset($this->sellingPoints) ? $this->sellingPoints : null;
    }
    /**
     * This method is responsible for validating the values passed to the setSellingPoints method
     * This method is willingly generated in order to preserve the one-line inline validation within the setSellingPoints method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateSellingPointsForArrayConstraintsFromSetSellingPoints(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $subscriptionProductSellingPointsItem) {
            // validation for constraint: itemType
            if (!is_string($subscriptionProductSellingPointsItem)) {
                $invalidValues[] = is_object($subscriptionProductSellingPointsItem) ? get_class($subscriptionProductSellingPointsItem) : sprintf('%s(%s)', gettype($subscriptionProductSellingPointsItem), var_export($subscriptionProductSellingPointsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The sellingPoints property can only contain items of type string, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set sellingPoints value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param string[] $sellingPoints
     * @return \Exerp\Person\StructType\SubscriptionProduct
     */
    public function setSellingPoints(?array $sellingPoints = null): self
    {
        // validation for constraint: array
        if ('' !== ($sellingPointsArrayErrorMessage = self::validateSellingPointsForArrayConstraintsFromSetSellingPoints($sellingPoints))) {
            throw new InvalidArgumentException($sellingPointsArrayErrorMessage, __LINE__);
        }
        if (is_null($sellingPoints) || (is_array($sellingPoints) && empty($sellingPoints))) {
            unset($this->sellingPoints);
        } else {
            $this->sellingPoints = $sellingPoints;
        }
        
        return $this;
    }
    /**
     * Add item to sellingPoints value
     * @throws InvalidArgumentException
     * @param string $item
     * @return \Exerp\Person\StructType\SubscriptionProduct
     */
    public function addToSellingPoints(string $item): self
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new InvalidArgumentException(sprintf('The sellingPoints property can only contain items of type string, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->sellingPoints[] = $item;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @uses \Exerp\Person\EnumType\SubscriptionType::valueIsValid()
     * @uses \Exerp\Person\EnumType\SubscriptionType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $type
     * @return \Exerp\Person\StructType\SubscriptionProduct
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Person\EnumType\SubscriptionType::valueIsValid($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Person\EnumType\SubscriptionType', is_array($type) ? implode(', ', $type) : var_export($type, true), implode(', ', \Exerp\Person\EnumType\SubscriptionType::getValidValues())), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
    /**
     * Get webname value
     * @return string|null
     */
    public function getWebname(): ?string
    {
        return $this->webname;
    }
    /**
     * Set webname value
     * @param string $webname
     * @return \Exerp\Person\StructType\SubscriptionProduct
     */
    public function setWebname(?string $webname = null): self
    {
        // validation for constraint: string
        if (!is_null($webname) && !is_string($webname)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($webname, true), gettype($webname)), __LINE__);
        }
        $this->webname = $webname;
        
        return $this;
    }
}
