<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for companyAgreement StructType
 * @subpackage Structs
 */
class CompanyAgreement extends AbstractStructBase
{
    /**
     * The allowedCorporateRelationTypes
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\AllowedCorporateRelationTypes|null
     */
    protected ?\Exerp\Person\StructType\AllowedCorporateRelationTypes $allowedCorporateRelationTypes = null;
    /**
     * The availabilityCenters
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \Exerp\Person\StructType\Center[]
     */
    protected ?array $availabilityCenters = null;
    /**
     * The companyAgreementId
     * @var \Exerp\Person\StructType\CompositeSubKey|null
     */
    protected ?\Exerp\Person\StructType\CompositeSubKey $companyAgreementId = null;
    /**
     * The companyKey
     * @var \Exerp\Person\StructType\CompositeKey|null
     */
    protected ?\Exerp\Person\StructType\CompositeKey $companyKey = null;
    /**
     * The contactPerson
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\ApiPersonKey|null
     */
    protected ?\Exerp\Person\StructType\ApiPersonKey $contactPerson = null;
    /**
     * The documentationRequired
     * @var bool|null
     */
    protected ?bool $documentationRequired = null;
    /**
     * The employeeNumberRequired
     * @var bool|null
     */
    protected ?bool $employeeNumberRequired = null;
    /**
     * The maximumFamilyMembers
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $maximumFamilyMembers = null;
    /**
     * The name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $name = null;
    /**
     * The privilegeSets
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \Exerp\Person\StructType\PrivilegeSetAndSponsorship[]
     */
    protected ?array $privilegeSets = null;
    /**
     * The ref
     * @var string|null
     */
    protected ?string $ref = null;
    /**
     * The requiresOtherPayer
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $requiresOtherPayer = null;
    /**
     * The state
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $state = null;
    /**
     * The validity
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $validity = null;
    /**
     * The validityUnit
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $validityUnit = null;
    /**
     * Constructor method for companyAgreement
     * @uses CompanyAgreement::setAllowedCorporateRelationTypes()
     * @uses CompanyAgreement::setAvailabilityCenters()
     * @uses CompanyAgreement::setCompanyAgreementId()
     * @uses CompanyAgreement::setCompanyKey()
     * @uses CompanyAgreement::setContactPerson()
     * @uses CompanyAgreement::setDocumentationRequired()
     * @uses CompanyAgreement::setEmployeeNumberRequired()
     * @uses CompanyAgreement::setMaximumFamilyMembers()
     * @uses CompanyAgreement::setName()
     * @uses CompanyAgreement::setPrivilegeSets()
     * @uses CompanyAgreement::setRef()
     * @uses CompanyAgreement::setRequiresOtherPayer()
     * @uses CompanyAgreement::setState()
     * @uses CompanyAgreement::setValidity()
     * @uses CompanyAgreement::setValidityUnit()
     * @param \Exerp\Person\StructType\AllowedCorporateRelationTypes $allowedCorporateRelationTypes
     * @param \Exerp\Person\StructType\Center[] $availabilityCenters
     * @param \Exerp\Person\StructType\CompositeSubKey $companyAgreementId
     * @param \Exerp\Person\StructType\CompositeKey $companyKey
     * @param \Exerp\Person\StructType\ApiPersonKey $contactPerson
     * @param bool $documentationRequired
     * @param bool $employeeNumberRequired
     * @param int $maximumFamilyMembers
     * @param string $name
     * @param \Exerp\Person\StructType\PrivilegeSetAndSponsorship[] $privilegeSets
     * @param string $ref
     * @param bool $requiresOtherPayer
     * @param string $state
     * @param int $validity
     * @param string $validityUnit
     */
    public function __construct(?\Exerp\Person\StructType\AllowedCorporateRelationTypes $allowedCorporateRelationTypes = null, ?array $availabilityCenters = null, ?\Exerp\Person\StructType\CompositeSubKey $companyAgreementId = null, ?\Exerp\Person\StructType\CompositeKey $companyKey = null, ?\Exerp\Person\StructType\ApiPersonKey $contactPerson = null, ?bool $documentationRequired = null, ?bool $employeeNumberRequired = null, ?int $maximumFamilyMembers = null, ?string $name = null, ?array $privilegeSets = null, ?string $ref = null, ?bool $requiresOtherPayer = null, ?string $state = null, ?int $validity = null, ?string $validityUnit = null)
    {
        $this
            ->setAllowedCorporateRelationTypes($allowedCorporateRelationTypes)
            ->setAvailabilityCenters($availabilityCenters)
            ->setCompanyAgreementId($companyAgreementId)
            ->setCompanyKey($companyKey)
            ->setContactPerson($contactPerson)
            ->setDocumentationRequired($documentationRequired)
            ->setEmployeeNumberRequired($employeeNumberRequired)
            ->setMaximumFamilyMembers($maximumFamilyMembers)
            ->setName($name)
            ->setPrivilegeSets($privilegeSets)
            ->setRef($ref)
            ->setRequiresOtherPayer($requiresOtherPayer)
            ->setState($state)
            ->setValidity($validity)
            ->setValidityUnit($validityUnit);
    }
    /**
     * Get allowedCorporateRelationTypes value
     * @return \Exerp\Person\StructType\AllowedCorporateRelationTypes|null
     */
    public function getAllowedCorporateRelationTypes(): ?\Exerp\Person\StructType\AllowedCorporateRelationTypes
    {
        return $this->allowedCorporateRelationTypes;
    }
    /**
     * Set allowedCorporateRelationTypes value
     * @param \Exerp\Person\StructType\AllowedCorporateRelationTypes $allowedCorporateRelationTypes
     * @return \Exerp\Person\StructType\CompanyAgreement
     */
    public function setAllowedCorporateRelationTypes(?\Exerp\Person\StructType\AllowedCorporateRelationTypes $allowedCorporateRelationTypes = null): self
    {
        $this->allowedCorporateRelationTypes = $allowedCorporateRelationTypes;
        
        return $this;
    }
    /**
     * Get availabilityCenters value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Exerp\Person\StructType\Center[]
     */
    public function getAvailabilityCenters(): ?array
    {
        return isset($this->availabilityCenters) ? $this->availabilityCenters : null;
    }
    /**
     * This method is responsible for validating the values passed to the setAvailabilityCenters method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAvailabilityCenters method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAvailabilityCentersForArrayConstraintsFromSetAvailabilityCenters(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $companyAgreementAvailabilityCentersItem) {
            // validation for constraint: itemType
            if (!$companyAgreementAvailabilityCentersItem instanceof \Exerp\Person\StructType\Center) {
                $invalidValues[] = is_object($companyAgreementAvailabilityCentersItem) ? get_class($companyAgreementAvailabilityCentersItem) : sprintf('%s(%s)', gettype($companyAgreementAvailabilityCentersItem), var_export($companyAgreementAvailabilityCentersItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The availabilityCenters property can only contain items of type \Exerp\Person\StructType\Center, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set availabilityCenters value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\Center[] $availabilityCenters
     * @return \Exerp\Person\StructType\CompanyAgreement
     */
    public function setAvailabilityCenters(?array $availabilityCenters = null): self
    {
        // validation for constraint: array
        if ('' !== ($availabilityCentersArrayErrorMessage = self::validateAvailabilityCentersForArrayConstraintsFromSetAvailabilityCenters($availabilityCenters))) {
            throw new InvalidArgumentException($availabilityCentersArrayErrorMessage, __LINE__);
        }
        if (is_null($availabilityCenters) || (is_array($availabilityCenters) && empty($availabilityCenters))) {
            unset($this->availabilityCenters);
        } else {
            $this->availabilityCenters = $availabilityCenters;
        }
        
        return $this;
    }
    /**
     * Add item to availabilityCenters value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\Center $item
     * @return \Exerp\Person\StructType\CompanyAgreement
     */
    public function addToAvailabilityCenters(\Exerp\Person\StructType\Center $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Exerp\Person\StructType\Center) {
            throw new InvalidArgumentException(sprintf('The availabilityCenters property can only contain items of type \Exerp\Person\StructType\Center, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->availabilityCenters[] = $item;
        
        return $this;
    }
    /**
     * Get companyAgreementId value
     * @return \Exerp\Person\StructType\CompositeSubKey|null
     */
    public function getCompanyAgreementId(): ?\Exerp\Person\StructType\CompositeSubKey
    {
        return $this->companyAgreementId;
    }
    /**
     * Set companyAgreementId value
     * @param \Exerp\Person\StructType\CompositeSubKey $companyAgreementId
     * @return \Exerp\Person\StructType\CompanyAgreement
     */
    public function setCompanyAgreementId(?\Exerp\Person\StructType\CompositeSubKey $companyAgreementId = null): self
    {
        $this->companyAgreementId = $companyAgreementId;
        
        return $this;
    }
    /**
     * Get companyKey value
     * @return \Exerp\Person\StructType\CompositeKey|null
     */
    public function getCompanyKey(): ?\Exerp\Person\StructType\CompositeKey
    {
        return $this->companyKey;
    }
    /**
     * Set companyKey value
     * @param \Exerp\Person\StructType\CompositeKey $companyKey
     * @return \Exerp\Person\StructType\CompanyAgreement
     */
    public function setCompanyKey(?\Exerp\Person\StructType\CompositeKey $companyKey = null): self
    {
        $this->companyKey = $companyKey;
        
        return $this;
    }
    /**
     * Get contactPerson value
     * @return \Exerp\Person\StructType\ApiPersonKey|null
     */
    public function getContactPerson(): ?\Exerp\Person\StructType\ApiPersonKey
    {
        return $this->contactPerson;
    }
    /**
     * Set contactPerson value
     * @param \Exerp\Person\StructType\ApiPersonKey $contactPerson
     * @return \Exerp\Person\StructType\CompanyAgreement
     */
    public function setContactPerson(?\Exerp\Person\StructType\ApiPersonKey $contactPerson = null): self
    {
        $this->contactPerson = $contactPerson;
        
        return $this;
    }
    /**
     * Get documentationRequired value
     * @return bool|null
     */
    public function getDocumentationRequired(): ?bool
    {
        return $this->documentationRequired;
    }
    /**
     * Set documentationRequired value
     * @param bool $documentationRequired
     * @return \Exerp\Person\StructType\CompanyAgreement
     */
    public function setDocumentationRequired(?bool $documentationRequired = null): self
    {
        // validation for constraint: boolean
        if (!is_null($documentationRequired) && !is_bool($documentationRequired)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($documentationRequired, true), gettype($documentationRequired)), __LINE__);
        }
        $this->documentationRequired = $documentationRequired;
        
        return $this;
    }
    /**
     * Get employeeNumberRequired value
     * @return bool|null
     */
    public function getEmployeeNumberRequired(): ?bool
    {
        return $this->employeeNumberRequired;
    }
    /**
     * Set employeeNumberRequired value
     * @param bool $employeeNumberRequired
     * @return \Exerp\Person\StructType\CompanyAgreement
     */
    public function setEmployeeNumberRequired(?bool $employeeNumberRequired = null): self
    {
        // validation for constraint: boolean
        if (!is_null($employeeNumberRequired) && !is_bool($employeeNumberRequired)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($employeeNumberRequired, true), gettype($employeeNumberRequired)), __LINE__);
        }
        $this->employeeNumberRequired = $employeeNumberRequired;
        
        return $this;
    }
    /**
     * Get maximumFamilyMembers value
     * @return int|null
     */
    public function getMaximumFamilyMembers(): ?int
    {
        return $this->maximumFamilyMembers;
    }
    /**
     * Set maximumFamilyMembers value
     * @param int $maximumFamilyMembers
     * @return \Exerp\Person\StructType\CompanyAgreement
     */
    public function setMaximumFamilyMembers(?int $maximumFamilyMembers = null): self
    {
        // validation for constraint: int
        if (!is_null($maximumFamilyMembers) && !(is_int($maximumFamilyMembers) || ctype_digit($maximumFamilyMembers))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($maximumFamilyMembers, true), gettype($maximumFamilyMembers)), __LINE__);
        }
        $this->maximumFamilyMembers = $maximumFamilyMembers;
        
        return $this;
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \Exerp\Person\StructType\CompanyAgreement
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        
        return $this;
    }
    /**
     * Get privilegeSets value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Exerp\Person\StructType\PrivilegeSetAndSponsorship[]
     */
    public function getPrivilegeSets(): ?array
    {
        return isset($this->privilegeSets) ? $this->privilegeSets : null;
    }
    /**
     * This method is responsible for validating the values passed to the setPrivilegeSets method
     * This method is willingly generated in order to preserve the one-line inline validation within the setPrivilegeSets method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validatePrivilegeSetsForArrayConstraintsFromSetPrivilegeSets(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $companyAgreementPrivilegeSetsItem) {
            // validation for constraint: itemType
            if (!$companyAgreementPrivilegeSetsItem instanceof \Exerp\Person\StructType\PrivilegeSetAndSponsorship) {
                $invalidValues[] = is_object($companyAgreementPrivilegeSetsItem) ? get_class($companyAgreementPrivilegeSetsItem) : sprintf('%s(%s)', gettype($companyAgreementPrivilegeSetsItem), var_export($companyAgreementPrivilegeSetsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The privilegeSets property can only contain items of type \Exerp\Person\StructType\PrivilegeSetAndSponsorship, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set privilegeSets value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\PrivilegeSetAndSponsorship[] $privilegeSets
     * @return \Exerp\Person\StructType\CompanyAgreement
     */
    public function setPrivilegeSets(?array $privilegeSets = null): self
    {
        // validation for constraint: array
        if ('' !== ($privilegeSetsArrayErrorMessage = self::validatePrivilegeSetsForArrayConstraintsFromSetPrivilegeSets($privilegeSets))) {
            throw new InvalidArgumentException($privilegeSetsArrayErrorMessage, __LINE__);
        }
        if (is_null($privilegeSets) || (is_array($privilegeSets) && empty($privilegeSets))) {
            unset($this->privilegeSets);
        } else {
            $this->privilegeSets = $privilegeSets;
        }
        
        return $this;
    }
    /**
     * Add item to privilegeSets value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\PrivilegeSetAndSponsorship $item
     * @return \Exerp\Person\StructType\CompanyAgreement
     */
    public function addToPrivilegeSets(\Exerp\Person\StructType\PrivilegeSetAndSponsorship $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Exerp\Person\StructType\PrivilegeSetAndSponsorship) {
            throw new InvalidArgumentException(sprintf('The privilegeSets property can only contain items of type \Exerp\Person\StructType\PrivilegeSetAndSponsorship, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->privilegeSets[] = $item;
        
        return $this;
    }
    /**
     * Get ref value
     * @return string|null
     */
    public function getRef(): ?string
    {
        return $this->ref;
    }
    /**
     * Set ref value
     * @param string $ref
     * @return \Exerp\Person\StructType\CompanyAgreement
     */
    public function setRef(?string $ref = null): self
    {
        // validation for constraint: string
        if (!is_null($ref) && !is_string($ref)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ref, true), gettype($ref)), __LINE__);
        }
        $this->ref = $ref;
        
        return $this;
    }
    /**
     * Get requiresOtherPayer value
     * @return bool|null
     */
    public function getRequiresOtherPayer(): ?bool
    {
        return $this->requiresOtherPayer;
    }
    /**
     * Set requiresOtherPayer value
     * @param bool $requiresOtherPayer
     * @return \Exerp\Person\StructType\CompanyAgreement
     */
    public function setRequiresOtherPayer(?bool $requiresOtherPayer = null): self
    {
        // validation for constraint: boolean
        if (!is_null($requiresOtherPayer) && !is_bool($requiresOtherPayer)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($requiresOtherPayer, true), gettype($requiresOtherPayer)), __LINE__);
        }
        $this->requiresOtherPayer = $requiresOtherPayer;
        
        return $this;
    }
    /**
     * Get state value
     * @return string|null
     */
    public function getState(): ?string
    {
        return $this->state;
    }
    /**
     * Set state value
     * @uses \Exerp\Person\EnumType\CompanyAgreementStateType::valueIsValid()
     * @uses \Exerp\Person\EnumType\CompanyAgreementStateType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $state
     * @return \Exerp\Person\StructType\CompanyAgreement
     */
    public function setState(?string $state = null): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Person\EnumType\CompanyAgreementStateType::valueIsValid($state)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Person\EnumType\CompanyAgreementStateType', is_array($state) ? implode(', ', $state) : var_export($state, true), implode(', ', \Exerp\Person\EnumType\CompanyAgreementStateType::getValidValues())), __LINE__);
        }
        $this->state = $state;
        
        return $this;
    }
    /**
     * Get validity value
     * @return int|null
     */
    public function getValidity(): ?int
    {
        return $this->validity;
    }
    /**
     * Set validity value
     * @param int $validity
     * @return \Exerp\Person\StructType\CompanyAgreement
     */
    public function setValidity(?int $validity = null): self
    {
        // validation for constraint: int
        if (!is_null($validity) && !(is_int($validity) || ctype_digit($validity))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($validity, true), gettype($validity)), __LINE__);
        }
        $this->validity = $validity;
        
        return $this;
    }
    /**
     * Get validityUnit value
     * @return string|null
     */
    public function getValidityUnit(): ?string
    {
        return $this->validityUnit;
    }
    /**
     * Set validityUnit value
     * @uses \Exerp\Person\EnumType\TimeUnit::valueIsValid()
     * @uses \Exerp\Person\EnumType\TimeUnit::getValidValues()
     * @throws InvalidArgumentException
     * @param string $validityUnit
     * @return \Exerp\Person\StructType\CompanyAgreement
     */
    public function setValidityUnit(?string $validityUnit = null): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Person\EnumType\TimeUnit::valueIsValid($validityUnit)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Person\EnumType\TimeUnit', is_array($validityUnit) ? implode(', ', $validityUnit) : var_export($validityUnit, true), implode(', ', \Exerp\Person\EnumType\TimeUnit::getValidValues())), __LINE__);
        }
        $this->validityUnit = $validityUnit;
        
        return $this;
    }
}
