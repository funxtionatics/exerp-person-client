<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for getAvailableSalutationsParameters StructType
 * @subpackage Structs
 */
class GetAvailableSalutationsParameters extends AbstractStructBase
{
    /**
     * The scopeKey
     * @var \Exerp\Person\StructType\ApiScopeKey|null
     */
    protected ?\Exerp\Person\StructType\ApiScopeKey $scopeKey = null;
    /**
     * Constructor method for getAvailableSalutationsParameters
     * @uses GetAvailableSalutationsParameters::setScopeKey()
     * @param \Exerp\Person\StructType\ApiScopeKey $scopeKey
     */
    public function __construct(?\Exerp\Person\StructType\ApiScopeKey $scopeKey = null)
    {
        $this
            ->setScopeKey($scopeKey);
    }
    /**
     * Get scopeKey value
     * @return \Exerp\Person\StructType\ApiScopeKey|null
     */
    public function getScopeKey(): ?\Exerp\Person\StructType\ApiScopeKey
    {
        return $this->scopeKey;
    }
    /**
     * Set scopeKey value
     * @param \Exerp\Person\StructType\ApiScopeKey $scopeKey
     * @return \Exerp\Person\StructType\GetAvailableSalutationsParameters
     */
    public function setScopeKey(?\Exerp\Person\StructType\ApiScopeKey $scopeKey = null): self
    {
        $this->scopeKey = $scopeKey;
        
        return $this;
    }
}
