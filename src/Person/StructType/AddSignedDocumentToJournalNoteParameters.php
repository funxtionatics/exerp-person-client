<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for addSignedDocumentToJournalNoteParameters StructType
 * @subpackage Structs
 */
class AddSignedDocumentToJournalNoteParameters extends AbstractStructBase
{
    /**
     * The journalNoteId
     * @var int|null
     */
    protected ?int $journalNoteId = null;
    /**
     * The signedDocument
     * @var \Exerp\Person\StructType\MimeDocument|null
     */
    protected ?\Exerp\Person\StructType\MimeDocument $signedDocument = null;
    /**
     * The signatureDatas
     * @var \Exerp\Person\StructType\SignatureDatas|null
     */
    protected ?\Exerp\Person\StructType\SignatureDatas $signatureDatas = null;
    /**
     * Constructor method for addSignedDocumentToJournalNoteParameters
     * @uses AddSignedDocumentToJournalNoteParameters::setJournalNoteId()
     * @uses AddSignedDocumentToJournalNoteParameters::setSignedDocument()
     * @uses AddSignedDocumentToJournalNoteParameters::setSignatureDatas()
     * @param int $journalNoteId
     * @param \Exerp\Person\StructType\MimeDocument $signedDocument
     * @param \Exerp\Person\StructType\SignatureDatas $signatureDatas
     */
    public function __construct(?int $journalNoteId = null, ?\Exerp\Person\StructType\MimeDocument $signedDocument = null, ?\Exerp\Person\StructType\SignatureDatas $signatureDatas = null)
    {
        $this
            ->setJournalNoteId($journalNoteId)
            ->setSignedDocument($signedDocument)
            ->setSignatureDatas($signatureDatas);
    }
    /**
     * Get journalNoteId value
     * @return int|null
     */
    public function getJournalNoteId(): ?int
    {
        return $this->journalNoteId;
    }
    /**
     * Set journalNoteId value
     * @param int $journalNoteId
     * @return \Exerp\Person\StructType\AddSignedDocumentToJournalNoteParameters
     */
    public function setJournalNoteId(?int $journalNoteId = null): self
    {
        // validation for constraint: int
        if (!is_null($journalNoteId) && !(is_int($journalNoteId) || ctype_digit($journalNoteId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($journalNoteId, true), gettype($journalNoteId)), __LINE__);
        }
        $this->journalNoteId = $journalNoteId;
        
        return $this;
    }
    /**
     * Get signedDocument value
     * @return \Exerp\Person\StructType\MimeDocument|null
     */
    public function getSignedDocument(): ?\Exerp\Person\StructType\MimeDocument
    {
        return $this->signedDocument;
    }
    /**
     * Set signedDocument value
     * @param \Exerp\Person\StructType\MimeDocument $signedDocument
     * @return \Exerp\Person\StructType\AddSignedDocumentToJournalNoteParameters
     */
    public function setSignedDocument(?\Exerp\Person\StructType\MimeDocument $signedDocument = null): self
    {
        $this->signedDocument = $signedDocument;
        
        return $this;
    }
    /**
     * Get signatureDatas value
     * @return \Exerp\Person\StructType\SignatureDatas|null
     */
    public function getSignatureDatas(): ?\Exerp\Person\StructType\SignatureDatas
    {
        return $this->signatureDatas;
    }
    /**
     * Set signatureDatas value
     * @param \Exerp\Person\StructType\SignatureDatas $signatureDatas
     * @return \Exerp\Person\StructType\AddSignedDocumentToJournalNoteParameters
     */
    public function setSignatureDatas(?\Exerp\Person\StructType\SignatureDatas $signatureDatas = null): self
    {
        $this->signatureDatas = $signatureDatas;
        
        return $this;
    }
}
