<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for centers StructType
 * @subpackage Structs
 */
class Centers extends AbstractStructBase
{
    /**
     * The center
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\CenterWithSalutations[]
     */
    protected ?array $center = null;
    /**
     * Constructor method for centers
     * @uses Centers::setCenter()
     * @param \Exerp\Person\StructType\CenterWithSalutations[] $center
     */
    public function __construct(?array $center = null)
    {
        $this
            ->setCenter($center);
    }
    /**
     * Get center value
     * @return \Exerp\Person\StructType\CenterWithSalutations[]
     */
    public function getCenter(): ?array
    {
        return $this->center;
    }
    /**
     * This method is responsible for validating the values passed to the setCenter method
     * This method is willingly generated in order to preserve the one-line inline validation within the setCenter method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateCenterForArrayConstraintsFromSetCenter(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $centersCenterItem) {
            // validation for constraint: itemType
            if (!$centersCenterItem instanceof \Exerp\Person\StructType\CenterWithSalutations) {
                $invalidValues[] = is_object($centersCenterItem) ? get_class($centersCenterItem) : sprintf('%s(%s)', gettype($centersCenterItem), var_export($centersCenterItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The center property can only contain items of type \Exerp\Person\StructType\CenterWithSalutations, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set center value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\CenterWithSalutations[] $center
     * @return \Exerp\Person\StructType\Centers
     */
    public function setCenter(?array $center = null): self
    {
        // validation for constraint: array
        if ('' !== ($centerArrayErrorMessage = self::validateCenterForArrayConstraintsFromSetCenter($center))) {
            throw new InvalidArgumentException($centerArrayErrorMessage, __LINE__);
        }
        $this->center = $center;
        
        return $this;
    }
    /**
     * Add item to center value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\CenterWithSalutations $item
     * @return \Exerp\Person\StructType\Centers
     */
    public function addToCenter(\Exerp\Person\StructType\CenterWithSalutations $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Exerp\Person\StructType\CenterWithSalutations) {
            throw new InvalidArgumentException(sprintf('The center property can only contain items of type \Exerp\Person\StructType\CenterWithSalutations, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->center[] = $item;
        
        return $this;
    }
}
