<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for subscriptions StructType
 * @subpackage Structs
 */
class Subscriptions extends AbstractStructBase
{
    /**
     * The subscription
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\Subscription[]
     */
    protected ?array $subscription = null;
    /**
     * Constructor method for subscriptions
     * @uses Subscriptions::setSubscription()
     * @param \Exerp\Person\StructType\Subscription[] $subscription
     */
    public function __construct(?array $subscription = null)
    {
        $this
            ->setSubscription($subscription);
    }
    /**
     * Get subscription value
     * @return \Exerp\Person\StructType\Subscription[]
     */
    public function getSubscription(): ?array
    {
        return $this->subscription;
    }
    /**
     * This method is responsible for validating the values passed to the setSubscription method
     * This method is willingly generated in order to preserve the one-line inline validation within the setSubscription method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateSubscriptionForArrayConstraintsFromSetSubscription(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $subscriptionsSubscriptionItem) {
            // validation for constraint: itemType
            if (!$subscriptionsSubscriptionItem instanceof \Exerp\Person\StructType\Subscription) {
                $invalidValues[] = is_object($subscriptionsSubscriptionItem) ? get_class($subscriptionsSubscriptionItem) : sprintf('%s(%s)', gettype($subscriptionsSubscriptionItem), var_export($subscriptionsSubscriptionItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The subscription property can only contain items of type \Exerp\Person\StructType\Subscription, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set subscription value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\Subscription[] $subscription
     * @return \Exerp\Person\StructType\Subscriptions
     */
    public function setSubscription(?array $subscription = null): self
    {
        // validation for constraint: array
        if ('' !== ($subscriptionArrayErrorMessage = self::validateSubscriptionForArrayConstraintsFromSetSubscription($subscription))) {
            throw new InvalidArgumentException($subscriptionArrayErrorMessage, __LINE__);
        }
        $this->subscription = $subscription;
        
        return $this;
    }
    /**
     * Add item to subscription value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\Subscription $item
     * @return \Exerp\Person\StructType\Subscriptions
     */
    public function addToSubscription(\Exerp\Person\StructType\Subscription $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Exerp\Person\StructType\Subscription) {
            throw new InvalidArgumentException(sprintf('The subscription property can only contain items of type \Exerp\Person\StructType\Subscription, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->subscription[] = $item;
        
        return $this;
    }
}
