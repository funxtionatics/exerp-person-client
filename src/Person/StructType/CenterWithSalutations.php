<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for centerWithSalutations StructType
 * @subpackage Structs
 */
class CenterWithSalutations extends AbstractStructBase
{
    /**
     * The id
     * @var int|null
     */
    protected ?int $id = null;
    /**
     * The availableSalutations
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\AvailableSalutations|null
     */
    protected ?\Exerp\Person\StructType\AvailableSalutations $availableSalutations = null;
    /**
     * Constructor method for centerWithSalutations
     * @uses CenterWithSalutations::setId()
     * @uses CenterWithSalutations::setAvailableSalutations()
     * @param int $id
     * @param \Exerp\Person\StructType\AvailableSalutations $availableSalutations
     */
    public function __construct(?int $id = null, ?\Exerp\Person\StructType\AvailableSalutations $availableSalutations = null)
    {
        $this
            ->setId($id)
            ->setAvailableSalutations($availableSalutations);
    }
    /**
     * Get id value
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }
    /**
     * Set id value
     * @param int $id
     * @return \Exerp\Person\StructType\CenterWithSalutations
     */
    public function setId(?int $id = null): self
    {
        // validation for constraint: int
        if (!is_null($id) && !(is_int($id) || ctype_digit($id))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($id, true), gettype($id)), __LINE__);
        }
        $this->id = $id;
        
        return $this;
    }
    /**
     * Get availableSalutations value
     * @return \Exerp\Person\StructType\AvailableSalutations|null
     */
    public function getAvailableSalutations(): ?\Exerp\Person\StructType\AvailableSalutations
    {
        return $this->availableSalutations;
    }
    /**
     * Set availableSalutations value
     * @param \Exerp\Person\StructType\AvailableSalutations $availableSalutations
     * @return \Exerp\Person\StructType\CenterWithSalutations
     */
    public function setAvailableSalutations(?\Exerp\Person\StructType\AvailableSalutations $availableSalutations = null): self
    {
        $this->availableSalutations = $availableSalutations;
        
        return $this;
    }
}
