<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for assignedAddOn StructType
 * @subpackage Structs
 */
class AssignedAddOn extends AbstractStructBase
{
    /**
     * The endDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $endDate = null;
    /**
     * The initialPeriodEndDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $initialPeriodEndDate = null;
    /**
     * The initialPeriodPrice
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $initialPeriodPrice = null;
    /**
     * The initialPeriodStartDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $initialPeriodStartDate = null;
    /**
     * The key
     * @var int|null
     */
    protected ?int $key = null;
    /**
     * The name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $name = null;
    /**
     * The price
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $price = null;
    /**
     * The pricePeriodLength
     * @var int|null
     */
    protected ?int $pricePeriodLength = null;
    /**
     * The pricePeriodUnit
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $pricePeriodUnit = null;
    /**
     * The startDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $startDate = null;
    /**
     * The startupCampaignEndDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $startupCampaignEndDate = null;
    /**
     * The startupCampaignPrice
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $startupCampaignPrice = null;
    /**
     * Constructor method for assignedAddOn
     * @uses AssignedAddOn::setEndDate()
     * @uses AssignedAddOn::setInitialPeriodEndDate()
     * @uses AssignedAddOn::setInitialPeriodPrice()
     * @uses AssignedAddOn::setInitialPeriodStartDate()
     * @uses AssignedAddOn::setKey()
     * @uses AssignedAddOn::setName()
     * @uses AssignedAddOn::setPrice()
     * @uses AssignedAddOn::setPricePeriodLength()
     * @uses AssignedAddOn::setPricePeriodUnit()
     * @uses AssignedAddOn::setStartDate()
     * @uses AssignedAddOn::setStartupCampaignEndDate()
     * @uses AssignedAddOn::setStartupCampaignPrice()
     * @param string $endDate
     * @param string $initialPeriodEndDate
     * @param float $initialPeriodPrice
     * @param string $initialPeriodStartDate
     * @param int $key
     * @param string $name
     * @param string $price
     * @param int $pricePeriodLength
     * @param string $pricePeriodUnit
     * @param string $startDate
     * @param string $startupCampaignEndDate
     * @param string $startupCampaignPrice
     */
    public function __construct(?string $endDate = null, ?string $initialPeriodEndDate = null, ?float $initialPeriodPrice = null, ?string $initialPeriodStartDate = null, ?int $key = null, ?string $name = null, ?string $price = null, ?int $pricePeriodLength = null, ?string $pricePeriodUnit = null, ?string $startDate = null, ?string $startupCampaignEndDate = null, ?string $startupCampaignPrice = null)
    {
        $this
            ->setEndDate($endDate)
            ->setInitialPeriodEndDate($initialPeriodEndDate)
            ->setInitialPeriodPrice($initialPeriodPrice)
            ->setInitialPeriodStartDate($initialPeriodStartDate)
            ->setKey($key)
            ->setName($name)
            ->setPrice($price)
            ->setPricePeriodLength($pricePeriodLength)
            ->setPricePeriodUnit($pricePeriodUnit)
            ->setStartDate($startDate)
            ->setStartupCampaignEndDate($startupCampaignEndDate)
            ->setStartupCampaignPrice($startupCampaignPrice);
    }
    /**
     * Get endDate value
     * @return string|null
     */
    public function getEndDate(): ?string
    {
        return $this->endDate;
    }
    /**
     * Set endDate value
     * @param string $endDate
     * @return \Exerp\Person\StructType\AssignedAddOn
     */
    public function setEndDate(?string $endDate = null): self
    {
        // validation for constraint: string
        if (!is_null($endDate) && !is_string($endDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($endDate, true), gettype($endDate)), __LINE__);
        }
        $this->endDate = $endDate;
        
        return $this;
    }
    /**
     * Get initialPeriodEndDate value
     * @return string|null
     */
    public function getInitialPeriodEndDate(): ?string
    {
        return $this->initialPeriodEndDate;
    }
    /**
     * Set initialPeriodEndDate value
     * @param string $initialPeriodEndDate
     * @return \Exerp\Person\StructType\AssignedAddOn
     */
    public function setInitialPeriodEndDate(?string $initialPeriodEndDate = null): self
    {
        // validation for constraint: string
        if (!is_null($initialPeriodEndDate) && !is_string($initialPeriodEndDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($initialPeriodEndDate, true), gettype($initialPeriodEndDate)), __LINE__);
        }
        $this->initialPeriodEndDate = $initialPeriodEndDate;
        
        return $this;
    }
    /**
     * Get initialPeriodPrice value
     * @return float|null
     */
    public function getInitialPeriodPrice(): ?float
    {
        return $this->initialPeriodPrice;
    }
    /**
     * Set initialPeriodPrice value
     * @param float $initialPeriodPrice
     * @return \Exerp\Person\StructType\AssignedAddOn
     */
    public function setInitialPeriodPrice(?float $initialPeriodPrice = null): self
    {
        // validation for constraint: float
        if (!is_null($initialPeriodPrice) && !(is_float($initialPeriodPrice) || is_numeric($initialPeriodPrice))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($initialPeriodPrice, true), gettype($initialPeriodPrice)), __LINE__);
        }
        $this->initialPeriodPrice = $initialPeriodPrice;
        
        return $this;
    }
    /**
     * Get initialPeriodStartDate value
     * @return string|null
     */
    public function getInitialPeriodStartDate(): ?string
    {
        return $this->initialPeriodStartDate;
    }
    /**
     * Set initialPeriodStartDate value
     * @param string $initialPeriodStartDate
     * @return \Exerp\Person\StructType\AssignedAddOn
     */
    public function setInitialPeriodStartDate(?string $initialPeriodStartDate = null): self
    {
        // validation for constraint: string
        if (!is_null($initialPeriodStartDate) && !is_string($initialPeriodStartDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($initialPeriodStartDate, true), gettype($initialPeriodStartDate)), __LINE__);
        }
        $this->initialPeriodStartDate = $initialPeriodStartDate;
        
        return $this;
    }
    /**
     * Get key value
     * @return int|null
     */
    public function getKey(): ?int
    {
        return $this->key;
    }
    /**
     * Set key value
     * @param int $key
     * @return \Exerp\Person\StructType\AssignedAddOn
     */
    public function setKey(?int $key = null): self
    {
        // validation for constraint: int
        if (!is_null($key) && !(is_int($key) || ctype_digit($key))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($key, true), gettype($key)), __LINE__);
        }
        $this->key = $key;
        
        return $this;
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \Exerp\Person\StructType\AssignedAddOn
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        
        return $this;
    }
    /**
     * Get price value
     * @return string|null
     */
    public function getPrice(): ?string
    {
        return $this->price;
    }
    /**
     * Set price value
     * @param string $price
     * @return \Exerp\Person\StructType\AssignedAddOn
     */
    public function setPrice(?string $price = null): self
    {
        // validation for constraint: string
        if (!is_null($price) && !is_string($price)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($price, true), gettype($price)), __LINE__);
        }
        $this->price = $price;
        
        return $this;
    }
    /**
     * Get pricePeriodLength value
     * @return int|null
     */
    public function getPricePeriodLength(): ?int
    {
        return $this->pricePeriodLength;
    }
    /**
     * Set pricePeriodLength value
     * @param int $pricePeriodLength
     * @return \Exerp\Person\StructType\AssignedAddOn
     */
    public function setPricePeriodLength(?int $pricePeriodLength = null): self
    {
        // validation for constraint: int
        if (!is_null($pricePeriodLength) && !(is_int($pricePeriodLength) || ctype_digit($pricePeriodLength))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($pricePeriodLength, true), gettype($pricePeriodLength)), __LINE__);
        }
        $this->pricePeriodLength = $pricePeriodLength;
        
        return $this;
    }
    /**
     * Get pricePeriodUnit value
     * @return string|null
     */
    public function getPricePeriodUnit(): ?string
    {
        return $this->pricePeriodUnit;
    }
    /**
     * Set pricePeriodUnit value
     * @uses \Exerp\Person\EnumType\TimeUnit::valueIsValid()
     * @uses \Exerp\Person\EnumType\TimeUnit::getValidValues()
     * @throws InvalidArgumentException
     * @param string $pricePeriodUnit
     * @return \Exerp\Person\StructType\AssignedAddOn
     */
    public function setPricePeriodUnit(?string $pricePeriodUnit = null): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Person\EnumType\TimeUnit::valueIsValid($pricePeriodUnit)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Person\EnumType\TimeUnit', is_array($pricePeriodUnit) ? implode(', ', $pricePeriodUnit) : var_export($pricePeriodUnit, true), implode(', ', \Exerp\Person\EnumType\TimeUnit::getValidValues())), __LINE__);
        }
        $this->pricePeriodUnit = $pricePeriodUnit;
        
        return $this;
    }
    /**
     * Get startDate value
     * @return string|null
     */
    public function getStartDate(): ?string
    {
        return $this->startDate;
    }
    /**
     * Set startDate value
     * @param string $startDate
     * @return \Exerp\Person\StructType\AssignedAddOn
     */
    public function setStartDate(?string $startDate = null): self
    {
        // validation for constraint: string
        if (!is_null($startDate) && !is_string($startDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($startDate, true), gettype($startDate)), __LINE__);
        }
        $this->startDate = $startDate;
        
        return $this;
    }
    /**
     * Get startupCampaignEndDate value
     * @return string|null
     */
    public function getStartupCampaignEndDate(): ?string
    {
        return $this->startupCampaignEndDate;
    }
    /**
     * Set startupCampaignEndDate value
     * @param string $startupCampaignEndDate
     * @return \Exerp\Person\StructType\AssignedAddOn
     */
    public function setStartupCampaignEndDate(?string $startupCampaignEndDate = null): self
    {
        // validation for constraint: string
        if (!is_null($startupCampaignEndDate) && !is_string($startupCampaignEndDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($startupCampaignEndDate, true), gettype($startupCampaignEndDate)), __LINE__);
        }
        $this->startupCampaignEndDate = $startupCampaignEndDate;
        
        return $this;
    }
    /**
     * Get startupCampaignPrice value
     * @return string|null
     */
    public function getStartupCampaignPrice(): ?string
    {
        return $this->startupCampaignPrice;
    }
    /**
     * Set startupCampaignPrice value
     * @param string $startupCampaignPrice
     * @return \Exerp\Person\StructType\AssignedAddOn
     */
    public function setStartupCampaignPrice(?string $startupCampaignPrice = null): self
    {
        // validation for constraint: string
        if (!is_null($startupCampaignPrice) && !is_string($startupCampaignPrice)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($startupCampaignPrice, true), gettype($startupCampaignPrice)), __LINE__);
        }
        $this->startupCampaignPrice = $startupCampaignPrice;
        
        return $this;
    }
}
