<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for friends StructType
 * @subpackage Structs
 */
class Friends extends AbstractStructBase
{
    /**
     * The friend
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\Friend[]
     */
    protected ?array $friend = null;
    /**
     * Constructor method for friends
     * @uses Friends::setFriend()
     * @param \Exerp\Person\StructType\Friend[] $friend
     */
    public function __construct(?array $friend = null)
    {
        $this
            ->setFriend($friend);
    }
    /**
     * Get friend value
     * @return \Exerp\Person\StructType\Friend[]
     */
    public function getFriend(): ?array
    {
        return $this->friend;
    }
    /**
     * This method is responsible for validating the values passed to the setFriend method
     * This method is willingly generated in order to preserve the one-line inline validation within the setFriend method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateFriendForArrayConstraintsFromSetFriend(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $friendsFriendItem) {
            // validation for constraint: itemType
            if (!$friendsFriendItem instanceof \Exerp\Person\StructType\Friend) {
                $invalidValues[] = is_object($friendsFriendItem) ? get_class($friendsFriendItem) : sprintf('%s(%s)', gettype($friendsFriendItem), var_export($friendsFriendItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The friend property can only contain items of type \Exerp\Person\StructType\Friend, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set friend value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\Friend[] $friend
     * @return \Exerp\Person\StructType\Friends
     */
    public function setFriend(?array $friend = null): self
    {
        // validation for constraint: array
        if ('' !== ($friendArrayErrorMessage = self::validateFriendForArrayConstraintsFromSetFriend($friend))) {
            throw new InvalidArgumentException($friendArrayErrorMessage, __LINE__);
        }
        $this->friend = $friend;
        
        return $this;
    }
    /**
     * Add item to friend value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\Friend $item
     * @return \Exerp\Person\StructType\Friends
     */
    public function addToFriend(\Exerp\Person\StructType\Friend $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Exerp\Person\StructType\Friend) {
            throw new InvalidArgumentException(sprintf('The friend property can only contain items of type \Exerp\Person\StructType\Friend, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->friend[] = $item;
        
        return $this;
    }
}
