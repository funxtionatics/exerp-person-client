<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for createCustomJournalDocumentParameters StructType
 * @subpackage Structs
 */
class CreateCustomJournalDocumentParameters extends AbstractStructBase
{
    /**
     * The personKey
     * @var \Exerp\Person\StructType\ApiPersonKey|null
     */
    protected ?\Exerp\Person\StructType\ApiPersonKey $personKey = null;
    /**
     * The customJournalDocumentTypeId
     * @var int|null
     */
    protected ?int $customJournalDocumentTypeId = null;
    /**
     * The subject
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $subject = null;
    /**
     * The note
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $note = null;
    /**
     * The issueDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $issueDate = null;
    /**
     * The overriddenExpirationDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $overriddenExpirationDate = null;
    /**
     * The document
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\MimeDocument|null
     */
    protected ?\Exerp\Person\StructType\MimeDocument $document = null;
    /**
     * Constructor method for createCustomJournalDocumentParameters
     * @uses CreateCustomJournalDocumentParameters::setPersonKey()
     * @uses CreateCustomJournalDocumentParameters::setCustomJournalDocumentTypeId()
     * @uses CreateCustomJournalDocumentParameters::setSubject()
     * @uses CreateCustomJournalDocumentParameters::setNote()
     * @uses CreateCustomJournalDocumentParameters::setIssueDate()
     * @uses CreateCustomJournalDocumentParameters::setOverriddenExpirationDate()
     * @uses CreateCustomJournalDocumentParameters::setDocument()
     * @param \Exerp\Person\StructType\ApiPersonKey $personKey
     * @param int $customJournalDocumentTypeId
     * @param string $subject
     * @param string $note
     * @param string $issueDate
     * @param string $overriddenExpirationDate
     * @param \Exerp\Person\StructType\MimeDocument $document
     */
    public function __construct(?\Exerp\Person\StructType\ApiPersonKey $personKey = null, ?int $customJournalDocumentTypeId = null, ?string $subject = null, ?string $note = null, ?string $issueDate = null, ?string $overriddenExpirationDate = null, ?\Exerp\Person\StructType\MimeDocument $document = null)
    {
        $this
            ->setPersonKey($personKey)
            ->setCustomJournalDocumentTypeId($customJournalDocumentTypeId)
            ->setSubject($subject)
            ->setNote($note)
            ->setIssueDate($issueDate)
            ->setOverriddenExpirationDate($overriddenExpirationDate)
            ->setDocument($document);
    }
    /**
     * Get personKey value
     * @return \Exerp\Person\StructType\ApiPersonKey|null
     */
    public function getPersonKey(): ?\Exerp\Person\StructType\ApiPersonKey
    {
        return $this->personKey;
    }
    /**
     * Set personKey value
     * @param \Exerp\Person\StructType\ApiPersonKey $personKey
     * @return \Exerp\Person\StructType\CreateCustomJournalDocumentParameters
     */
    public function setPersonKey(?\Exerp\Person\StructType\ApiPersonKey $personKey = null): self
    {
        $this->personKey = $personKey;
        
        return $this;
    }
    /**
     * Get customJournalDocumentTypeId value
     * @return int|null
     */
    public function getCustomJournalDocumentTypeId(): ?int
    {
        return $this->customJournalDocumentTypeId;
    }
    /**
     * Set customJournalDocumentTypeId value
     * @param int $customJournalDocumentTypeId
     * @return \Exerp\Person\StructType\CreateCustomJournalDocumentParameters
     */
    public function setCustomJournalDocumentTypeId(?int $customJournalDocumentTypeId = null): self
    {
        // validation for constraint: int
        if (!is_null($customJournalDocumentTypeId) && !(is_int($customJournalDocumentTypeId) || ctype_digit($customJournalDocumentTypeId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($customJournalDocumentTypeId, true), gettype($customJournalDocumentTypeId)), __LINE__);
        }
        $this->customJournalDocumentTypeId = $customJournalDocumentTypeId;
        
        return $this;
    }
    /**
     * Get subject value
     * @return string|null
     */
    public function getSubject(): ?string
    {
        return $this->subject;
    }
    /**
     * Set subject value
     * @param string $subject
     * @return \Exerp\Person\StructType\CreateCustomJournalDocumentParameters
     */
    public function setSubject(?string $subject = null): self
    {
        // validation for constraint: string
        if (!is_null($subject) && !is_string($subject)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($subject, true), gettype($subject)), __LINE__);
        }
        $this->subject = $subject;
        
        return $this;
    }
    /**
     * Get note value
     * @return string|null
     */
    public function getNote(): ?string
    {
        return $this->note;
    }
    /**
     * Set note value
     * @param string $note
     * @return \Exerp\Person\StructType\CreateCustomJournalDocumentParameters
     */
    public function setNote(?string $note = null): self
    {
        // validation for constraint: string
        if (!is_null($note) && !is_string($note)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($note, true), gettype($note)), __LINE__);
        }
        $this->note = $note;
        
        return $this;
    }
    /**
     * Get issueDate value
     * @return string|null
     */
    public function getIssueDate(): ?string
    {
        return $this->issueDate;
    }
    /**
     * Set issueDate value
     * @param string $issueDate
     * @return \Exerp\Person\StructType\CreateCustomJournalDocumentParameters
     */
    public function setIssueDate(?string $issueDate = null): self
    {
        // validation for constraint: string
        if (!is_null($issueDate) && !is_string($issueDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($issueDate, true), gettype($issueDate)), __LINE__);
        }
        $this->issueDate = $issueDate;
        
        return $this;
    }
    /**
     * Get overriddenExpirationDate value
     * @return string|null
     */
    public function getOverriddenExpirationDate(): ?string
    {
        return $this->overriddenExpirationDate;
    }
    /**
     * Set overriddenExpirationDate value
     * @param string $overriddenExpirationDate
     * @return \Exerp\Person\StructType\CreateCustomJournalDocumentParameters
     */
    public function setOverriddenExpirationDate(?string $overriddenExpirationDate = null): self
    {
        // validation for constraint: string
        if (!is_null($overriddenExpirationDate) && !is_string($overriddenExpirationDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($overriddenExpirationDate, true), gettype($overriddenExpirationDate)), __LINE__);
        }
        $this->overriddenExpirationDate = $overriddenExpirationDate;
        
        return $this;
    }
    /**
     * Get document value
     * @return \Exerp\Person\StructType\MimeDocument|null
     */
    public function getDocument(): ?\Exerp\Person\StructType\MimeDocument
    {
        return $this->document;
    }
    /**
     * Set document value
     * @param \Exerp\Person\StructType\MimeDocument $document
     * @return \Exerp\Person\StructType\CreateCustomJournalDocumentParameters
     */
    public function setDocument(?\Exerp\Person\StructType\MimeDocument $document = null): self
    {
        $this->document = $document;
        
        return $this;
    }
}
