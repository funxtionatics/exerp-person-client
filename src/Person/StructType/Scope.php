<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for scope StructType
 * Meta information extracted from the WSDL
 * - final: extension restriction
 * @subpackage Structs
 */
class Scope extends AbstractStructBase
{
    /**
     * The children
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \Exerp\Person\StructType\Scope[]
     */
    protected ?array $children = null;
    /**
     * The fullName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $fullName = null;
    /**
     * The id
     * @var int|null
     */
    protected ?int $id = null;
    /**
     * The name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $name = null;
    /**
     * The type
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for scope
     * @uses Scope::setChildren()
     * @uses Scope::setFullName()
     * @uses Scope::setId()
     * @uses Scope::setName()
     * @uses Scope::setType()
     * @param \Exerp\Person\StructType\Scope[] $children
     * @param string $fullName
     * @param int $id
     * @param string $name
     * @param string $type
     */
    public function __construct(?array $children = null, ?string $fullName = null, ?int $id = null, ?string $name = null, ?string $type = null)
    {
        $this
            ->setChildren($children)
            ->setFullName($fullName)
            ->setId($id)
            ->setName($name)
            ->setType($type);
    }
    /**
     * Get children value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Exerp\Person\StructType\Scope[]
     */
    public function getChildren(): ?array
    {
        return isset($this->children) ? $this->children : null;
    }
    /**
     * This method is responsible for validating the values passed to the setChildren method
     * This method is willingly generated in order to preserve the one-line inline validation within the setChildren method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateChildrenForArrayConstraintsFromSetChildren(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $scopeChildrenItem) {
            // validation for constraint: itemType
            if (!$scopeChildrenItem instanceof \Exerp\Person\StructType\Scope) {
                $invalidValues[] = is_object($scopeChildrenItem) ? get_class($scopeChildrenItem) : sprintf('%s(%s)', gettype($scopeChildrenItem), var_export($scopeChildrenItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The children property can only contain items of type \Exerp\Person\StructType\Scope, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set children value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\Scope[] $children
     * @return \Exerp\Person\StructType\Scope
     */
    public function setChildren(?array $children = null): self
    {
        // validation for constraint: array
        if ('' !== ($childrenArrayErrorMessage = self::validateChildrenForArrayConstraintsFromSetChildren($children))) {
            throw new InvalidArgumentException($childrenArrayErrorMessage, __LINE__);
        }
        if (is_null($children) || (is_array($children) && empty($children))) {
            unset($this->children);
        } else {
            $this->children = $children;
        }
        
        return $this;
    }
    /**
     * Add item to children value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\Scope $item
     * @return \Exerp\Person\StructType\Scope
     */
    public function addToChildren(\Exerp\Person\StructType\Scope $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Exerp\Person\StructType\Scope) {
            throw new InvalidArgumentException(sprintf('The children property can only contain items of type \Exerp\Person\StructType\Scope, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->children[] = $item;
        
        return $this;
    }
    /**
     * Get fullName value
     * @return string|null
     */
    public function getFullName(): ?string
    {
        return $this->fullName;
    }
    /**
     * Set fullName value
     * @param string $fullName
     * @return \Exerp\Person\StructType\Scope
     */
    public function setFullName(?string $fullName = null): self
    {
        // validation for constraint: string
        if (!is_null($fullName) && !is_string($fullName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($fullName, true), gettype($fullName)), __LINE__);
        }
        $this->fullName = $fullName;
        
        return $this;
    }
    /**
     * Get id value
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }
    /**
     * Set id value
     * @param int $id
     * @return \Exerp\Person\StructType\Scope
     */
    public function setId(?int $id = null): self
    {
        // validation for constraint: int
        if (!is_null($id) && !(is_int($id) || ctype_digit($id))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($id, true), gettype($id)), __LINE__);
        }
        $this->id = $id;
        
        return $this;
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \Exerp\Person\StructType\Scope
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @uses \Exerp\Person\EnumType\ScopeType::valueIsValid()
     * @uses \Exerp\Person\EnumType\ScopeType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $type
     * @return \Exerp\Person\StructType\Scope
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Person\EnumType\ScopeType::valueIsValid($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Person\EnumType\ScopeType', is_array($type) ? implode(', ', $type) : var_export($type, true), implode(', ', \Exerp\Person\EnumType\ScopeType::getValidValues())), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
