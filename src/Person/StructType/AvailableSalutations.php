<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for availableSalutations StructType
 * @subpackage Structs
 */
class AvailableSalutations extends AbstractStructBase
{
    /**
     * The availableSalutation
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\ApiSalutation[]
     */
    protected ?array $availableSalutation = null;
    /**
     * Constructor method for availableSalutations
     * @uses AvailableSalutations::setAvailableSalutation()
     * @param \Exerp\Person\StructType\ApiSalutation[] $availableSalutation
     */
    public function __construct(?array $availableSalutation = null)
    {
        $this
            ->setAvailableSalutation($availableSalutation);
    }
    /**
     * Get availableSalutation value
     * @return \Exerp\Person\StructType\ApiSalutation[]
     */
    public function getAvailableSalutation(): ?array
    {
        return $this->availableSalutation;
    }
    /**
     * This method is responsible for validating the values passed to the setAvailableSalutation method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAvailableSalutation method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAvailableSalutationForArrayConstraintsFromSetAvailableSalutation(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $availableSalutationsAvailableSalutationItem) {
            // validation for constraint: itemType
            if (!$availableSalutationsAvailableSalutationItem instanceof \Exerp\Person\StructType\ApiSalutation) {
                $invalidValues[] = is_object($availableSalutationsAvailableSalutationItem) ? get_class($availableSalutationsAvailableSalutationItem) : sprintf('%s(%s)', gettype($availableSalutationsAvailableSalutationItem), var_export($availableSalutationsAvailableSalutationItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The availableSalutation property can only contain items of type \Exerp\Person\StructType\ApiSalutation, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set availableSalutation value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\ApiSalutation[] $availableSalutation
     * @return \Exerp\Person\StructType\AvailableSalutations
     */
    public function setAvailableSalutation(?array $availableSalutation = null): self
    {
        // validation for constraint: array
        if ('' !== ($availableSalutationArrayErrorMessage = self::validateAvailableSalutationForArrayConstraintsFromSetAvailableSalutation($availableSalutation))) {
            throw new InvalidArgumentException($availableSalutationArrayErrorMessage, __LINE__);
        }
        $this->availableSalutation = $availableSalutation;
        
        return $this;
    }
    /**
     * Add item to availableSalutation value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\ApiSalutation $item
     * @return \Exerp\Person\StructType\AvailableSalutations
     */
    public function addToAvailableSalutation(\Exerp\Person\StructType\ApiSalutation $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Exerp\Person\StructType\ApiSalutation) {
            throw new InvalidArgumentException(sprintf('The availableSalutation property can only contain items of type \Exerp\Person\StructType\ApiSalutation, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->availableSalutation[] = $item;
        
        return $this;
    }
}
