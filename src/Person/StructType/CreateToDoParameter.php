<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for createToDoParameter StructType
 * @subpackage Structs
 */
class CreateToDoParameter extends AbstractStructBase
{
    /**
     * The assignedPersonKey
     * @var \Exerp\Person\StructType\ApiPersonKey|null
     */
    protected ?\Exerp\Person\StructType\ApiPersonKey $assignedPersonKey = null;
    /**
     * The centerId
     * @var int|null
     */
    protected ?int $centerId = null;
    /**
     * The comment
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $comment = null;
    /**
     * The deadline
     * @var string|null
     */
    protected ?string $deadline = null;
    /**
     * The regardingPersonKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\ApiPersonKey|null
     */
    protected ?\Exerp\Person\StructType\ApiPersonKey $regardingPersonKey = null;
    /**
     * The subject
     * @var string|null
     */
    protected ?string $subject = null;
    /**
     * The toDoGroupExternalId
     * @var string|null
     */
    protected ?string $toDoGroupExternalId = null;
    /**
     * Constructor method for createToDoParameter
     * @uses CreateToDoParameter::setAssignedPersonKey()
     * @uses CreateToDoParameter::setCenterId()
     * @uses CreateToDoParameter::setComment()
     * @uses CreateToDoParameter::setDeadline()
     * @uses CreateToDoParameter::setRegardingPersonKey()
     * @uses CreateToDoParameter::setSubject()
     * @uses CreateToDoParameter::setToDoGroupExternalId()
     * @param \Exerp\Person\StructType\ApiPersonKey $assignedPersonKey
     * @param int $centerId
     * @param string $comment
     * @param string $deadline
     * @param \Exerp\Person\StructType\ApiPersonKey $regardingPersonKey
     * @param string $subject
     * @param string $toDoGroupExternalId
     */
    public function __construct(?\Exerp\Person\StructType\ApiPersonKey $assignedPersonKey = null, ?int $centerId = null, ?string $comment = null, ?string $deadline = null, ?\Exerp\Person\StructType\ApiPersonKey $regardingPersonKey = null, ?string $subject = null, ?string $toDoGroupExternalId = null)
    {
        $this
            ->setAssignedPersonKey($assignedPersonKey)
            ->setCenterId($centerId)
            ->setComment($comment)
            ->setDeadline($deadline)
            ->setRegardingPersonKey($regardingPersonKey)
            ->setSubject($subject)
            ->setToDoGroupExternalId($toDoGroupExternalId);
    }
    /**
     * Get assignedPersonKey value
     * @return \Exerp\Person\StructType\ApiPersonKey|null
     */
    public function getAssignedPersonKey(): ?\Exerp\Person\StructType\ApiPersonKey
    {
        return $this->assignedPersonKey;
    }
    /**
     * Set assignedPersonKey value
     * @param \Exerp\Person\StructType\ApiPersonKey $assignedPersonKey
     * @return \Exerp\Person\StructType\CreateToDoParameter
     */
    public function setAssignedPersonKey(?\Exerp\Person\StructType\ApiPersonKey $assignedPersonKey = null): self
    {
        $this->assignedPersonKey = $assignedPersonKey;
        
        return $this;
    }
    /**
     * Get centerId value
     * @return int|null
     */
    public function getCenterId(): ?int
    {
        return $this->centerId;
    }
    /**
     * Set centerId value
     * @param int $centerId
     * @return \Exerp\Person\StructType\CreateToDoParameter
     */
    public function setCenterId(?int $centerId = null): self
    {
        // validation for constraint: int
        if (!is_null($centerId) && !(is_int($centerId) || ctype_digit($centerId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($centerId, true), gettype($centerId)), __LINE__);
        }
        $this->centerId = $centerId;
        
        return $this;
    }
    /**
     * Get comment value
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }
    /**
     * Set comment value
     * @param string $comment
     * @return \Exerp\Person\StructType\CreateToDoParameter
     */
    public function setComment(?string $comment = null): self
    {
        // validation for constraint: string
        if (!is_null($comment) && !is_string($comment)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($comment, true), gettype($comment)), __LINE__);
        }
        $this->comment = $comment;
        
        return $this;
    }
    /**
     * Get deadline value
     * @return string|null
     */
    public function getDeadline(): ?string
    {
        return $this->deadline;
    }
    /**
     * Set deadline value
     * @param string $deadline
     * @return \Exerp\Person\StructType\CreateToDoParameter
     */
    public function setDeadline(?string $deadline = null): self
    {
        // validation for constraint: string
        if (!is_null($deadline) && !is_string($deadline)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($deadline, true), gettype($deadline)), __LINE__);
        }
        $this->deadline = $deadline;
        
        return $this;
    }
    /**
     * Get regardingPersonKey value
     * @return \Exerp\Person\StructType\ApiPersonKey|null
     */
    public function getRegardingPersonKey(): ?\Exerp\Person\StructType\ApiPersonKey
    {
        return $this->regardingPersonKey;
    }
    /**
     * Set regardingPersonKey value
     * @param \Exerp\Person\StructType\ApiPersonKey $regardingPersonKey
     * @return \Exerp\Person\StructType\CreateToDoParameter
     */
    public function setRegardingPersonKey(?\Exerp\Person\StructType\ApiPersonKey $regardingPersonKey = null): self
    {
        $this->regardingPersonKey = $regardingPersonKey;
        
        return $this;
    }
    /**
     * Get subject value
     * @return string|null
     */
    public function getSubject(): ?string
    {
        return $this->subject;
    }
    /**
     * Set subject value
     * @param string $subject
     * @return \Exerp\Person\StructType\CreateToDoParameter
     */
    public function setSubject(?string $subject = null): self
    {
        // validation for constraint: string
        if (!is_null($subject) && !is_string($subject)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($subject, true), gettype($subject)), __LINE__);
        }
        $this->subject = $subject;
        
        return $this;
    }
    /**
     * Get toDoGroupExternalId value
     * @return string|null
     */
    public function getToDoGroupExternalId(): ?string
    {
        return $this->toDoGroupExternalId;
    }
    /**
     * Set toDoGroupExternalId value
     * @param string $toDoGroupExternalId
     * @return \Exerp\Person\StructType\CreateToDoParameter
     */
    public function setToDoGroupExternalId(?string $toDoGroupExternalId = null): self
    {
        // validation for constraint: string
        if (!is_null($toDoGroupExternalId) && !is_string($toDoGroupExternalId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($toDoGroupExternalId, true), gettype($toDoGroupExternalId)), __LINE__);
        }
        $this->toDoGroupExternalId = $toDoGroupExternalId;
        
        return $this;
    }
}
