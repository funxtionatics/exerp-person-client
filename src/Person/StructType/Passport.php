<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for passport StructType
 * @subpackage Structs
 */
class Passport extends AbstractStructBase
{
    /**
     * The number
     * @var string|null
     */
    protected ?string $number = null;
    /**
     * The issuanceCountry
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $issuanceCountry = null;
    /**
     * The expirationDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $expirationDate = null;
    /**
     * Constructor method for passport
     * @uses Passport::setNumber()
     * @uses Passport::setIssuanceCountry()
     * @uses Passport::setExpirationDate()
     * @param string $number
     * @param string $issuanceCountry
     * @param string $expirationDate
     */
    public function __construct(?string $number = null, ?string $issuanceCountry = null, ?string $expirationDate = null)
    {
        $this
            ->setNumber($number)
            ->setIssuanceCountry($issuanceCountry)
            ->setExpirationDate($expirationDate);
    }
    /**
     * Get number value
     * @return string|null
     */
    public function getNumber(): ?string
    {
        return $this->number;
    }
    /**
     * Set number value
     * @param string $number
     * @return \Exerp\Person\StructType\Passport
     */
    public function setNumber(?string $number = null): self
    {
        // validation for constraint: string
        if (!is_null($number) && !is_string($number)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($number, true), gettype($number)), __LINE__);
        }
        $this->number = $number;
        
        return $this;
    }
    /**
     * Get issuanceCountry value
     * @return string|null
     */
    public function getIssuanceCountry(): ?string
    {
        return $this->issuanceCountry;
    }
    /**
     * Set issuanceCountry value
     * @param string $issuanceCountry
     * @return \Exerp\Person\StructType\Passport
     */
    public function setIssuanceCountry(?string $issuanceCountry = null): self
    {
        // validation for constraint: string
        if (!is_null($issuanceCountry) && !is_string($issuanceCountry)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($issuanceCountry, true), gettype($issuanceCountry)), __LINE__);
        }
        $this->issuanceCountry = $issuanceCountry;
        
        return $this;
    }
    /**
     * Get expirationDate value
     * @return string|null
     */
    public function getExpirationDate(): ?string
    {
        return $this->expirationDate;
    }
    /**
     * Set expirationDate value
     * @param string $expirationDate
     * @return \Exerp\Person\StructType\Passport
     */
    public function setExpirationDate(?string $expirationDate = null): self
    {
        // validation for constraint: string
        if (!is_null($expirationDate) && !is_string($expirationDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($expirationDate, true), gettype($expirationDate)), __LINE__);
        }
        $this->expirationDate = $expirationDate;
        
        return $this;
    }
}
