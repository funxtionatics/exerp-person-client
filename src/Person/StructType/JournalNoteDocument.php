<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for journalNoteDocument StructType
 * @subpackage Structs
 */
class JournalNoteDocument extends AbstractStructBase
{
    /**
     * The journalNoteKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $journalNoteKey = null;
    /**
     * The mimeDocument
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\MimeDocument|null
     */
    protected ?\Exerp\Person\StructType\MimeDocument $mimeDocument = null;
    /**
     * Constructor method for journalNoteDocument
     * @uses JournalNoteDocument::setJournalNoteKey()
     * @uses JournalNoteDocument::setMimeDocument()
     * @param int $journalNoteKey
     * @param \Exerp\Person\StructType\MimeDocument $mimeDocument
     */
    public function __construct(?int $journalNoteKey = null, ?\Exerp\Person\StructType\MimeDocument $mimeDocument = null)
    {
        $this
            ->setJournalNoteKey($journalNoteKey)
            ->setMimeDocument($mimeDocument);
    }
    /**
     * Get journalNoteKey value
     * @return int|null
     */
    public function getJournalNoteKey(): ?int
    {
        return $this->journalNoteKey;
    }
    /**
     * Set journalNoteKey value
     * @param int $journalNoteKey
     * @return \Exerp\Person\StructType\JournalNoteDocument
     */
    public function setJournalNoteKey(?int $journalNoteKey = null): self
    {
        // validation for constraint: int
        if (!is_null($journalNoteKey) && !(is_int($journalNoteKey) || ctype_digit($journalNoteKey))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($journalNoteKey, true), gettype($journalNoteKey)), __LINE__);
        }
        $this->journalNoteKey = $journalNoteKey;
        
        return $this;
    }
    /**
     * Get mimeDocument value
     * @return \Exerp\Person\StructType\MimeDocument|null
     */
    public function getMimeDocument(): ?\Exerp\Person\StructType\MimeDocument
    {
        return $this->mimeDocument;
    }
    /**
     * Set mimeDocument value
     * @param \Exerp\Person\StructType\MimeDocument $mimeDocument
     * @return \Exerp\Person\StructType\JournalNoteDocument
     */
    public function setMimeDocument(?\Exerp\Person\StructType\MimeDocument $mimeDocument = null): self
    {
        $this->mimeDocument = $mimeDocument;
        
        return $this;
    }
}
