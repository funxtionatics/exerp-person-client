<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for personPreferredCentersResponse StructType
 * @subpackage Structs
 */
class PersonPreferredCentersResponse extends AbstractStructBase
{
    /**
     * The personPreferredCenters
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\PersonPreferredCenters|null
     */
    protected ?\Exerp\Person\StructType\PersonPreferredCenters $personPreferredCenters = null;
    /**
     * Constructor method for personPreferredCentersResponse
     * @uses PersonPreferredCentersResponse::setPersonPreferredCenters()
     * @param \Exerp\Person\StructType\PersonPreferredCenters $personPreferredCenters
     */
    public function __construct(?\Exerp\Person\StructType\PersonPreferredCenters $personPreferredCenters = null)
    {
        $this
            ->setPersonPreferredCenters($personPreferredCenters);
    }
    /**
     * Get personPreferredCenters value
     * @return \Exerp\Person\StructType\PersonPreferredCenters|null
     */
    public function getPersonPreferredCenters(): ?\Exerp\Person\StructType\PersonPreferredCenters
    {
        return $this->personPreferredCenters;
    }
    /**
     * Set personPreferredCenters value
     * @param \Exerp\Person\StructType\PersonPreferredCenters $personPreferredCenters
     * @return \Exerp\Person\StructType\PersonPreferredCentersResponse
     */
    public function setPersonPreferredCenters(?\Exerp\Person\StructType\PersonPreferredCenters $personPreferredCenters = null): self
    {
        $this->personPreferredCenters = $personPreferredCenters;
        
        return $this;
    }
}
