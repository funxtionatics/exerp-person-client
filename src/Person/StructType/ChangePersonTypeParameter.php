<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for changePersonTypeParameter StructType
 * @subpackage Structs
 */
class ChangePersonTypeParameter extends AbstractStructBase
{
    /**
     * The companyAgreementId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\CompositeSubKey|null
     */
    protected ?\Exerp\Person\StructType\CompositeSubKey $companyAgreementId = null;
    /**
     * The corporateRelationType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $corporateRelationType = null;
    /**
     * The documentExpireDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $documentExpireDate = null;
    /**
     * The employeeNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $employeeNumber = null;
    /**
     * The personKey
     * @var \Exerp\Person\StructType\ApiPersonKey|null
     */
    protected ?\Exerp\Person\StructType\ApiPersonKey $personKey = null;
    /**
     * The personType
     * @var string|null
     */
    protected ?string $personType = null;
    /**
     * The relatedPersonKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\ApiPersonKey|null
     */
    protected ?\Exerp\Person\StructType\ApiPersonKey $relatedPersonKey = null;
    /**
     * Constructor method for changePersonTypeParameter
     * @uses ChangePersonTypeParameter::setCompanyAgreementId()
     * @uses ChangePersonTypeParameter::setCorporateRelationType()
     * @uses ChangePersonTypeParameter::setDocumentExpireDate()
     * @uses ChangePersonTypeParameter::setEmployeeNumber()
     * @uses ChangePersonTypeParameter::setPersonKey()
     * @uses ChangePersonTypeParameter::setPersonType()
     * @uses ChangePersonTypeParameter::setRelatedPersonKey()
     * @param \Exerp\Person\StructType\CompositeSubKey $companyAgreementId
     * @param string $corporateRelationType
     * @param string $documentExpireDate
     * @param string $employeeNumber
     * @param \Exerp\Person\StructType\ApiPersonKey $personKey
     * @param string $personType
     * @param \Exerp\Person\StructType\ApiPersonKey $relatedPersonKey
     */
    public function __construct(?\Exerp\Person\StructType\CompositeSubKey $companyAgreementId = null, ?string $corporateRelationType = null, ?string $documentExpireDate = null, ?string $employeeNumber = null, ?\Exerp\Person\StructType\ApiPersonKey $personKey = null, ?string $personType = null, ?\Exerp\Person\StructType\ApiPersonKey $relatedPersonKey = null)
    {
        $this
            ->setCompanyAgreementId($companyAgreementId)
            ->setCorporateRelationType($corporateRelationType)
            ->setDocumentExpireDate($documentExpireDate)
            ->setEmployeeNumber($employeeNumber)
            ->setPersonKey($personKey)
            ->setPersonType($personType)
            ->setRelatedPersonKey($relatedPersonKey);
    }
    /**
     * Get companyAgreementId value
     * @return \Exerp\Person\StructType\CompositeSubKey|null
     */
    public function getCompanyAgreementId(): ?\Exerp\Person\StructType\CompositeSubKey
    {
        return $this->companyAgreementId;
    }
    /**
     * Set companyAgreementId value
     * @param \Exerp\Person\StructType\CompositeSubKey $companyAgreementId
     * @return \Exerp\Person\StructType\ChangePersonTypeParameter
     */
    public function setCompanyAgreementId(?\Exerp\Person\StructType\CompositeSubKey $companyAgreementId = null): self
    {
        $this->companyAgreementId = $companyAgreementId;
        
        return $this;
    }
    /**
     * Get corporateRelationType value
     * @return string|null
     */
    public function getCorporateRelationType(): ?string
    {
        return $this->corporateRelationType;
    }
    /**
     * Set corporateRelationType value
     * @uses \Exerp\Person\EnumType\CorporateRelationType::valueIsValid()
     * @uses \Exerp\Person\EnumType\CorporateRelationType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $corporateRelationType
     * @return \Exerp\Person\StructType\ChangePersonTypeParameter
     */
    public function setCorporateRelationType(?string $corporateRelationType = null): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Person\EnumType\CorporateRelationType::valueIsValid($corporateRelationType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Person\EnumType\CorporateRelationType', is_array($corporateRelationType) ? implode(', ', $corporateRelationType) : var_export($corporateRelationType, true), implode(', ', \Exerp\Person\EnumType\CorporateRelationType::getValidValues())), __LINE__);
        }
        $this->corporateRelationType = $corporateRelationType;
        
        return $this;
    }
    /**
     * Get documentExpireDate value
     * @return string|null
     */
    public function getDocumentExpireDate(): ?string
    {
        return $this->documentExpireDate;
    }
    /**
     * Set documentExpireDate value
     * @param string $documentExpireDate
     * @return \Exerp\Person\StructType\ChangePersonTypeParameter
     */
    public function setDocumentExpireDate(?string $documentExpireDate = null): self
    {
        // validation for constraint: string
        if (!is_null($documentExpireDate) && !is_string($documentExpireDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($documentExpireDate, true), gettype($documentExpireDate)), __LINE__);
        }
        $this->documentExpireDate = $documentExpireDate;
        
        return $this;
    }
    /**
     * Get employeeNumber value
     * @return string|null
     */
    public function getEmployeeNumber(): ?string
    {
        return $this->employeeNumber;
    }
    /**
     * Set employeeNumber value
     * @param string $employeeNumber
     * @return \Exerp\Person\StructType\ChangePersonTypeParameter
     */
    public function setEmployeeNumber(?string $employeeNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($employeeNumber) && !is_string($employeeNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($employeeNumber, true), gettype($employeeNumber)), __LINE__);
        }
        $this->employeeNumber = $employeeNumber;
        
        return $this;
    }
    /**
     * Get personKey value
     * @return \Exerp\Person\StructType\ApiPersonKey|null
     */
    public function getPersonKey(): ?\Exerp\Person\StructType\ApiPersonKey
    {
        return $this->personKey;
    }
    /**
     * Set personKey value
     * @param \Exerp\Person\StructType\ApiPersonKey $personKey
     * @return \Exerp\Person\StructType\ChangePersonTypeParameter
     */
    public function setPersonKey(?\Exerp\Person\StructType\ApiPersonKey $personKey = null): self
    {
        $this->personKey = $personKey;
        
        return $this;
    }
    /**
     * Get personType value
     * @return string|null
     */
    public function getPersonType(): ?string
    {
        return $this->personType;
    }
    /**
     * Set personType value
     * @uses \Exerp\Person\EnumType\PersonType::valueIsValid()
     * @uses \Exerp\Person\EnumType\PersonType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $personType
     * @return \Exerp\Person\StructType\ChangePersonTypeParameter
     */
    public function setPersonType(?string $personType = null): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Person\EnumType\PersonType::valueIsValid($personType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Person\EnumType\PersonType', is_array($personType) ? implode(', ', $personType) : var_export($personType, true), implode(', ', \Exerp\Person\EnumType\PersonType::getValidValues())), __LINE__);
        }
        $this->personType = $personType;
        
        return $this;
    }
    /**
     * Get relatedPersonKey value
     * @return \Exerp\Person\StructType\ApiPersonKey|null
     */
    public function getRelatedPersonKey(): ?\Exerp\Person\StructType\ApiPersonKey
    {
        return $this->relatedPersonKey;
    }
    /**
     * Set relatedPersonKey value
     * @param \Exerp\Person\StructType\ApiPersonKey $relatedPersonKey
     * @return \Exerp\Person\StructType\ChangePersonTypeParameter
     */
    public function setRelatedPersonKey(?\Exerp\Person\StructType\ApiPersonKey $relatedPersonKey = null): self
    {
        $this->relatedPersonKey = $relatedPersonKey;
        
        return $this;
    }
}
