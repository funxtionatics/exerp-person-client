<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for toDo StructType
 * @subpackage Structs
 */
class ToDo extends AbstractStructBase
{
    /**
     * The assignedPersonKey
     * @var \Exerp\Person\StructType\ApiPersonKey|null
     */
    protected ?\Exerp\Person\StructType\ApiPersonKey $assignedPersonKey = null;
    /**
     * The createdByKey
     * @var \Exerp\Person\StructType\ApiPersonKey|null
     */
    protected ?\Exerp\Person\StructType\ApiPersonKey $createdByKey = null;
    /**
     * The creationTime
     * @var int|null
     */
    protected ?int $creationTime = null;
    /**
     * The deadline
     * @var string|null
     */
    protected ?string $deadline = null;
    /**
     * The regardingPerson
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\Person|null
     */
    protected ?\Exerp\Person\StructType\Person $regardingPerson = null;
    /**
     * The subject
     * @var string|null
     */
    protected ?string $subject = null;
    /**
     * The toDoExternalId
     * @var string|null
     */
    protected ?string $toDoExternalId = null;
    /**
     * The toDoGroupName
     * @var string|null
     */
    protected ?string $toDoGroupName = null;
    /**
     * The toDoState
     * @var string|null
     */
    protected ?string $toDoState = null;
    /**
     * Constructor method for toDo
     * @uses ToDo::setAssignedPersonKey()
     * @uses ToDo::setCreatedByKey()
     * @uses ToDo::setCreationTime()
     * @uses ToDo::setDeadline()
     * @uses ToDo::setRegardingPerson()
     * @uses ToDo::setSubject()
     * @uses ToDo::setToDoExternalId()
     * @uses ToDo::setToDoGroupName()
     * @uses ToDo::setToDoState()
     * @param \Exerp\Person\StructType\ApiPersonKey $assignedPersonKey
     * @param \Exerp\Person\StructType\ApiPersonKey $createdByKey
     * @param int $creationTime
     * @param string $deadline
     * @param \Exerp\Person\StructType\Person $regardingPerson
     * @param string $subject
     * @param string $toDoExternalId
     * @param string $toDoGroupName
     * @param string $toDoState
     */
    public function __construct(?\Exerp\Person\StructType\ApiPersonKey $assignedPersonKey = null, ?\Exerp\Person\StructType\ApiPersonKey $createdByKey = null, ?int $creationTime = null, ?string $deadline = null, ?\Exerp\Person\StructType\Person $regardingPerson = null, ?string $subject = null, ?string $toDoExternalId = null, ?string $toDoGroupName = null, ?string $toDoState = null)
    {
        $this
            ->setAssignedPersonKey($assignedPersonKey)
            ->setCreatedByKey($createdByKey)
            ->setCreationTime($creationTime)
            ->setDeadline($deadline)
            ->setRegardingPerson($regardingPerson)
            ->setSubject($subject)
            ->setToDoExternalId($toDoExternalId)
            ->setToDoGroupName($toDoGroupName)
            ->setToDoState($toDoState);
    }
    /**
     * Get assignedPersonKey value
     * @return \Exerp\Person\StructType\ApiPersonKey|null
     */
    public function getAssignedPersonKey(): ?\Exerp\Person\StructType\ApiPersonKey
    {
        return $this->assignedPersonKey;
    }
    /**
     * Set assignedPersonKey value
     * @param \Exerp\Person\StructType\ApiPersonKey $assignedPersonKey
     * @return \Exerp\Person\StructType\ToDo
     */
    public function setAssignedPersonKey(?\Exerp\Person\StructType\ApiPersonKey $assignedPersonKey = null): self
    {
        $this->assignedPersonKey = $assignedPersonKey;
        
        return $this;
    }
    /**
     * Get createdByKey value
     * @return \Exerp\Person\StructType\ApiPersonKey|null
     */
    public function getCreatedByKey(): ?\Exerp\Person\StructType\ApiPersonKey
    {
        return $this->createdByKey;
    }
    /**
     * Set createdByKey value
     * @param \Exerp\Person\StructType\ApiPersonKey $createdByKey
     * @return \Exerp\Person\StructType\ToDo
     */
    public function setCreatedByKey(?\Exerp\Person\StructType\ApiPersonKey $createdByKey = null): self
    {
        $this->createdByKey = $createdByKey;
        
        return $this;
    }
    /**
     * Get creationTime value
     * @return int|null
     */
    public function getCreationTime(): ?int
    {
        return $this->creationTime;
    }
    /**
     * Set creationTime value
     * @param int $creationTime
     * @return \Exerp\Person\StructType\ToDo
     */
    public function setCreationTime(?int $creationTime = null): self
    {
        // validation for constraint: int
        if (!is_null($creationTime) && !(is_int($creationTime) || ctype_digit($creationTime))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($creationTime, true), gettype($creationTime)), __LINE__);
        }
        $this->creationTime = $creationTime;
        
        return $this;
    }
    /**
     * Get deadline value
     * @return string|null
     */
    public function getDeadline(): ?string
    {
        return $this->deadline;
    }
    /**
     * Set deadline value
     * @param string $deadline
     * @return \Exerp\Person\StructType\ToDo
     */
    public function setDeadline(?string $deadline = null): self
    {
        // validation for constraint: string
        if (!is_null($deadline) && !is_string($deadline)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($deadline, true), gettype($deadline)), __LINE__);
        }
        $this->deadline = $deadline;
        
        return $this;
    }
    /**
     * Get regardingPerson value
     * @return \Exerp\Person\StructType\Person|null
     */
    public function getRegardingPerson(): ?\Exerp\Person\StructType\Person
    {
        return $this->regardingPerson;
    }
    /**
     * Set regardingPerson value
     * @param \Exerp\Person\StructType\Person $regardingPerson
     * @return \Exerp\Person\StructType\ToDo
     */
    public function setRegardingPerson(?\Exerp\Person\StructType\Person $regardingPerson = null): self
    {
        $this->regardingPerson = $regardingPerson;
        
        return $this;
    }
    /**
     * Get subject value
     * @return string|null
     */
    public function getSubject(): ?string
    {
        return $this->subject;
    }
    /**
     * Set subject value
     * @param string $subject
     * @return \Exerp\Person\StructType\ToDo
     */
    public function setSubject(?string $subject = null): self
    {
        // validation for constraint: string
        if (!is_null($subject) && !is_string($subject)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($subject, true), gettype($subject)), __LINE__);
        }
        $this->subject = $subject;
        
        return $this;
    }
    /**
     * Get toDoExternalId value
     * @return string|null
     */
    public function getToDoExternalId(): ?string
    {
        return $this->toDoExternalId;
    }
    /**
     * Set toDoExternalId value
     * @param string $toDoExternalId
     * @return \Exerp\Person\StructType\ToDo
     */
    public function setToDoExternalId(?string $toDoExternalId = null): self
    {
        // validation for constraint: string
        if (!is_null($toDoExternalId) && !is_string($toDoExternalId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($toDoExternalId, true), gettype($toDoExternalId)), __LINE__);
        }
        $this->toDoExternalId = $toDoExternalId;
        
        return $this;
    }
    /**
     * Get toDoGroupName value
     * @return string|null
     */
    public function getToDoGroupName(): ?string
    {
        return $this->toDoGroupName;
    }
    /**
     * Set toDoGroupName value
     * @param string $toDoGroupName
     * @return \Exerp\Person\StructType\ToDo
     */
    public function setToDoGroupName(?string $toDoGroupName = null): self
    {
        // validation for constraint: string
        if (!is_null($toDoGroupName) && !is_string($toDoGroupName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($toDoGroupName, true), gettype($toDoGroupName)), __LINE__);
        }
        $this->toDoGroupName = $toDoGroupName;
        
        return $this;
    }
    /**
     * Get toDoState value
     * @return string|null
     */
    public function getToDoState(): ?string
    {
        return $this->toDoState;
    }
    /**
     * Set toDoState value
     * @param string $toDoState
     * @return \Exerp\Person\StructType\ToDo
     */
    public function setToDoState(?string $toDoState = null): self
    {
        // validation for constraint: string
        if (!is_null($toDoState) && !is_string($toDoState)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($toDoState, true), gettype($toDoState)), __LINE__);
        }
        $this->toDoState = $toDoState;
        
        return $this;
    }
}
