<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for personDetail StructType
 * @subpackage Structs
 */
class PersonDetail extends AbstractStructBase
{
    /**
     * The allowFriendsToBook
     * @var bool|null
     */
    protected ?bool $allowFriendsToBook = null;
    /**
     * The companyRelation
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\PersonCompanyRelation|null
     */
    protected ?\Exerp\Person\StructType\PersonCompanyRelation $companyRelation = null;
    /**
     * The extendedAttributes
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\ExtendedAttributes|null
     */
    protected ?\Exerp\Person\StructType\ExtendedAttributes $extendedAttributes = null;
    /**
     * The friends
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\Friends|null
     */
    protected ?\Exerp\Person\StructType\Friends $friends = null;
    /**
     * The memberCards
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\MemberCards|null
     */
    protected ?\Exerp\Person\StructType\MemberCards $memberCards = null;
    /**
     * The person
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\Person|null
     */
    protected ?\Exerp\Person\StructType\Person $person = null;
    /**
     * The personActiveDates
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\PersonActivationDates|null
     */
    protected ?\Exerp\Person\StructType\PersonActivationDates $personActiveDates = null;
    /**
     * The personCommunication
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\PersonCommunication|null
     */
    protected ?\Exerp\Person\StructType\PersonCommunication $personCommunication = null;
    /**
     * The personTypeAndStatus
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\PersonTypeAndStatus|null
     */
    protected ?\Exerp\Person\StructType\PersonTypeAndStatus $personTypeAndStatus = null;
    /**
     * The subscriptions
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\Subscriptions|null
     */
    protected ?\Exerp\Person\StructType\Subscriptions $subscriptions = null;
    /**
     * Constructor method for personDetail
     * @uses PersonDetail::setAllowFriendsToBook()
     * @uses PersonDetail::setCompanyRelation()
     * @uses PersonDetail::setExtendedAttributes()
     * @uses PersonDetail::setFriends()
     * @uses PersonDetail::setMemberCards()
     * @uses PersonDetail::setPerson()
     * @uses PersonDetail::setPersonActiveDates()
     * @uses PersonDetail::setPersonCommunication()
     * @uses PersonDetail::setPersonTypeAndStatus()
     * @uses PersonDetail::setSubscriptions()
     * @param bool $allowFriendsToBook
     * @param \Exerp\Person\StructType\PersonCompanyRelation $companyRelation
     * @param \Exerp\Person\StructType\ExtendedAttributes $extendedAttributes
     * @param \Exerp\Person\StructType\Friends $friends
     * @param \Exerp\Person\StructType\MemberCards $memberCards
     * @param \Exerp\Person\StructType\Person $person
     * @param \Exerp\Person\StructType\PersonActivationDates $personActiveDates
     * @param \Exerp\Person\StructType\PersonCommunication $personCommunication
     * @param \Exerp\Person\StructType\PersonTypeAndStatus $personTypeAndStatus
     * @param \Exerp\Person\StructType\Subscriptions $subscriptions
     */
    public function __construct(?bool $allowFriendsToBook = null, ?\Exerp\Person\StructType\PersonCompanyRelation $companyRelation = null, ?\Exerp\Person\StructType\ExtendedAttributes $extendedAttributes = null, ?\Exerp\Person\StructType\Friends $friends = null, ?\Exerp\Person\StructType\MemberCards $memberCards = null, ?\Exerp\Person\StructType\Person $person = null, ?\Exerp\Person\StructType\PersonActivationDates $personActiveDates = null, ?\Exerp\Person\StructType\PersonCommunication $personCommunication = null, ?\Exerp\Person\StructType\PersonTypeAndStatus $personTypeAndStatus = null, ?\Exerp\Person\StructType\Subscriptions $subscriptions = null)
    {
        $this
            ->setAllowFriendsToBook($allowFriendsToBook)
            ->setCompanyRelation($companyRelation)
            ->setExtendedAttributes($extendedAttributes)
            ->setFriends($friends)
            ->setMemberCards($memberCards)
            ->setPerson($person)
            ->setPersonActiveDates($personActiveDates)
            ->setPersonCommunication($personCommunication)
            ->setPersonTypeAndStatus($personTypeAndStatus)
            ->setSubscriptions($subscriptions);
    }
    /**
     * Get allowFriendsToBook value
     * @return bool|null
     */
    public function getAllowFriendsToBook(): ?bool
    {
        return $this->allowFriendsToBook;
    }
    /**
     * Set allowFriendsToBook value
     * @param bool $allowFriendsToBook
     * @return \Exerp\Person\StructType\PersonDetail
     */
    public function setAllowFriendsToBook(?bool $allowFriendsToBook = null): self
    {
        // validation for constraint: boolean
        if (!is_null($allowFriendsToBook) && !is_bool($allowFriendsToBook)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($allowFriendsToBook, true), gettype($allowFriendsToBook)), __LINE__);
        }
        $this->allowFriendsToBook = $allowFriendsToBook;
        
        return $this;
    }
    /**
     * Get companyRelation value
     * @return \Exerp\Person\StructType\PersonCompanyRelation|null
     */
    public function getCompanyRelation(): ?\Exerp\Person\StructType\PersonCompanyRelation
    {
        return $this->companyRelation;
    }
    /**
     * Set companyRelation value
     * @param \Exerp\Person\StructType\PersonCompanyRelation $companyRelation
     * @return \Exerp\Person\StructType\PersonDetail
     */
    public function setCompanyRelation(?\Exerp\Person\StructType\PersonCompanyRelation $companyRelation = null): self
    {
        $this->companyRelation = $companyRelation;
        
        return $this;
    }
    /**
     * Get extendedAttributes value
     * @return \Exerp\Person\StructType\ExtendedAttributes|null
     */
    public function getExtendedAttributes(): ?\Exerp\Person\StructType\ExtendedAttributes
    {
        return $this->extendedAttributes;
    }
    /**
     * Set extendedAttributes value
     * @param \Exerp\Person\StructType\ExtendedAttributes $extendedAttributes
     * @return \Exerp\Person\StructType\PersonDetail
     */
    public function setExtendedAttributes(?\Exerp\Person\StructType\ExtendedAttributes $extendedAttributes = null): self
    {
        $this->extendedAttributes = $extendedAttributes;
        
        return $this;
    }
    /**
     * Get friends value
     * @return \Exerp\Person\StructType\Friends|null
     */
    public function getFriends(): ?\Exerp\Person\StructType\Friends
    {
        return $this->friends;
    }
    /**
     * Set friends value
     * @param \Exerp\Person\StructType\Friends $friends
     * @return \Exerp\Person\StructType\PersonDetail
     */
    public function setFriends(?\Exerp\Person\StructType\Friends $friends = null): self
    {
        $this->friends = $friends;
        
        return $this;
    }
    /**
     * Get memberCards value
     * @return \Exerp\Person\StructType\MemberCards|null
     */
    public function getMemberCards(): ?\Exerp\Person\StructType\MemberCards
    {
        return $this->memberCards;
    }
    /**
     * Set memberCards value
     * @param \Exerp\Person\StructType\MemberCards $memberCards
     * @return \Exerp\Person\StructType\PersonDetail
     */
    public function setMemberCards(?\Exerp\Person\StructType\MemberCards $memberCards = null): self
    {
        $this->memberCards = $memberCards;
        
        return $this;
    }
    /**
     * Get person value
     * @return \Exerp\Person\StructType\Person|null
     */
    public function getPerson(): ?\Exerp\Person\StructType\Person
    {
        return $this->person;
    }
    /**
     * Set person value
     * @param \Exerp\Person\StructType\Person $person
     * @return \Exerp\Person\StructType\PersonDetail
     */
    public function setPerson(?\Exerp\Person\StructType\Person $person = null): self
    {
        $this->person = $person;
        
        return $this;
    }
    /**
     * Get personActiveDates value
     * @return \Exerp\Person\StructType\PersonActivationDates|null
     */
    public function getPersonActiveDates(): ?\Exerp\Person\StructType\PersonActivationDates
    {
        return $this->personActiveDates;
    }
    /**
     * Set personActiveDates value
     * @param \Exerp\Person\StructType\PersonActivationDates $personActiveDates
     * @return \Exerp\Person\StructType\PersonDetail
     */
    public function setPersonActiveDates(?\Exerp\Person\StructType\PersonActivationDates $personActiveDates = null): self
    {
        $this->personActiveDates = $personActiveDates;
        
        return $this;
    }
    /**
     * Get personCommunication value
     * @return \Exerp\Person\StructType\PersonCommunication|null
     */
    public function getPersonCommunication(): ?\Exerp\Person\StructType\PersonCommunication
    {
        return $this->personCommunication;
    }
    /**
     * Set personCommunication value
     * @param \Exerp\Person\StructType\PersonCommunication $personCommunication
     * @return \Exerp\Person\StructType\PersonDetail
     */
    public function setPersonCommunication(?\Exerp\Person\StructType\PersonCommunication $personCommunication = null): self
    {
        $this->personCommunication = $personCommunication;
        
        return $this;
    }
    /**
     * Get personTypeAndStatus value
     * @return \Exerp\Person\StructType\PersonTypeAndStatus|null
     */
    public function getPersonTypeAndStatus(): ?\Exerp\Person\StructType\PersonTypeAndStatus
    {
        return $this->personTypeAndStatus;
    }
    /**
     * Set personTypeAndStatus value
     * @param \Exerp\Person\StructType\PersonTypeAndStatus $personTypeAndStatus
     * @return \Exerp\Person\StructType\PersonDetail
     */
    public function setPersonTypeAndStatus(?\Exerp\Person\StructType\PersonTypeAndStatus $personTypeAndStatus = null): self
    {
        $this->personTypeAndStatus = $personTypeAndStatus;
        
        return $this;
    }
    /**
     * Get subscriptions value
     * @return \Exerp\Person\StructType\Subscriptions|null
     */
    public function getSubscriptions(): ?\Exerp\Person\StructType\Subscriptions
    {
        return $this->subscriptions;
    }
    /**
     * Set subscriptions value
     * @param \Exerp\Person\StructType\Subscriptions $subscriptions
     * @return \Exerp\Person\StructType\PersonDetail
     */
    public function setSubscriptions(?\Exerp\Person\StructType\Subscriptions $subscriptions = null): self
    {
        $this->subscriptions = $subscriptions;
        
        return $this;
    }
}
