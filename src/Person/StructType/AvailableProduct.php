<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for availableProduct StructType
 * @subpackage Structs
 */
class AvailableProduct extends AbstractStructBase
{
    /**
     * The globalId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $globalId = null;
    /**
     * The mainSubscription
     * @var bool|null
     */
    protected ?bool $mainSubscription = null;
    /**
     * The name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $name = null;
    /**
     * The price
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $price = null;
    /**
     * The productKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\CompositeKey|null
     */
    protected ?\Exerp\Person\StructType\CompositeKey $productKey = null;
    /**
     * Constructor method for availableProduct
     * @uses AvailableProduct::setGlobalId()
     * @uses AvailableProduct::setMainSubscription()
     * @uses AvailableProduct::setName()
     * @uses AvailableProduct::setPrice()
     * @uses AvailableProduct::setProductKey()
     * @param string $globalId
     * @param bool $mainSubscription
     * @param string $name
     * @param float $price
     * @param \Exerp\Person\StructType\CompositeKey $productKey
     */
    public function __construct(?string $globalId = null, ?bool $mainSubscription = null, ?string $name = null, ?float $price = null, ?\Exerp\Person\StructType\CompositeKey $productKey = null)
    {
        $this
            ->setGlobalId($globalId)
            ->setMainSubscription($mainSubscription)
            ->setName($name)
            ->setPrice($price)
            ->setProductKey($productKey);
    }
    /**
     * Get globalId value
     * @return string|null
     */
    public function getGlobalId(): ?string
    {
        return $this->globalId;
    }
    /**
     * Set globalId value
     * @param string $globalId
     * @return \Exerp\Person\StructType\AvailableProduct
     */
    public function setGlobalId(?string $globalId = null): self
    {
        // validation for constraint: string
        if (!is_null($globalId) && !is_string($globalId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($globalId, true), gettype($globalId)), __LINE__);
        }
        $this->globalId = $globalId;
        
        return $this;
    }
    /**
     * Get mainSubscription value
     * @return bool|null
     */
    public function getMainSubscription(): ?bool
    {
        return $this->mainSubscription;
    }
    /**
     * Set mainSubscription value
     * @param bool $mainSubscription
     * @return \Exerp\Person\StructType\AvailableProduct
     */
    public function setMainSubscription(?bool $mainSubscription = null): self
    {
        // validation for constraint: boolean
        if (!is_null($mainSubscription) && !is_bool($mainSubscription)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($mainSubscription, true), gettype($mainSubscription)), __LINE__);
        }
        $this->mainSubscription = $mainSubscription;
        
        return $this;
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \Exerp\Person\StructType\AvailableProduct
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        
        return $this;
    }
    /**
     * Get price value
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }
    /**
     * Set price value
     * @param float $price
     * @return \Exerp\Person\StructType\AvailableProduct
     */
    public function setPrice(?float $price = null): self
    {
        // validation for constraint: float
        if (!is_null($price) && !(is_float($price) || is_numeric($price))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($price, true), gettype($price)), __LINE__);
        }
        $this->price = $price;
        
        return $this;
    }
    /**
     * Get productKey value
     * @return \Exerp\Person\StructType\CompositeKey|null
     */
    public function getProductKey(): ?\Exerp\Person\StructType\CompositeKey
    {
        return $this->productKey;
    }
    /**
     * Set productKey value
     * @param \Exerp\Person\StructType\CompositeKey $productKey
     * @return \Exerp\Person\StructType\AvailableProduct
     */
    public function setProductKey(?\Exerp\Person\StructType\CompositeKey $productKey = null): self
    {
        $this->productKey = $productKey;
        
        return $this;
    }
}
