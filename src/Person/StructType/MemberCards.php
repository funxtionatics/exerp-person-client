<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for memberCards StructType
 * @subpackage Structs
 */
class MemberCards extends AbstractStructBase
{
    /**
     * The memberCard
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\MemberCard[]
     */
    protected ?array $memberCard = null;
    /**
     * Constructor method for memberCards
     * @uses MemberCards::setMemberCard()
     * @param \Exerp\Person\StructType\MemberCard[] $memberCard
     */
    public function __construct(?array $memberCard = null)
    {
        $this
            ->setMemberCard($memberCard);
    }
    /**
     * Get memberCard value
     * @return \Exerp\Person\StructType\MemberCard[]
     */
    public function getMemberCard(): ?array
    {
        return $this->memberCard;
    }
    /**
     * This method is responsible for validating the values passed to the setMemberCard method
     * This method is willingly generated in order to preserve the one-line inline validation within the setMemberCard method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateMemberCardForArrayConstraintsFromSetMemberCard(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $memberCardsMemberCardItem) {
            // validation for constraint: itemType
            if (!$memberCardsMemberCardItem instanceof \Exerp\Person\StructType\MemberCard) {
                $invalidValues[] = is_object($memberCardsMemberCardItem) ? get_class($memberCardsMemberCardItem) : sprintf('%s(%s)', gettype($memberCardsMemberCardItem), var_export($memberCardsMemberCardItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The memberCard property can only contain items of type \Exerp\Person\StructType\MemberCard, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set memberCard value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\MemberCard[] $memberCard
     * @return \Exerp\Person\StructType\MemberCards
     */
    public function setMemberCard(?array $memberCard = null): self
    {
        // validation for constraint: array
        if ('' !== ($memberCardArrayErrorMessage = self::validateMemberCardForArrayConstraintsFromSetMemberCard($memberCard))) {
            throw new InvalidArgumentException($memberCardArrayErrorMessage, __LINE__);
        }
        $this->memberCard = $memberCard;
        
        return $this;
    }
    /**
     * Add item to memberCard value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\MemberCard $item
     * @return \Exerp\Person\StructType\MemberCards
     */
    public function addToMemberCard(\Exerp\Person\StructType\MemberCard $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Exerp\Person\StructType\MemberCard) {
            throw new InvalidArgumentException(sprintf('The memberCard property can only contain items of type \Exerp\Person\StructType\MemberCard, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->memberCard[] = $item;
        
        return $this;
    }
}
