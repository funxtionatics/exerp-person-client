<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for findPerson StructType
 * @subpackage Structs
 */
class FindPerson extends AbstractStructBase
{
    /**
     * The attributes
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \Exerp\Person\StructType\ExtendedAttribute[]
     */
    protected ?array $attributes = null;
    /**
     * The birthday
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $birthday = null;
    /**
     * The center
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $center = null;
    /**
     * The country
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $country = null;
    /**
     * The name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $name = null;
    /**
     * The personType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $personType = null;
    /**
     * The phoneNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $phoneNumber = null;
    /**
     * The socialSecurityNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $socialSecurityNumber = null;
    /**
     * Constructor method for findPerson
     * @uses FindPerson::setAttributes()
     * @uses FindPerson::setBirthday()
     * @uses FindPerson::setCenter()
     * @uses FindPerson::setCountry()
     * @uses FindPerson::setName()
     * @uses FindPerson::setPersonType()
     * @uses FindPerson::setPhoneNumber()
     * @uses FindPerson::setSocialSecurityNumber()
     * @param \Exerp\Person\StructType\ExtendedAttribute[] $attributes
     * @param string $birthday
     * @param int $center
     * @param string $country
     * @param string $name
     * @param string $personType
     * @param string $phoneNumber
     * @param string $socialSecurityNumber
     */
    public function __construct(?array $attributes = null, ?string $birthday = null, ?int $center = null, ?string $country = null, ?string $name = null, ?string $personType = null, ?string $phoneNumber = null, ?string $socialSecurityNumber = null)
    {
        $this
            ->setAttributes($attributes)
            ->setBirthday($birthday)
            ->setCenter($center)
            ->setCountry($country)
            ->setName($name)
            ->setPersonType($personType)
            ->setPhoneNumber($phoneNumber)
            ->setSocialSecurityNumber($socialSecurityNumber);
    }
    /**
     * Get attributes value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Exerp\Person\StructType\ExtendedAttribute[]
     */
    public function getAttributes(): ?array
    {
        return isset($this->attributes) ? $this->attributes : null;
    }
    /**
     * This method is responsible for validating the values passed to the setAttributes method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAttributes method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAttributesForArrayConstraintsFromSetAttributes(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $findPersonAttributesItem) {
            // validation for constraint: itemType
            if (!$findPersonAttributesItem instanceof \Exerp\Person\StructType\ExtendedAttribute) {
                $invalidValues[] = is_object($findPersonAttributesItem) ? get_class($findPersonAttributesItem) : sprintf('%s(%s)', gettype($findPersonAttributesItem), var_export($findPersonAttributesItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The attributes property can only contain items of type \Exerp\Person\StructType\ExtendedAttribute, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set attributes value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\ExtendedAttribute[] $attributes
     * @return \Exerp\Person\StructType\FindPerson
     */
    public function setAttributes(?array $attributes = null): self
    {
        // validation for constraint: array
        if ('' !== ($attributesArrayErrorMessage = self::validateAttributesForArrayConstraintsFromSetAttributes($attributes))) {
            throw new InvalidArgumentException($attributesArrayErrorMessage, __LINE__);
        }
        if (is_null($attributes) || (is_array($attributes) && empty($attributes))) {
            unset($this->attributes);
        } else {
            $this->attributes = $attributes;
        }
        
        return $this;
    }
    /**
     * Add item to attributes value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\ExtendedAttribute $item
     * @return \Exerp\Person\StructType\FindPerson
     */
    public function addToAttributes(\Exerp\Person\StructType\ExtendedAttribute $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Exerp\Person\StructType\ExtendedAttribute) {
            throw new InvalidArgumentException(sprintf('The attributes property can only contain items of type \Exerp\Person\StructType\ExtendedAttribute, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->attributes[] = $item;
        
        return $this;
    }
    /**
     * Get birthday value
     * @return string|null
     */
    public function getBirthday(): ?string
    {
        return $this->birthday;
    }
    /**
     * Set birthday value
     * @param string $birthday
     * @return \Exerp\Person\StructType\FindPerson
     */
    public function setBirthday(?string $birthday = null): self
    {
        // validation for constraint: string
        if (!is_null($birthday) && !is_string($birthday)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($birthday, true), gettype($birthday)), __LINE__);
        }
        $this->birthday = $birthday;
        
        return $this;
    }
    /**
     * Get center value
     * @return int|null
     */
    public function getCenter(): ?int
    {
        return $this->center;
    }
    /**
     * Set center value
     * @param int $center
     * @return \Exerp\Person\StructType\FindPerson
     */
    public function setCenter(?int $center = null): self
    {
        // validation for constraint: int
        if (!is_null($center) && !(is_int($center) || ctype_digit($center))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($center, true), gettype($center)), __LINE__);
        }
        $this->center = $center;
        
        return $this;
    }
    /**
     * Get country value
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }
    /**
     * Set country value
     * @param string $country
     * @return \Exerp\Person\StructType\FindPerson
     */
    public function setCountry(?string $country = null): self
    {
        // validation for constraint: string
        if (!is_null($country) && !is_string($country)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($country, true), gettype($country)), __LINE__);
        }
        $this->country = $country;
        
        return $this;
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \Exerp\Person\StructType\FindPerson
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        
        return $this;
    }
    /**
     * Get personType value
     * @return string|null
     */
    public function getPersonType(): ?string
    {
        return $this->personType;
    }
    /**
     * Set personType value
     * @uses \Exerp\Person\EnumType\PersonType::valueIsValid()
     * @uses \Exerp\Person\EnumType\PersonType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $personType
     * @return \Exerp\Person\StructType\FindPerson
     */
    public function setPersonType(?string $personType = null): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Person\EnumType\PersonType::valueIsValid($personType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Person\EnumType\PersonType', is_array($personType) ? implode(', ', $personType) : var_export($personType, true), implode(', ', \Exerp\Person\EnumType\PersonType::getValidValues())), __LINE__);
        }
        $this->personType = $personType;
        
        return $this;
    }
    /**
     * Get phoneNumber value
     * @return string|null
     */
    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }
    /**
     * Set phoneNumber value
     * @param string $phoneNumber
     * @return \Exerp\Person\StructType\FindPerson
     */
    public function setPhoneNumber(?string $phoneNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($phoneNumber) && !is_string($phoneNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($phoneNumber, true), gettype($phoneNumber)), __LINE__);
        }
        $this->phoneNumber = $phoneNumber;
        
        return $this;
    }
    /**
     * Get socialSecurityNumber value
     * @return string|null
     */
    public function getSocialSecurityNumber(): ?string
    {
        return $this->socialSecurityNumber;
    }
    /**
     * Set socialSecurityNumber value
     * @param string $socialSecurityNumber
     * @return \Exerp\Person\StructType\FindPerson
     */
    public function setSocialSecurityNumber(?string $socialSecurityNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($socialSecurityNumber) && !is_string($socialSecurityNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($socialSecurityNumber, true), gettype($socialSecurityNumber)), __LINE__);
        }
        $this->socialSecurityNumber = $socialSecurityNumber;
        
        return $this;
    }
}
