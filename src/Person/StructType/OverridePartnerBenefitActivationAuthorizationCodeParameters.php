<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for
 * overridePartnerBenefitActivationAuthorizationCodeParameters StructType
 * @subpackage Structs
 */
class OverridePartnerBenefitActivationAuthorizationCodeParameters extends AbstractStructBase
{
    /**
     * The personKey
     * @var \Exerp\Person\StructType\ApiPersonKey|null
     */
    protected ?\Exerp\Person\StructType\ApiPersonKey $personKey = null;
    /**
     * The activationAuthorizationCode
     * @var string|null
     */
    protected ?string $activationAuthorizationCode = null;
    /**
     * Constructor method for
     * overridePartnerBenefitActivationAuthorizationCodeParameters
     * @uses OverridePartnerBenefitActivationAuthorizationCodeParameters::setPersonKey()
     * @uses OverridePartnerBenefitActivationAuthorizationCodeParameters::setActivationAuthorizationCode()
     * @param \Exerp\Person\StructType\ApiPersonKey $personKey
     * @param string $activationAuthorizationCode
     */
    public function __construct(?\Exerp\Person\StructType\ApiPersonKey $personKey = null, ?string $activationAuthorizationCode = null)
    {
        $this
            ->setPersonKey($personKey)
            ->setActivationAuthorizationCode($activationAuthorizationCode);
    }
    /**
     * Get personKey value
     * @return \Exerp\Person\StructType\ApiPersonKey|null
     */
    public function getPersonKey(): ?\Exerp\Person\StructType\ApiPersonKey
    {
        return $this->personKey;
    }
    /**
     * Set personKey value
     * @param \Exerp\Person\StructType\ApiPersonKey $personKey
     * @return \Exerp\Person\StructType\OverridePartnerBenefitActivationAuthorizationCodeParameters
     */
    public function setPersonKey(?\Exerp\Person\StructType\ApiPersonKey $personKey = null): self
    {
        $this->personKey = $personKey;
        
        return $this;
    }
    /**
     * Get activationAuthorizationCode value
     * @return string|null
     */
    public function getActivationAuthorizationCode(): ?string
    {
        return $this->activationAuthorizationCode;
    }
    /**
     * Set activationAuthorizationCode value
     * @param string $activationAuthorizationCode
     * @return \Exerp\Person\StructType\OverridePartnerBenefitActivationAuthorizationCodeParameters
     */
    public function setActivationAuthorizationCode(?string $activationAuthorizationCode = null): self
    {
        // validation for constraint: string
        if (!is_null($activationAuthorizationCode) && !is_string($activationAuthorizationCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($activationAuthorizationCode, true), gettype($activationAuthorizationCode)), __LINE__);
        }
        $this->activationAuthorizationCode = $activationAuthorizationCode;
        
        return $this;
    }
}
