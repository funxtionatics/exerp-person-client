<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for requiredSignatures StructType
 * @subpackage Structs
 */
class RequiredSignatures extends AbstractStructBase
{
    /**
     * The requiredSignature
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\JournalNoteDocumentSignature[]
     */
    protected ?array $requiredSignature = null;
    /**
     * Constructor method for requiredSignatures
     * @uses RequiredSignatures::setRequiredSignature()
     * @param \Exerp\Person\StructType\JournalNoteDocumentSignature[] $requiredSignature
     */
    public function __construct(?array $requiredSignature = null)
    {
        $this
            ->setRequiredSignature($requiredSignature);
    }
    /**
     * Get requiredSignature value
     * @return \Exerp\Person\StructType\JournalNoteDocumentSignature[]
     */
    public function getRequiredSignature(): ?array
    {
        return $this->requiredSignature;
    }
    /**
     * This method is responsible for validating the values passed to the setRequiredSignature method
     * This method is willingly generated in order to preserve the one-line inline validation within the setRequiredSignature method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateRequiredSignatureForArrayConstraintsFromSetRequiredSignature(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $requiredSignaturesRequiredSignatureItem) {
            // validation for constraint: itemType
            if (!$requiredSignaturesRequiredSignatureItem instanceof \Exerp\Person\StructType\JournalNoteDocumentSignature) {
                $invalidValues[] = is_object($requiredSignaturesRequiredSignatureItem) ? get_class($requiredSignaturesRequiredSignatureItem) : sprintf('%s(%s)', gettype($requiredSignaturesRequiredSignatureItem), var_export($requiredSignaturesRequiredSignatureItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The requiredSignature property can only contain items of type \Exerp\Person\StructType\JournalNoteDocumentSignature, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set requiredSignature value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\JournalNoteDocumentSignature[] $requiredSignature
     * @return \Exerp\Person\StructType\RequiredSignatures
     */
    public function setRequiredSignature(?array $requiredSignature = null): self
    {
        // validation for constraint: array
        if ('' !== ($requiredSignatureArrayErrorMessage = self::validateRequiredSignatureForArrayConstraintsFromSetRequiredSignature($requiredSignature))) {
            throw new InvalidArgumentException($requiredSignatureArrayErrorMessage, __LINE__);
        }
        $this->requiredSignature = $requiredSignature;
        
        return $this;
    }
    /**
     * Add item to requiredSignature value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\JournalNoteDocumentSignature $item
     * @return \Exerp\Person\StructType\RequiredSignatures
     */
    public function addToRequiredSignature(\Exerp\Person\StructType\JournalNoteDocumentSignature $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Exerp\Person\StructType\JournalNoteDocumentSignature) {
            throw new InvalidArgumentException(sprintf('The requiredSignature property can only contain items of type \Exerp\Person\StructType\JournalNoteDocumentSignature, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->requiredSignature[] = $item;
        
        return $this;
    }
}
