<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for extendedAttributeMIMEs StructType
 * @subpackage Structs
 */
class ExtendedAttributeMIMEs extends AbstractStructBase
{
    /**
     * The extendedAttributeMIME
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\ExtendedAttributeMIME[]
     */
    protected ?array $extendedAttributeMIME = null;
    /**
     * Constructor method for extendedAttributeMIMEs
     * @uses ExtendedAttributeMIMEs::setExtendedAttributeMIME()
     * @param \Exerp\Person\StructType\ExtendedAttributeMIME[] $extendedAttributeMIME
     */
    public function __construct(?array $extendedAttributeMIME = null)
    {
        $this
            ->setExtendedAttributeMIME($extendedAttributeMIME);
    }
    /**
     * Get extendedAttributeMIME value
     * @return \Exerp\Person\StructType\ExtendedAttributeMIME[]
     */
    public function getExtendedAttributeMIME(): ?array
    {
        return $this->extendedAttributeMIME;
    }
    /**
     * This method is responsible for validating the values passed to the setExtendedAttributeMIME method
     * This method is willingly generated in order to preserve the one-line inline validation within the setExtendedAttributeMIME method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateExtendedAttributeMIMEForArrayConstraintsFromSetExtendedAttributeMIME(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $extendedAttributeMIMEsExtendedAttributeMIMEItem) {
            // validation for constraint: itemType
            if (!$extendedAttributeMIMEsExtendedAttributeMIMEItem instanceof \Exerp\Person\StructType\ExtendedAttributeMIME) {
                $invalidValues[] = is_object($extendedAttributeMIMEsExtendedAttributeMIMEItem) ? get_class($extendedAttributeMIMEsExtendedAttributeMIMEItem) : sprintf('%s(%s)', gettype($extendedAttributeMIMEsExtendedAttributeMIMEItem), var_export($extendedAttributeMIMEsExtendedAttributeMIMEItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The extendedAttributeMIME property can only contain items of type \Exerp\Person\StructType\ExtendedAttributeMIME, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set extendedAttributeMIME value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\ExtendedAttributeMIME[] $extendedAttributeMIME
     * @return \Exerp\Person\StructType\ExtendedAttributeMIMEs
     */
    public function setExtendedAttributeMIME(?array $extendedAttributeMIME = null): self
    {
        // validation for constraint: array
        if ('' !== ($extendedAttributeMIMEArrayErrorMessage = self::validateExtendedAttributeMIMEForArrayConstraintsFromSetExtendedAttributeMIME($extendedAttributeMIME))) {
            throw new InvalidArgumentException($extendedAttributeMIMEArrayErrorMessage, __LINE__);
        }
        $this->extendedAttributeMIME = $extendedAttributeMIME;
        
        return $this;
    }
    /**
     * Add item to extendedAttributeMIME value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\ExtendedAttributeMIME $item
     * @return \Exerp\Person\StructType\ExtendedAttributeMIMEs
     */
    public function addToExtendedAttributeMIME(\Exerp\Person\StructType\ExtendedAttributeMIME $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Exerp\Person\StructType\ExtendedAttributeMIME) {
            throw new InvalidArgumentException(sprintf('The extendedAttributeMIME property can only contain items of type \Exerp\Person\StructType\ExtendedAttributeMIME, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->extendedAttributeMIME[] = $item;
        
        return $this;
    }
}
