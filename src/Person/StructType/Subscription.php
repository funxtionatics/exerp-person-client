<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for subscription StructType
 * @subpackage Structs
 */
class Subscription extends AbstractStructBase
{
    /**
     * The assignedAddOns
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \Exerp\Person\StructType\AssignedAddOn[]
     */
    protected ?array $assignedAddOns = null;
    /**
     * The billedUntilDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $billedUntilDate = null;
    /**
     * The bindingEndDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $bindingEndDate = null;
    /**
     * The changeRestrictionEnabled
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $changeRestrictionEnabled = null;
    /**
     * The endDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $endDate = null;
    /**
     * The freeDays
     * @var int|null
     */
    protected ?int $freeDays = null;
    /**
     * The paymentAgreementKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\CompositeSubKey|null
     */
    protected ?\Exerp\Person\StructType\CompositeSubKey $paymentAgreementKey = null;
    /**
     * The personId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\ApiPersonKey|null
     */
    protected ?\Exerp\Person\StructType\ApiPersonKey $personId = null;
    /**
     * The price
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $price = null;
    /**
     * The product
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\SubscriptionProduct|null
     */
    protected ?\Exerp\Person\StructType\SubscriptionProduct $product = null;
    /**
     * The startDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $startDate = null;
    /**
     * The state
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $state = null;
    /**
     * The subState
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $subState = null;
    /**
     * The subscriptionId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\CompositeKey|null
     */
    protected ?\Exerp\Person\StructType\CompositeKey $subscriptionId = null;
    /**
     * The windowOfOpportunity
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $windowOfOpportunity = null;
    /**
     * Constructor method for subscription
     * @uses Subscription::setAssignedAddOns()
     * @uses Subscription::setBilledUntilDate()
     * @uses Subscription::setBindingEndDate()
     * @uses Subscription::setChangeRestrictionEnabled()
     * @uses Subscription::setEndDate()
     * @uses Subscription::setFreeDays()
     * @uses Subscription::setPaymentAgreementKey()
     * @uses Subscription::setPersonId()
     * @uses Subscription::setPrice()
     * @uses Subscription::setProduct()
     * @uses Subscription::setStartDate()
     * @uses Subscription::setState()
     * @uses Subscription::setSubState()
     * @uses Subscription::setSubscriptionId()
     * @uses Subscription::setWindowOfOpportunity()
     * @param \Exerp\Person\StructType\AssignedAddOn[] $assignedAddOns
     * @param string $billedUntilDate
     * @param string $bindingEndDate
     * @param bool $changeRestrictionEnabled
     * @param string $endDate
     * @param int $freeDays
     * @param \Exerp\Person\StructType\CompositeSubKey $paymentAgreementKey
     * @param \Exerp\Person\StructType\ApiPersonKey $personId
     * @param string $price
     * @param \Exerp\Person\StructType\SubscriptionProduct $product
     * @param string $startDate
     * @param string $state
     * @param string $subState
     * @param \Exerp\Person\StructType\CompositeKey $subscriptionId
     * @param int $windowOfOpportunity
     */
    public function __construct(?array $assignedAddOns = null, ?string $billedUntilDate = null, ?string $bindingEndDate = null, ?bool $changeRestrictionEnabled = null, ?string $endDate = null, ?int $freeDays = null, ?\Exerp\Person\StructType\CompositeSubKey $paymentAgreementKey = null, ?\Exerp\Person\StructType\ApiPersonKey $personId = null, ?string $price = null, ?\Exerp\Person\StructType\SubscriptionProduct $product = null, ?string $startDate = null, ?string $state = null, ?string $subState = null, ?\Exerp\Person\StructType\CompositeKey $subscriptionId = null, ?int $windowOfOpportunity = null)
    {
        $this
            ->setAssignedAddOns($assignedAddOns)
            ->setBilledUntilDate($billedUntilDate)
            ->setBindingEndDate($bindingEndDate)
            ->setChangeRestrictionEnabled($changeRestrictionEnabled)
            ->setEndDate($endDate)
            ->setFreeDays($freeDays)
            ->setPaymentAgreementKey($paymentAgreementKey)
            ->setPersonId($personId)
            ->setPrice($price)
            ->setProduct($product)
            ->setStartDate($startDate)
            ->setState($state)
            ->setSubState($subState)
            ->setSubscriptionId($subscriptionId)
            ->setWindowOfOpportunity($windowOfOpportunity);
    }
    /**
     * Get assignedAddOns value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Exerp\Person\StructType\AssignedAddOn[]
     */
    public function getAssignedAddOns(): ?array
    {
        return isset($this->assignedAddOns) ? $this->assignedAddOns : null;
    }
    /**
     * This method is responsible for validating the values passed to the setAssignedAddOns method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAssignedAddOns method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAssignedAddOnsForArrayConstraintsFromSetAssignedAddOns(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $subscriptionAssignedAddOnsItem) {
            // validation for constraint: itemType
            if (!$subscriptionAssignedAddOnsItem instanceof \Exerp\Person\StructType\AssignedAddOn) {
                $invalidValues[] = is_object($subscriptionAssignedAddOnsItem) ? get_class($subscriptionAssignedAddOnsItem) : sprintf('%s(%s)', gettype($subscriptionAssignedAddOnsItem), var_export($subscriptionAssignedAddOnsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The assignedAddOns property can only contain items of type \Exerp\Person\StructType\AssignedAddOn, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set assignedAddOns value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\AssignedAddOn[] $assignedAddOns
     * @return \Exerp\Person\StructType\Subscription
     */
    public function setAssignedAddOns(?array $assignedAddOns = null): self
    {
        // validation for constraint: array
        if ('' !== ($assignedAddOnsArrayErrorMessage = self::validateAssignedAddOnsForArrayConstraintsFromSetAssignedAddOns($assignedAddOns))) {
            throw new InvalidArgumentException($assignedAddOnsArrayErrorMessage, __LINE__);
        }
        if (is_null($assignedAddOns) || (is_array($assignedAddOns) && empty($assignedAddOns))) {
            unset($this->assignedAddOns);
        } else {
            $this->assignedAddOns = $assignedAddOns;
        }
        
        return $this;
    }
    /**
     * Add item to assignedAddOns value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\AssignedAddOn $item
     * @return \Exerp\Person\StructType\Subscription
     */
    public function addToAssignedAddOns(\Exerp\Person\StructType\AssignedAddOn $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Exerp\Person\StructType\AssignedAddOn) {
            throw new InvalidArgumentException(sprintf('The assignedAddOns property can only contain items of type \Exerp\Person\StructType\AssignedAddOn, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->assignedAddOns[] = $item;
        
        return $this;
    }
    /**
     * Get billedUntilDate value
     * @return string|null
     */
    public function getBilledUntilDate(): ?string
    {
        return $this->billedUntilDate;
    }
    /**
     * Set billedUntilDate value
     * @param string $billedUntilDate
     * @return \Exerp\Person\StructType\Subscription
     */
    public function setBilledUntilDate(?string $billedUntilDate = null): self
    {
        // validation for constraint: string
        if (!is_null($billedUntilDate) && !is_string($billedUntilDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($billedUntilDate, true), gettype($billedUntilDate)), __LINE__);
        }
        $this->billedUntilDate = $billedUntilDate;
        
        return $this;
    }
    /**
     * Get bindingEndDate value
     * @return string|null
     */
    public function getBindingEndDate(): ?string
    {
        return $this->bindingEndDate;
    }
    /**
     * Set bindingEndDate value
     * @param string $bindingEndDate
     * @return \Exerp\Person\StructType\Subscription
     */
    public function setBindingEndDate(?string $bindingEndDate = null): self
    {
        // validation for constraint: string
        if (!is_null($bindingEndDate) && !is_string($bindingEndDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($bindingEndDate, true), gettype($bindingEndDate)), __LINE__);
        }
        $this->bindingEndDate = $bindingEndDate;
        
        return $this;
    }
    /**
     * Get changeRestrictionEnabled value
     * @return bool|null
     */
    public function getChangeRestrictionEnabled(): ?bool
    {
        return $this->changeRestrictionEnabled;
    }
    /**
     * Set changeRestrictionEnabled value
     * @param bool $changeRestrictionEnabled
     * @return \Exerp\Person\StructType\Subscription
     */
    public function setChangeRestrictionEnabled(?bool $changeRestrictionEnabled = null): self
    {
        // validation for constraint: boolean
        if (!is_null($changeRestrictionEnabled) && !is_bool($changeRestrictionEnabled)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($changeRestrictionEnabled, true), gettype($changeRestrictionEnabled)), __LINE__);
        }
        $this->changeRestrictionEnabled = $changeRestrictionEnabled;
        
        return $this;
    }
    /**
     * Get endDate value
     * @return string|null
     */
    public function getEndDate(): ?string
    {
        return $this->endDate;
    }
    /**
     * Set endDate value
     * @param string $endDate
     * @return \Exerp\Person\StructType\Subscription
     */
    public function setEndDate(?string $endDate = null): self
    {
        // validation for constraint: string
        if (!is_null($endDate) && !is_string($endDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($endDate, true), gettype($endDate)), __LINE__);
        }
        $this->endDate = $endDate;
        
        return $this;
    }
    /**
     * Get freeDays value
     * @return int|null
     */
    public function getFreeDays(): ?int
    {
        return $this->freeDays;
    }
    /**
     * Set freeDays value
     * @param int $freeDays
     * @return \Exerp\Person\StructType\Subscription
     */
    public function setFreeDays(?int $freeDays = null): self
    {
        // validation for constraint: int
        if (!is_null($freeDays) && !(is_int($freeDays) || ctype_digit($freeDays))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($freeDays, true), gettype($freeDays)), __LINE__);
        }
        $this->freeDays = $freeDays;
        
        return $this;
    }
    /**
     * Get paymentAgreementKey value
     * @return \Exerp\Person\StructType\CompositeSubKey|null
     */
    public function getPaymentAgreementKey(): ?\Exerp\Person\StructType\CompositeSubKey
    {
        return $this->paymentAgreementKey;
    }
    /**
     * Set paymentAgreementKey value
     * @param \Exerp\Person\StructType\CompositeSubKey $paymentAgreementKey
     * @return \Exerp\Person\StructType\Subscription
     */
    public function setPaymentAgreementKey(?\Exerp\Person\StructType\CompositeSubKey $paymentAgreementKey = null): self
    {
        $this->paymentAgreementKey = $paymentAgreementKey;
        
        return $this;
    }
    /**
     * Get personId value
     * @return \Exerp\Person\StructType\ApiPersonKey|null
     */
    public function getPersonId(): ?\Exerp\Person\StructType\ApiPersonKey
    {
        return $this->personId;
    }
    /**
     * Set personId value
     * @param \Exerp\Person\StructType\ApiPersonKey $personId
     * @return \Exerp\Person\StructType\Subscription
     */
    public function setPersonId(?\Exerp\Person\StructType\ApiPersonKey $personId = null): self
    {
        $this->personId = $personId;
        
        return $this;
    }
    /**
     * Get price value
     * @return string|null
     */
    public function getPrice(): ?string
    {
        return $this->price;
    }
    /**
     * Set price value
     * @param string $price
     * @return \Exerp\Person\StructType\Subscription
     */
    public function setPrice(?string $price = null): self
    {
        // validation for constraint: string
        if (!is_null($price) && !is_string($price)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($price, true), gettype($price)), __LINE__);
        }
        $this->price = $price;
        
        return $this;
    }
    /**
     * Get product value
     * @return \Exerp\Person\StructType\SubscriptionProduct|null
     */
    public function getProduct(): ?\Exerp\Person\StructType\SubscriptionProduct
    {
        return $this->product;
    }
    /**
     * Set product value
     * @param \Exerp\Person\StructType\SubscriptionProduct $product
     * @return \Exerp\Person\StructType\Subscription
     */
    public function setProduct(?\Exerp\Person\StructType\SubscriptionProduct $product = null): self
    {
        $this->product = $product;
        
        return $this;
    }
    /**
     * Get startDate value
     * @return string|null
     */
    public function getStartDate(): ?string
    {
        return $this->startDate;
    }
    /**
     * Set startDate value
     * @param string $startDate
     * @return \Exerp\Person\StructType\Subscription
     */
    public function setStartDate(?string $startDate = null): self
    {
        // validation for constraint: string
        if (!is_null($startDate) && !is_string($startDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($startDate, true), gettype($startDate)), __LINE__);
        }
        $this->startDate = $startDate;
        
        return $this;
    }
    /**
     * Get state value
     * @return string|null
     */
    public function getState(): ?string
    {
        return $this->state;
    }
    /**
     * Set state value
     * @uses \Exerp\Person\EnumType\SubscriptionState::valueIsValid()
     * @uses \Exerp\Person\EnumType\SubscriptionState::getValidValues()
     * @throws InvalidArgumentException
     * @param string $state
     * @return \Exerp\Person\StructType\Subscription
     */
    public function setState(?string $state = null): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Person\EnumType\SubscriptionState::valueIsValid($state)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Person\EnumType\SubscriptionState', is_array($state) ? implode(', ', $state) : var_export($state, true), implode(', ', \Exerp\Person\EnumType\SubscriptionState::getValidValues())), __LINE__);
        }
        $this->state = $state;
        
        return $this;
    }
    /**
     * Get subState value
     * @return string|null
     */
    public function getSubState(): ?string
    {
        return $this->subState;
    }
    /**
     * Set subState value
     * @uses \Exerp\Person\EnumType\SubscriptionSubState::valueIsValid()
     * @uses \Exerp\Person\EnumType\SubscriptionSubState::getValidValues()
     * @throws InvalidArgumentException
     * @param string $subState
     * @return \Exerp\Person\StructType\Subscription
     */
    public function setSubState(?string $subState = null): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Person\EnumType\SubscriptionSubState::valueIsValid($subState)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Person\EnumType\SubscriptionSubState', is_array($subState) ? implode(', ', $subState) : var_export($subState, true), implode(', ', \Exerp\Person\EnumType\SubscriptionSubState::getValidValues())), __LINE__);
        }
        $this->subState = $subState;
        
        return $this;
    }
    /**
     * Get subscriptionId value
     * @return \Exerp\Person\StructType\CompositeKey|null
     */
    public function getSubscriptionId(): ?\Exerp\Person\StructType\CompositeKey
    {
        return $this->subscriptionId;
    }
    /**
     * Set subscriptionId value
     * @param \Exerp\Person\StructType\CompositeKey $subscriptionId
     * @return \Exerp\Person\StructType\Subscription
     */
    public function setSubscriptionId(?\Exerp\Person\StructType\CompositeKey $subscriptionId = null): self
    {
        $this->subscriptionId = $subscriptionId;
        
        return $this;
    }
    /**
     * Get windowOfOpportunity value
     * @return int|null
     */
    public function getWindowOfOpportunity(): ?int
    {
        return $this->windowOfOpportunity;
    }
    /**
     * Set windowOfOpportunity value
     * @param int $windowOfOpportunity
     * @return \Exerp\Person\StructType\Subscription
     */
    public function setWindowOfOpportunity(?int $windowOfOpportunity = null): self
    {
        // validation for constraint: int
        if (!is_null($windowOfOpportunity) && !(is_int($windowOfOpportunity) || ctype_digit($windowOfOpportunity))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($windowOfOpportunity, true), gettype($windowOfOpportunity)), __LINE__);
        }
        $this->windowOfOpportunity = $windowOfOpportunity;
        
        return $this;
    }
}
