<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for availableSalutationsResponse StructType
 * @subpackage Structs
 */
class AvailableSalutationsResponse extends AbstractStructBase
{
    /**
     * The centers
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\Centers|null
     */
    protected ?\Exerp\Person\StructType\Centers $centers = null;
    /**
     * Constructor method for availableSalutationsResponse
     * @uses AvailableSalutationsResponse::setCenters()
     * @param \Exerp\Person\StructType\Centers $centers
     */
    public function __construct(?\Exerp\Person\StructType\Centers $centers = null)
    {
        $this
            ->setCenters($centers);
    }
    /**
     * Get centers value
     * @return \Exerp\Person\StructType\Centers|null
     */
    public function getCenters(): ?\Exerp\Person\StructType\Centers
    {
        return $this->centers;
    }
    /**
     * Set centers value
     * @param \Exerp\Person\StructType\Centers $centers
     * @return \Exerp\Person\StructType\AvailableSalutationsResponse
     */
    public function setCenters(?\Exerp\Person\StructType\Centers $centers = null): self
    {
        $this->centers = $centers;
        
        return $this;
    }
}
