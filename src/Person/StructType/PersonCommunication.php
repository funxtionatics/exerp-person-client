<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for personCommunication StructType
 * @subpackage Structs
 */
class PersonCommunication extends AbstractStructBase
{
    /**
     * The allowChargedSMS
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $allowChargedSMS = null;
    /**
     * The allowEmail
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $allowEmail = null;
    /**
     * The allowLetter
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $allowLetter = null;
    /**
     * The allowPhone
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $allowPhone = null;
    /**
     * The allowSMS
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $allowSMS = null;
    /**
     * The email
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $email = null;
    /**
     * The homePhoneNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $homePhoneNumber = null;
    /**
     * The mobilePhoneNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $mobilePhoneNumber = null;
    /**
     * The personId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\ApiPersonKey|null
     */
    protected ?\Exerp\Person\StructType\ApiPersonKey $personId = null;
    /**
     * The preferredCommunicationLanguage
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $preferredCommunicationLanguage = null;
    /**
     * The quickChannel
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $quickChannel = null;
    /**
     * The wishToReceiveEmailNewsLetters
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $wishToReceiveEmailNewsLetters = null;
    /**
     * The wishToReceiveThirdPartyOffers
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $wishToReceiveThirdPartyOffers = null;
    /**
     * The workPhoneNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $workPhoneNumber = null;
    /**
     * Constructor method for personCommunication
     * @uses PersonCommunication::setAllowChargedSMS()
     * @uses PersonCommunication::setAllowEmail()
     * @uses PersonCommunication::setAllowLetter()
     * @uses PersonCommunication::setAllowPhone()
     * @uses PersonCommunication::setAllowSMS()
     * @uses PersonCommunication::setEmail()
     * @uses PersonCommunication::setHomePhoneNumber()
     * @uses PersonCommunication::setMobilePhoneNumber()
     * @uses PersonCommunication::setPersonId()
     * @uses PersonCommunication::setPreferredCommunicationLanguage()
     * @uses PersonCommunication::setQuickChannel()
     * @uses PersonCommunication::setWishToReceiveEmailNewsLetters()
     * @uses PersonCommunication::setWishToReceiveThirdPartyOffers()
     * @uses PersonCommunication::setWorkPhoneNumber()
     * @param bool $allowChargedSMS
     * @param bool $allowEmail
     * @param bool $allowLetter
     * @param bool $allowPhone
     * @param bool $allowSMS
     * @param string $email
     * @param string $homePhoneNumber
     * @param string $mobilePhoneNumber
     * @param \Exerp\Person\StructType\ApiPersonKey $personId
     * @param string $preferredCommunicationLanguage
     * @param string $quickChannel
     * @param bool $wishToReceiveEmailNewsLetters
     * @param bool $wishToReceiveThirdPartyOffers
     * @param string $workPhoneNumber
     */
    public function __construct(?bool $allowChargedSMS = null, ?bool $allowEmail = null, ?bool $allowLetter = null, ?bool $allowPhone = null, ?bool $allowSMS = null, ?string $email = null, ?string $homePhoneNumber = null, ?string $mobilePhoneNumber = null, ?\Exerp\Person\StructType\ApiPersonKey $personId = null, ?string $preferredCommunicationLanguage = null, ?string $quickChannel = null, ?bool $wishToReceiveEmailNewsLetters = null, ?bool $wishToReceiveThirdPartyOffers = null, ?string $workPhoneNumber = null)
    {
        $this
            ->setAllowChargedSMS($allowChargedSMS)
            ->setAllowEmail($allowEmail)
            ->setAllowLetter($allowLetter)
            ->setAllowPhone($allowPhone)
            ->setAllowSMS($allowSMS)
            ->setEmail($email)
            ->setHomePhoneNumber($homePhoneNumber)
            ->setMobilePhoneNumber($mobilePhoneNumber)
            ->setPersonId($personId)
            ->setPreferredCommunicationLanguage($preferredCommunicationLanguage)
            ->setQuickChannel($quickChannel)
            ->setWishToReceiveEmailNewsLetters($wishToReceiveEmailNewsLetters)
            ->setWishToReceiveThirdPartyOffers($wishToReceiveThirdPartyOffers)
            ->setWorkPhoneNumber($workPhoneNumber);
    }
    /**
     * Get allowChargedSMS value
     * @return bool|null
     */
    public function getAllowChargedSMS(): ?bool
    {
        return $this->allowChargedSMS;
    }
    /**
     * Set allowChargedSMS value
     * @param bool $allowChargedSMS
     * @return \Exerp\Person\StructType\PersonCommunication
     */
    public function setAllowChargedSMS(?bool $allowChargedSMS = null): self
    {
        // validation for constraint: boolean
        if (!is_null($allowChargedSMS) && !is_bool($allowChargedSMS)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($allowChargedSMS, true), gettype($allowChargedSMS)), __LINE__);
        }
        $this->allowChargedSMS = $allowChargedSMS;
        
        return $this;
    }
    /**
     * Get allowEmail value
     * @return bool|null
     */
    public function getAllowEmail(): ?bool
    {
        return $this->allowEmail;
    }
    /**
     * Set allowEmail value
     * @param bool $allowEmail
     * @return \Exerp\Person\StructType\PersonCommunication
     */
    public function setAllowEmail(?bool $allowEmail = null): self
    {
        // validation for constraint: boolean
        if (!is_null($allowEmail) && !is_bool($allowEmail)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($allowEmail, true), gettype($allowEmail)), __LINE__);
        }
        $this->allowEmail = $allowEmail;
        
        return $this;
    }
    /**
     * Get allowLetter value
     * @return bool|null
     */
    public function getAllowLetter(): ?bool
    {
        return $this->allowLetter;
    }
    /**
     * Set allowLetter value
     * @param bool $allowLetter
     * @return \Exerp\Person\StructType\PersonCommunication
     */
    public function setAllowLetter(?bool $allowLetter = null): self
    {
        // validation for constraint: boolean
        if (!is_null($allowLetter) && !is_bool($allowLetter)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($allowLetter, true), gettype($allowLetter)), __LINE__);
        }
        $this->allowLetter = $allowLetter;
        
        return $this;
    }
    /**
     * Get allowPhone value
     * @return bool|null
     */
    public function getAllowPhone(): ?bool
    {
        return $this->allowPhone;
    }
    /**
     * Set allowPhone value
     * @param bool $allowPhone
     * @return \Exerp\Person\StructType\PersonCommunication
     */
    public function setAllowPhone(?bool $allowPhone = null): self
    {
        // validation for constraint: boolean
        if (!is_null($allowPhone) && !is_bool($allowPhone)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($allowPhone, true), gettype($allowPhone)), __LINE__);
        }
        $this->allowPhone = $allowPhone;
        
        return $this;
    }
    /**
     * Get allowSMS value
     * @return bool|null
     */
    public function getAllowSMS(): ?bool
    {
        return $this->allowSMS;
    }
    /**
     * Set allowSMS value
     * @param bool $allowSMS
     * @return \Exerp\Person\StructType\PersonCommunication
     */
    public function setAllowSMS(?bool $allowSMS = null): self
    {
        // validation for constraint: boolean
        if (!is_null($allowSMS) && !is_bool($allowSMS)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($allowSMS, true), gettype($allowSMS)), __LINE__);
        }
        $this->allowSMS = $allowSMS;
        
        return $this;
    }
    /**
     * Get email value
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }
    /**
     * Set email value
     * @param string $email
     * @return \Exerp\Person\StructType\PersonCommunication
     */
    public function setEmail(?string $email = null): self
    {
        // validation for constraint: string
        if (!is_null($email) && !is_string($email)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($email, true), gettype($email)), __LINE__);
        }
        $this->email = $email;
        
        return $this;
    }
    /**
     * Get homePhoneNumber value
     * @return string|null
     */
    public function getHomePhoneNumber(): ?string
    {
        return $this->homePhoneNumber;
    }
    /**
     * Set homePhoneNumber value
     * @param string $homePhoneNumber
     * @return \Exerp\Person\StructType\PersonCommunication
     */
    public function setHomePhoneNumber(?string $homePhoneNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($homePhoneNumber) && !is_string($homePhoneNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($homePhoneNumber, true), gettype($homePhoneNumber)), __LINE__);
        }
        $this->homePhoneNumber = $homePhoneNumber;
        
        return $this;
    }
    /**
     * Get mobilePhoneNumber value
     * @return string|null
     */
    public function getMobilePhoneNumber(): ?string
    {
        return $this->mobilePhoneNumber;
    }
    /**
     * Set mobilePhoneNumber value
     * @param string $mobilePhoneNumber
     * @return \Exerp\Person\StructType\PersonCommunication
     */
    public function setMobilePhoneNumber(?string $mobilePhoneNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($mobilePhoneNumber) && !is_string($mobilePhoneNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($mobilePhoneNumber, true), gettype($mobilePhoneNumber)), __LINE__);
        }
        $this->mobilePhoneNumber = $mobilePhoneNumber;
        
        return $this;
    }
    /**
     * Get personId value
     * @return \Exerp\Person\StructType\ApiPersonKey|null
     */
    public function getPersonId(): ?\Exerp\Person\StructType\ApiPersonKey
    {
        return $this->personId;
    }
    /**
     * Set personId value
     * @param \Exerp\Person\StructType\ApiPersonKey $personId
     * @return \Exerp\Person\StructType\PersonCommunication
     */
    public function setPersonId(?\Exerp\Person\StructType\ApiPersonKey $personId = null): self
    {
        $this->personId = $personId;
        
        return $this;
    }
    /**
     * Get preferredCommunicationLanguage value
     * @return string|null
     */
    public function getPreferredCommunicationLanguage(): ?string
    {
        return $this->preferredCommunicationLanguage;
    }
    /**
     * Set preferredCommunicationLanguage value
     * @param string $preferredCommunicationLanguage
     * @return \Exerp\Person\StructType\PersonCommunication
     */
    public function setPreferredCommunicationLanguage(?string $preferredCommunicationLanguage = null): self
    {
        // validation for constraint: string
        if (!is_null($preferredCommunicationLanguage) && !is_string($preferredCommunicationLanguage)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($preferredCommunicationLanguage, true), gettype($preferredCommunicationLanguage)), __LINE__);
        }
        $this->preferredCommunicationLanguage = $preferredCommunicationLanguage;
        
        return $this;
    }
    /**
     * Get quickChannel value
     * @return string|null
     */
    public function getQuickChannel(): ?string
    {
        return $this->quickChannel;
    }
    /**
     * Set quickChannel value
     * @uses \Exerp\Person\EnumType\QuickChannel::valueIsValid()
     * @uses \Exerp\Person\EnumType\QuickChannel::getValidValues()
     * @throws InvalidArgumentException
     * @param string $quickChannel
     * @return \Exerp\Person\StructType\PersonCommunication
     */
    public function setQuickChannel(?string $quickChannel = null): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Person\EnumType\QuickChannel::valueIsValid($quickChannel)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Person\EnumType\QuickChannel', is_array($quickChannel) ? implode(', ', $quickChannel) : var_export($quickChannel, true), implode(', ', \Exerp\Person\EnumType\QuickChannel::getValidValues())), __LINE__);
        }
        $this->quickChannel = $quickChannel;
        
        return $this;
    }
    /**
     * Get wishToReceiveEmailNewsLetters value
     * @return bool|null
     */
    public function getWishToReceiveEmailNewsLetters(): ?bool
    {
        return $this->wishToReceiveEmailNewsLetters;
    }
    /**
     * Set wishToReceiveEmailNewsLetters value
     * @param bool $wishToReceiveEmailNewsLetters
     * @return \Exerp\Person\StructType\PersonCommunication
     */
    public function setWishToReceiveEmailNewsLetters(?bool $wishToReceiveEmailNewsLetters = null): self
    {
        // validation for constraint: boolean
        if (!is_null($wishToReceiveEmailNewsLetters) && !is_bool($wishToReceiveEmailNewsLetters)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($wishToReceiveEmailNewsLetters, true), gettype($wishToReceiveEmailNewsLetters)), __LINE__);
        }
        $this->wishToReceiveEmailNewsLetters = $wishToReceiveEmailNewsLetters;
        
        return $this;
    }
    /**
     * Get wishToReceiveThirdPartyOffers value
     * @return bool|null
     */
    public function getWishToReceiveThirdPartyOffers(): ?bool
    {
        return $this->wishToReceiveThirdPartyOffers;
    }
    /**
     * Set wishToReceiveThirdPartyOffers value
     * @param bool $wishToReceiveThirdPartyOffers
     * @return \Exerp\Person\StructType\PersonCommunication
     */
    public function setWishToReceiveThirdPartyOffers(?bool $wishToReceiveThirdPartyOffers = null): self
    {
        // validation for constraint: boolean
        if (!is_null($wishToReceiveThirdPartyOffers) && !is_bool($wishToReceiveThirdPartyOffers)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($wishToReceiveThirdPartyOffers, true), gettype($wishToReceiveThirdPartyOffers)), __LINE__);
        }
        $this->wishToReceiveThirdPartyOffers = $wishToReceiveThirdPartyOffers;
        
        return $this;
    }
    /**
     * Get workPhoneNumber value
     * @return string|null
     */
    public function getWorkPhoneNumber(): ?string
    {
        return $this->workPhoneNumber;
    }
    /**
     * Set workPhoneNumber value
     * @param string $workPhoneNumber
     * @return \Exerp\Person\StructType\PersonCommunication
     */
    public function setWorkPhoneNumber(?string $workPhoneNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($workPhoneNumber) && !is_string($workPhoneNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($workPhoneNumber, true), gettype($workPhoneNumber)), __LINE__);
        }
        $this->workPhoneNumber = $workPhoneNumber;
        
        return $this;
    }
}
