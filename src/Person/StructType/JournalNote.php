<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for journalNote StructType
 * @subpackage Structs
 */
class JournalNote extends AbstractStructBase
{
    /**
     * The key
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $key = null;
    /**
     * The type
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * The personKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\ApiPersonKey|null
     */
    protected ?\Exerp\Person\StructType\ApiPersonKey $personKey = null;
    /**
     * The text
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $text = null;
    /**
     * The name
     * @var string|null
     */
    protected ?string $name = null;
    /**
     * The filename
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $filename = null;
    /**
     * The creationTime
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $creationTime = null;
    /**
     * The creationDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $creationDate = null;
    /**
     * The requiredSignatures
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\RequiredSignatures|null
     */
    protected ?\Exerp\Person\StructType\RequiredSignatures $requiredSignatures = null;
    /**
     * The mimeDocument
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\MimeDocument|null
     */
    protected ?\Exerp\Person\StructType\MimeDocument $mimeDocument = null;
    /**
     * The issueDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $issueDate = null;
    /**
     * The expirationDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $expirationDate = null;
    /**
     * The customJournalDocumentTypeId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $customJournalDocumentTypeId = null;
    /**
     * Constructor method for journalNote
     * @uses JournalNote::setKey()
     * @uses JournalNote::setType()
     * @uses JournalNote::setPersonKey()
     * @uses JournalNote::setText()
     * @uses JournalNote::setName()
     * @uses JournalNote::setFilename()
     * @uses JournalNote::setCreationTime()
     * @uses JournalNote::setCreationDate()
     * @uses JournalNote::setRequiredSignatures()
     * @uses JournalNote::setMimeDocument()
     * @uses JournalNote::setIssueDate()
     * @uses JournalNote::setExpirationDate()
     * @uses JournalNote::setCustomJournalDocumentTypeId()
     * @param int $key
     * @param string $type
     * @param \Exerp\Person\StructType\ApiPersonKey $personKey
     * @param string $text
     * @param string $name
     * @param string $filename
     * @param string $creationTime
     * @param string $creationDate
     * @param \Exerp\Person\StructType\RequiredSignatures $requiredSignatures
     * @param \Exerp\Person\StructType\MimeDocument $mimeDocument
     * @param string $issueDate
     * @param string $expirationDate
     * @param int $customJournalDocumentTypeId
     */
    public function __construct(?int $key = null, ?string $type = null, ?\Exerp\Person\StructType\ApiPersonKey $personKey = null, ?string $text = null, ?string $name = null, ?string $filename = null, ?string $creationTime = null, ?string $creationDate = null, ?\Exerp\Person\StructType\RequiredSignatures $requiredSignatures = null, ?\Exerp\Person\StructType\MimeDocument $mimeDocument = null, ?string $issueDate = null, ?string $expirationDate = null, ?int $customJournalDocumentTypeId = null)
    {
        $this
            ->setKey($key)
            ->setType($type)
            ->setPersonKey($personKey)
            ->setText($text)
            ->setName($name)
            ->setFilename($filename)
            ->setCreationTime($creationTime)
            ->setCreationDate($creationDate)
            ->setRequiredSignatures($requiredSignatures)
            ->setMimeDocument($mimeDocument)
            ->setIssueDate($issueDate)
            ->setExpirationDate($expirationDate)
            ->setCustomJournalDocumentTypeId($customJournalDocumentTypeId);
    }
    /**
     * Get key value
     * @return int|null
     */
    public function getKey(): ?int
    {
        return $this->key;
    }
    /**
     * Set key value
     * @param int $key
     * @return \Exerp\Person\StructType\JournalNote
     */
    public function setKey(?int $key = null): self
    {
        // validation for constraint: int
        if (!is_null($key) && !(is_int($key) || ctype_digit($key))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($key, true), gettype($key)), __LINE__);
        }
        $this->key = $key;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @uses \Exerp\Person\EnumType\JournalNoteType::valueIsValid()
     * @uses \Exerp\Person\EnumType\JournalNoteType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $type
     * @return \Exerp\Person\StructType\JournalNote
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Person\EnumType\JournalNoteType::valueIsValid($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Person\EnumType\JournalNoteType', is_array($type) ? implode(', ', $type) : var_export($type, true), implode(', ', \Exerp\Person\EnumType\JournalNoteType::getValidValues())), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
    /**
     * Get personKey value
     * @return \Exerp\Person\StructType\ApiPersonKey|null
     */
    public function getPersonKey(): ?\Exerp\Person\StructType\ApiPersonKey
    {
        return $this->personKey;
    }
    /**
     * Set personKey value
     * @param \Exerp\Person\StructType\ApiPersonKey $personKey
     * @return \Exerp\Person\StructType\JournalNote
     */
    public function setPersonKey(?\Exerp\Person\StructType\ApiPersonKey $personKey = null): self
    {
        $this->personKey = $personKey;
        
        return $this;
    }
    /**
     * Get text value
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }
    /**
     * Set text value
     * @param string $text
     * @return \Exerp\Person\StructType\JournalNote
     */
    public function setText(?string $text = null): self
    {
        // validation for constraint: string
        if (!is_null($text) && !is_string($text)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($text, true), gettype($text)), __LINE__);
        }
        $this->text = $text;
        
        return $this;
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \Exerp\Person\StructType\JournalNote
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        
        return $this;
    }
    /**
     * Get filename value
     * @return string|null
     */
    public function getFilename(): ?string
    {
        return $this->filename;
    }
    /**
     * Set filename value
     * @param string $filename
     * @return \Exerp\Person\StructType\JournalNote
     */
    public function setFilename(?string $filename = null): self
    {
        // validation for constraint: string
        if (!is_null($filename) && !is_string($filename)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($filename, true), gettype($filename)), __LINE__);
        }
        $this->filename = $filename;
        
        return $this;
    }
    /**
     * Get creationTime value
     * @return string|null
     */
    public function getCreationTime(): ?string
    {
        return $this->creationTime;
    }
    /**
     * Set creationTime value
     * @param string $creationTime
     * @return \Exerp\Person\StructType\JournalNote
     */
    public function setCreationTime(?string $creationTime = null): self
    {
        // validation for constraint: string
        if (!is_null($creationTime) && !is_string($creationTime)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($creationTime, true), gettype($creationTime)), __LINE__);
        }
        $this->creationTime = $creationTime;
        
        return $this;
    }
    /**
     * Get creationDate value
     * @return string|null
     */
    public function getCreationDate(): ?string
    {
        return $this->creationDate;
    }
    /**
     * Set creationDate value
     * @param string $creationDate
     * @return \Exerp\Person\StructType\JournalNote
     */
    public function setCreationDate(?string $creationDate = null): self
    {
        // validation for constraint: string
        if (!is_null($creationDate) && !is_string($creationDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($creationDate, true), gettype($creationDate)), __LINE__);
        }
        $this->creationDate = $creationDate;
        
        return $this;
    }
    /**
     * Get requiredSignatures value
     * @return \Exerp\Person\StructType\RequiredSignatures|null
     */
    public function getRequiredSignatures(): ?\Exerp\Person\StructType\RequiredSignatures
    {
        return $this->requiredSignatures;
    }
    /**
     * Set requiredSignatures value
     * @param \Exerp\Person\StructType\RequiredSignatures $requiredSignatures
     * @return \Exerp\Person\StructType\JournalNote
     */
    public function setRequiredSignatures(?\Exerp\Person\StructType\RequiredSignatures $requiredSignatures = null): self
    {
        $this->requiredSignatures = $requiredSignatures;
        
        return $this;
    }
    /**
     * Get mimeDocument value
     * @return \Exerp\Person\StructType\MimeDocument|null
     */
    public function getMimeDocument(): ?\Exerp\Person\StructType\MimeDocument
    {
        return $this->mimeDocument;
    }
    /**
     * Set mimeDocument value
     * @param \Exerp\Person\StructType\MimeDocument $mimeDocument
     * @return \Exerp\Person\StructType\JournalNote
     */
    public function setMimeDocument(?\Exerp\Person\StructType\MimeDocument $mimeDocument = null): self
    {
        $this->mimeDocument = $mimeDocument;
        
        return $this;
    }
    /**
     * Get issueDate value
     * @return string|null
     */
    public function getIssueDate(): ?string
    {
        return $this->issueDate;
    }
    /**
     * Set issueDate value
     * @param string $issueDate
     * @return \Exerp\Person\StructType\JournalNote
     */
    public function setIssueDate(?string $issueDate = null): self
    {
        // validation for constraint: string
        if (!is_null($issueDate) && !is_string($issueDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($issueDate, true), gettype($issueDate)), __LINE__);
        }
        $this->issueDate = $issueDate;
        
        return $this;
    }
    /**
     * Get expirationDate value
     * @return string|null
     */
    public function getExpirationDate(): ?string
    {
        return $this->expirationDate;
    }
    /**
     * Set expirationDate value
     * @param string $expirationDate
     * @return \Exerp\Person\StructType\JournalNote
     */
    public function setExpirationDate(?string $expirationDate = null): self
    {
        // validation for constraint: string
        if (!is_null($expirationDate) && !is_string($expirationDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($expirationDate, true), gettype($expirationDate)), __LINE__);
        }
        $this->expirationDate = $expirationDate;
        
        return $this;
    }
    /**
     * Get customJournalDocumentTypeId value
     * @return int|null
     */
    public function getCustomJournalDocumentTypeId(): ?int
    {
        return $this->customJournalDocumentTypeId;
    }
    /**
     * Set customJournalDocumentTypeId value
     * @param int $customJournalDocumentTypeId
     * @return \Exerp\Person\StructType\JournalNote
     */
    public function setCustomJournalDocumentTypeId(?int $customJournalDocumentTypeId = null): self
    {
        // validation for constraint: int
        if (!is_null($customJournalDocumentTypeId) && !(is_int($customJournalDocumentTypeId) || ctype_digit($customJournalDocumentTypeId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($customJournalDocumentTypeId, true), gettype($customJournalDocumentTypeId)), __LINE__);
        }
        $this->customJournalDocumentTypeId = $customJournalDocumentTypeId;
        
        return $this;
    }
}
