<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for personRelationParameters StructType
 * @subpackage Structs
 */
class PersonRelationParameters extends AbstractStructBase
{
    /**
     * The personKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\ApiPersonKey|null
     */
    protected ?\Exerp\Person\StructType\ApiPersonKey $personKey = null;
    /**
     * Constructor method for personRelationParameters
     * @uses PersonRelationParameters::setPersonKey()
     * @param \Exerp\Person\StructType\ApiPersonKey $personKey
     */
    public function __construct(?\Exerp\Person\StructType\ApiPersonKey $personKey = null)
    {
        $this
            ->setPersonKey($personKey);
    }
    /**
     * Get personKey value
     * @return \Exerp\Person\StructType\ApiPersonKey|null
     */
    public function getPersonKey(): ?\Exerp\Person\StructType\ApiPersonKey
    {
        return $this->personKey;
    }
    /**
     * Set personKey value
     * @param \Exerp\Person\StructType\ApiPersonKey $personKey
     * @return \Exerp\Person\StructType\PersonRelationParameters
     */
    public function setPersonKey(?\Exerp\Person\StructType\ApiPersonKey $personKey = null): self
    {
        $this->personKey = $personKey;
        
        return $this;
    }
}
