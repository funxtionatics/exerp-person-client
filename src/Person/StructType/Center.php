<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for center StructType
 * @subpackage Structs
 */
class Center extends AbstractStructBase
{
    /**
     * The address
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\Address|null
     */
    protected ?\Exerp\Person\StructType\Address $address = null;
    /**
     * The centerId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $centerId = null;
    /**
     * The name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $name = null;
    /**
     * The shortName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $shortName = null;
    /**
     * The webName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $webName = null;
    /**
     * Constructor method for center
     * @uses Center::setAddress()
     * @uses Center::setCenterId()
     * @uses Center::setName()
     * @uses Center::setShortName()
     * @uses Center::setWebName()
     * @param \Exerp\Person\StructType\Address $address
     * @param int $centerId
     * @param string $name
     * @param string $shortName
     * @param string $webName
     */
    public function __construct(?\Exerp\Person\StructType\Address $address = null, ?int $centerId = null, ?string $name = null, ?string $shortName = null, ?string $webName = null)
    {
        $this
            ->setAddress($address)
            ->setCenterId($centerId)
            ->setName($name)
            ->setShortName($shortName)
            ->setWebName($webName);
    }
    /**
     * Get address value
     * @return \Exerp\Person\StructType\Address|null
     */
    public function getAddress(): ?\Exerp\Person\StructType\Address
    {
        return $this->address;
    }
    /**
     * Set address value
     * @param \Exerp\Person\StructType\Address $address
     * @return \Exerp\Person\StructType\Center
     */
    public function setAddress(?\Exerp\Person\StructType\Address $address = null): self
    {
        $this->address = $address;
        
        return $this;
    }
    /**
     * Get centerId value
     * @return int|null
     */
    public function getCenterId(): ?int
    {
        return $this->centerId;
    }
    /**
     * Set centerId value
     * @param int $centerId
     * @return \Exerp\Person\StructType\Center
     */
    public function setCenterId(?int $centerId = null): self
    {
        // validation for constraint: int
        if (!is_null($centerId) && !(is_int($centerId) || ctype_digit($centerId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($centerId, true), gettype($centerId)), __LINE__);
        }
        $this->centerId = $centerId;
        
        return $this;
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \Exerp\Person\StructType\Center
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        
        return $this;
    }
    /**
     * Get shortName value
     * @return string|null
     */
    public function getShortName(): ?string
    {
        return $this->shortName;
    }
    /**
     * Set shortName value
     * @param string $shortName
     * @return \Exerp\Person\StructType\Center
     */
    public function setShortName(?string $shortName = null): self
    {
        // validation for constraint: string
        if (!is_null($shortName) && !is_string($shortName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($shortName, true), gettype($shortName)), __LINE__);
        }
        $this->shortName = $shortName;
        
        return $this;
    }
    /**
     * Get webName value
     * @return string|null
     */
    public function getWebName(): ?string
    {
        return $this->webName;
    }
    /**
     * Set webName value
     * @param string $webName
     * @return \Exerp\Person\StructType\Center
     */
    public function setWebName(?string $webName = null): self
    {
        // validation for constraint: string
        if (!is_null($webName) && !is_string($webName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($webName, true), gettype($webName)), __LINE__);
        }
        $this->webName = $webName;
        
        return $this;
    }
}
