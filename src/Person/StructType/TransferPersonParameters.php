<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for transferPersonParameters StructType
 * @subpackage Structs
 */
class TransferPersonParameters extends AbstractStructBase
{
    /**
     * The changeEndDateForCashSubscriptions
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $changeEndDateForCashSubscriptions = null;
    /**
     * The changePriceForEFTSubscriptions
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $changePriceForEFTSubscriptions = null;
    /**
     * The personKey
     * @var \Exerp\Person\StructType\ApiPersonKey|null
     */
    protected ?\Exerp\Person\StructType\ApiPersonKey $personKey = null;
    /**
     * The toCenterId
     * @var int|null
     */
    protected ?int $toCenterId = null;
    /**
     * The transferSubscriptionsInfo
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\TransferSubscriptionInfo[]
     */
    protected ?array $transferSubscriptionsInfo = null;
    /**
     * Constructor method for transferPersonParameters
     * @uses TransferPersonParameters::setChangeEndDateForCashSubscriptions()
     * @uses TransferPersonParameters::setChangePriceForEFTSubscriptions()
     * @uses TransferPersonParameters::setPersonKey()
     * @uses TransferPersonParameters::setToCenterId()
     * @uses TransferPersonParameters::setTransferSubscriptionsInfo()
     * @param bool $changeEndDateForCashSubscriptions
     * @param bool $changePriceForEFTSubscriptions
     * @param \Exerp\Person\StructType\ApiPersonKey $personKey
     * @param int $toCenterId
     * @param \Exerp\Person\StructType\TransferSubscriptionInfo[] $transferSubscriptionsInfo
     */
    public function __construct(?bool $changeEndDateForCashSubscriptions = null, ?bool $changePriceForEFTSubscriptions = null, ?\Exerp\Person\StructType\ApiPersonKey $personKey = null, ?int $toCenterId = null, ?array $transferSubscriptionsInfo = null)
    {
        $this
            ->setChangeEndDateForCashSubscriptions($changeEndDateForCashSubscriptions)
            ->setChangePriceForEFTSubscriptions($changePriceForEFTSubscriptions)
            ->setPersonKey($personKey)
            ->setToCenterId($toCenterId)
            ->setTransferSubscriptionsInfo($transferSubscriptionsInfo);
    }
    /**
     * Get changeEndDateForCashSubscriptions value
     * @return bool|null
     */
    public function getChangeEndDateForCashSubscriptions(): ?bool
    {
        return $this->changeEndDateForCashSubscriptions;
    }
    /**
     * Set changeEndDateForCashSubscriptions value
     * @param bool $changeEndDateForCashSubscriptions
     * @return \Exerp\Person\StructType\TransferPersonParameters
     */
    public function setChangeEndDateForCashSubscriptions(?bool $changeEndDateForCashSubscriptions = null): self
    {
        // validation for constraint: boolean
        if (!is_null($changeEndDateForCashSubscriptions) && !is_bool($changeEndDateForCashSubscriptions)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($changeEndDateForCashSubscriptions, true), gettype($changeEndDateForCashSubscriptions)), __LINE__);
        }
        $this->changeEndDateForCashSubscriptions = $changeEndDateForCashSubscriptions;
        
        return $this;
    }
    /**
     * Get changePriceForEFTSubscriptions value
     * @return bool|null
     */
    public function getChangePriceForEFTSubscriptions(): ?bool
    {
        return $this->changePriceForEFTSubscriptions;
    }
    /**
     * Set changePriceForEFTSubscriptions value
     * @param bool $changePriceForEFTSubscriptions
     * @return \Exerp\Person\StructType\TransferPersonParameters
     */
    public function setChangePriceForEFTSubscriptions(?bool $changePriceForEFTSubscriptions = null): self
    {
        // validation for constraint: boolean
        if (!is_null($changePriceForEFTSubscriptions) && !is_bool($changePriceForEFTSubscriptions)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($changePriceForEFTSubscriptions, true), gettype($changePriceForEFTSubscriptions)), __LINE__);
        }
        $this->changePriceForEFTSubscriptions = $changePriceForEFTSubscriptions;
        
        return $this;
    }
    /**
     * Get personKey value
     * @return \Exerp\Person\StructType\ApiPersonKey|null
     */
    public function getPersonKey(): ?\Exerp\Person\StructType\ApiPersonKey
    {
        return $this->personKey;
    }
    /**
     * Set personKey value
     * @param \Exerp\Person\StructType\ApiPersonKey $personKey
     * @return \Exerp\Person\StructType\TransferPersonParameters
     */
    public function setPersonKey(?\Exerp\Person\StructType\ApiPersonKey $personKey = null): self
    {
        $this->personKey = $personKey;
        
        return $this;
    }
    /**
     * Get toCenterId value
     * @return int|null
     */
    public function getToCenterId(): ?int
    {
        return $this->toCenterId;
    }
    /**
     * Set toCenterId value
     * @param int $toCenterId
     * @return \Exerp\Person\StructType\TransferPersonParameters
     */
    public function setToCenterId(?int $toCenterId = null): self
    {
        // validation for constraint: int
        if (!is_null($toCenterId) && !(is_int($toCenterId) || ctype_digit($toCenterId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($toCenterId, true), gettype($toCenterId)), __LINE__);
        }
        $this->toCenterId = $toCenterId;
        
        return $this;
    }
    /**
     * Get transferSubscriptionsInfo value
     * @return \Exerp\Person\StructType\TransferSubscriptionInfo[]
     */
    public function getTransferSubscriptionsInfo(): ?array
    {
        return $this->transferSubscriptionsInfo;
    }
    /**
     * This method is responsible for validating the values passed to the setTransferSubscriptionsInfo method
     * This method is willingly generated in order to preserve the one-line inline validation within the setTransferSubscriptionsInfo method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateTransferSubscriptionsInfoForArrayConstraintsFromSetTransferSubscriptionsInfo(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $transferPersonParametersTransferSubscriptionsInfoItem) {
            // validation for constraint: itemType
            if (!$transferPersonParametersTransferSubscriptionsInfoItem instanceof \Exerp\Person\StructType\TransferSubscriptionInfo) {
                $invalidValues[] = is_object($transferPersonParametersTransferSubscriptionsInfoItem) ? get_class($transferPersonParametersTransferSubscriptionsInfoItem) : sprintf('%s(%s)', gettype($transferPersonParametersTransferSubscriptionsInfoItem), var_export($transferPersonParametersTransferSubscriptionsInfoItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The transferSubscriptionsInfo property can only contain items of type \Exerp\Person\StructType\TransferSubscriptionInfo, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set transferSubscriptionsInfo value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\TransferSubscriptionInfo[] $transferSubscriptionsInfo
     * @return \Exerp\Person\StructType\TransferPersonParameters
     */
    public function setTransferSubscriptionsInfo(?array $transferSubscriptionsInfo = null): self
    {
        // validation for constraint: array
        if ('' !== ($transferSubscriptionsInfoArrayErrorMessage = self::validateTransferSubscriptionsInfoForArrayConstraintsFromSetTransferSubscriptionsInfo($transferSubscriptionsInfo))) {
            throw new InvalidArgumentException($transferSubscriptionsInfoArrayErrorMessage, __LINE__);
        }
        $this->transferSubscriptionsInfo = $transferSubscriptionsInfo;
        
        return $this;
    }
    /**
     * Add item to transferSubscriptionsInfo value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\TransferSubscriptionInfo $item
     * @return \Exerp\Person\StructType\TransferPersonParameters
     */
    public function addToTransferSubscriptionsInfo(\Exerp\Person\StructType\TransferSubscriptionInfo $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Exerp\Person\StructType\TransferSubscriptionInfo) {
            throw new InvalidArgumentException(sprintf('The transferSubscriptionsInfo property can only contain items of type \Exerp\Person\StructType\TransferSubscriptionInfo, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->transferSubscriptionsInfo[] = $item;
        
        return $this;
    }
}
