<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for signatureData StructType
 * Meta information extracted from the WSDL
 * - final: extension restriction
 * @subpackage Structs
 */
class SignatureData extends AbstractStructBase
{
    /**
     * The signatureRank
     * @var int|null
     */
    protected ?int $signatureRank = null;
    /**
     * The signatureImage
     * @var \Exerp\Person\StructType\MimeDocument|null
     */
    protected ?\Exerp\Person\StructType\MimeDocument $signatureImage = null;
    /**
     * Constructor method for signatureData
     * @uses SignatureData::setSignatureRank()
     * @uses SignatureData::setSignatureImage()
     * @param int $signatureRank
     * @param \Exerp\Person\StructType\MimeDocument $signatureImage
     */
    public function __construct(?int $signatureRank = null, ?\Exerp\Person\StructType\MimeDocument $signatureImage = null)
    {
        $this
            ->setSignatureRank($signatureRank)
            ->setSignatureImage($signatureImage);
    }
    /**
     * Get signatureRank value
     * @return int|null
     */
    public function getSignatureRank(): ?int
    {
        return $this->signatureRank;
    }
    /**
     * Set signatureRank value
     * @param int $signatureRank
     * @return \Exerp\Person\StructType\SignatureData
     */
    public function setSignatureRank(?int $signatureRank = null): self
    {
        // validation for constraint: int
        if (!is_null($signatureRank) && !(is_int($signatureRank) || ctype_digit($signatureRank))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($signatureRank, true), gettype($signatureRank)), __LINE__);
        }
        $this->signatureRank = $signatureRank;
        
        return $this;
    }
    /**
     * Get signatureImage value
     * @return \Exerp\Person\StructType\MimeDocument|null
     */
    public function getSignatureImage(): ?\Exerp\Person\StructType\MimeDocument
    {
        return $this->signatureImage;
    }
    /**
     * Set signatureImage value
     * @param \Exerp\Person\StructType\MimeDocument $signatureImage
     * @return \Exerp\Person\StructType\SignatureData
     */
    public function setSignatureImage(?\Exerp\Person\StructType\MimeDocument $signatureImage = null): self
    {
        $this->signatureImage = $signatureImage;
        
        return $this;
    }
}
