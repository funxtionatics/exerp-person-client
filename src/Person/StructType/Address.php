<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for address StructType
 * @subpackage Structs
 */
class Address extends AbstractStructBase
{
    /**
     * The address1
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $address1 = null;
    /**
     * The address2
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $address2 = null;
    /**
     * The address3
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $address3 = null;
    /**
     * The coName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $coName = null;
    /**
     * The country
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $country = null;
    /**
     * The county
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $county = null;
    /**
     * The province
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $province = null;
    /**
     * The zip
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $zip = null;
    /**
     * The zipName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $zipName = null;
    /**
     * Constructor method for address
     * @uses Address::setAddress1()
     * @uses Address::setAddress2()
     * @uses Address::setAddress3()
     * @uses Address::setCoName()
     * @uses Address::setCountry()
     * @uses Address::setCounty()
     * @uses Address::setProvince()
     * @uses Address::setZip()
     * @uses Address::setZipName()
     * @param string $address1
     * @param string $address2
     * @param string $address3
     * @param string $coName
     * @param string $country
     * @param string $county
     * @param string $province
     * @param string $zip
     * @param string $zipName
     */
    public function __construct(?string $address1 = null, ?string $address2 = null, ?string $address3 = null, ?string $coName = null, ?string $country = null, ?string $county = null, ?string $province = null, ?string $zip = null, ?string $zipName = null)
    {
        $this
            ->setAddress1($address1)
            ->setAddress2($address2)
            ->setAddress3($address3)
            ->setCoName($coName)
            ->setCountry($country)
            ->setCounty($county)
            ->setProvince($province)
            ->setZip($zip)
            ->setZipName($zipName);
    }
    /**
     * Get address1 value
     * @return string|null
     */
    public function getAddress1(): ?string
    {
        return $this->address1;
    }
    /**
     * Set address1 value
     * @param string $address1
     * @return \Exerp\Person\StructType\Address
     */
    public function setAddress1(?string $address1 = null): self
    {
        // validation for constraint: string
        if (!is_null($address1) && !is_string($address1)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($address1, true), gettype($address1)), __LINE__);
        }
        $this->address1 = $address1;
        
        return $this;
    }
    /**
     * Get address2 value
     * @return string|null
     */
    public function getAddress2(): ?string
    {
        return $this->address2;
    }
    /**
     * Set address2 value
     * @param string $address2
     * @return \Exerp\Person\StructType\Address
     */
    public function setAddress2(?string $address2 = null): self
    {
        // validation for constraint: string
        if (!is_null($address2) && !is_string($address2)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($address2, true), gettype($address2)), __LINE__);
        }
        $this->address2 = $address2;
        
        return $this;
    }
    /**
     * Get address3 value
     * @return string|null
     */
    public function getAddress3(): ?string
    {
        return $this->address3;
    }
    /**
     * Set address3 value
     * @param string $address3
     * @return \Exerp\Person\StructType\Address
     */
    public function setAddress3(?string $address3 = null): self
    {
        // validation for constraint: string
        if (!is_null($address3) && !is_string($address3)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($address3, true), gettype($address3)), __LINE__);
        }
        $this->address3 = $address3;
        
        return $this;
    }
    /**
     * Get coName value
     * @return string|null
     */
    public function getCoName(): ?string
    {
        return $this->coName;
    }
    /**
     * Set coName value
     * @param string $coName
     * @return \Exerp\Person\StructType\Address
     */
    public function setCoName(?string $coName = null): self
    {
        // validation for constraint: string
        if (!is_null($coName) && !is_string($coName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($coName, true), gettype($coName)), __LINE__);
        }
        $this->coName = $coName;
        
        return $this;
    }
    /**
     * Get country value
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }
    /**
     * Set country value
     * @param string $country
     * @return \Exerp\Person\StructType\Address
     */
    public function setCountry(?string $country = null): self
    {
        // validation for constraint: string
        if (!is_null($country) && !is_string($country)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($country, true), gettype($country)), __LINE__);
        }
        $this->country = $country;
        
        return $this;
    }
    /**
     * Get county value
     * @return string|null
     */
    public function getCounty(): ?string
    {
        return $this->county;
    }
    /**
     * Set county value
     * @param string $county
     * @return \Exerp\Person\StructType\Address
     */
    public function setCounty(?string $county = null): self
    {
        // validation for constraint: string
        if (!is_null($county) && !is_string($county)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($county, true), gettype($county)), __LINE__);
        }
        $this->county = $county;
        
        return $this;
    }
    /**
     * Get province value
     * @return string|null
     */
    public function getProvince(): ?string
    {
        return $this->province;
    }
    /**
     * Set province value
     * @param string $province
     * @return \Exerp\Person\StructType\Address
     */
    public function setProvince(?string $province = null): self
    {
        // validation for constraint: string
        if (!is_null($province) && !is_string($province)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($province, true), gettype($province)), __LINE__);
        }
        $this->province = $province;
        
        return $this;
    }
    /**
     * Get zip value
     * @return string|null
     */
    public function getZip(): ?string
    {
        return $this->zip;
    }
    /**
     * Set zip value
     * @param string $zip
     * @return \Exerp\Person\StructType\Address
     */
    public function setZip(?string $zip = null): self
    {
        // validation for constraint: string
        if (!is_null($zip) && !is_string($zip)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($zip, true), gettype($zip)), __LINE__);
        }
        $this->zip = $zip;
        
        return $this;
    }
    /**
     * Get zipName value
     * @return string|null
     */
    public function getZipName(): ?string
    {
        return $this->zipName;
    }
    /**
     * Set zipName value
     * @param string $zipName
     * @return \Exerp\Person\StructType\Address
     */
    public function setZipName(?string $zipName = null): self
    {
        // validation for constraint: string
        if (!is_null($zipName) && !is_string($zipName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($zipName, true), gettype($zipName)), __LINE__);
        }
        $this->zipName = $zipName;
        
        return $this;
    }
}
