<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for journalNoteParameters StructType
 * @subpackage Structs
 */
class JournalNoteParameters extends AbstractStructBase
{
    /**
     * The customTypeId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $customTypeId = null;
    /**
     * The fromDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $fromDate = null;
    /**
     * The maxEntries
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int|null
     */
    protected ?int $maxEntries = null;
    /**
     * The personId
     * @var \Exerp\Person\StructType\ApiPersonKey|null
     */
    protected ?\Exerp\Person\StructType\ApiPersonKey $personId = null;
    /**
     * The toDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $toDate = null;
    /**
     * The type
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for journalNoteParameters
     * @uses JournalNoteParameters::setCustomTypeId()
     * @uses JournalNoteParameters::setFromDate()
     * @uses JournalNoteParameters::setMaxEntries()
     * @uses JournalNoteParameters::setPersonId()
     * @uses JournalNoteParameters::setToDate()
     * @uses JournalNoteParameters::setType()
     * @param int $customTypeId
     * @param string $fromDate
     * @param int $maxEntries
     * @param \Exerp\Person\StructType\ApiPersonKey $personId
     * @param string $toDate
     * @param string $type
     */
    public function __construct(?int $customTypeId = null, ?string $fromDate = null, ?int $maxEntries = null, ?\Exerp\Person\StructType\ApiPersonKey $personId = null, ?string $toDate = null, ?string $type = null)
    {
        $this
            ->setCustomTypeId($customTypeId)
            ->setFromDate($fromDate)
            ->setMaxEntries($maxEntries)
            ->setPersonId($personId)
            ->setToDate($toDate)
            ->setType($type);
    }
    /**
     * Get customTypeId value
     * @return int|null
     */
    public function getCustomTypeId(): ?int
    {
        return $this->customTypeId;
    }
    /**
     * Set customTypeId value
     * @param int $customTypeId
     * @return \Exerp\Person\StructType\JournalNoteParameters
     */
    public function setCustomTypeId(?int $customTypeId = null): self
    {
        // validation for constraint: int
        if (!is_null($customTypeId) && !(is_int($customTypeId) || ctype_digit($customTypeId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($customTypeId, true), gettype($customTypeId)), __LINE__);
        }
        $this->customTypeId = $customTypeId;
        
        return $this;
    }
    /**
     * Get fromDate value
     * @return string|null
     */
    public function getFromDate(): ?string
    {
        return $this->fromDate;
    }
    /**
     * Set fromDate value
     * @param string $fromDate
     * @return \Exerp\Person\StructType\JournalNoteParameters
     */
    public function setFromDate(?string $fromDate = null): self
    {
        // validation for constraint: string
        if (!is_null($fromDate) && !is_string($fromDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($fromDate, true), gettype($fromDate)), __LINE__);
        }
        $this->fromDate = $fromDate;
        
        return $this;
    }
    /**
     * Get maxEntries value
     * @return int|null
     */
    public function getMaxEntries(): ?int
    {
        return $this->maxEntries;
    }
    /**
     * Set maxEntries value
     * @param int $maxEntries
     * @return \Exerp\Person\StructType\JournalNoteParameters
     */
    public function setMaxEntries(?int $maxEntries = null): self
    {
        // validation for constraint: int
        if (!is_null($maxEntries) && !(is_int($maxEntries) || ctype_digit($maxEntries))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($maxEntries, true), gettype($maxEntries)), __LINE__);
        }
        $this->maxEntries = $maxEntries;
        
        return $this;
    }
    /**
     * Get personId value
     * @return \Exerp\Person\StructType\ApiPersonKey|null
     */
    public function getPersonId(): ?\Exerp\Person\StructType\ApiPersonKey
    {
        return $this->personId;
    }
    /**
     * Set personId value
     * @param \Exerp\Person\StructType\ApiPersonKey $personId
     * @return \Exerp\Person\StructType\JournalNoteParameters
     */
    public function setPersonId(?\Exerp\Person\StructType\ApiPersonKey $personId = null): self
    {
        $this->personId = $personId;
        
        return $this;
    }
    /**
     * Get toDate value
     * @return string|null
     */
    public function getToDate(): ?string
    {
        return $this->toDate;
    }
    /**
     * Set toDate value
     * @param string $toDate
     * @return \Exerp\Person\StructType\JournalNoteParameters
     */
    public function setToDate(?string $toDate = null): self
    {
        // validation for constraint: string
        if (!is_null($toDate) && !is_string($toDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($toDate, true), gettype($toDate)), __LINE__);
        }
        $this->toDate = $toDate;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @uses \Exerp\Person\EnumType\JournalNoteType::valueIsValid()
     * @uses \Exerp\Person\EnumType\JournalNoteType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $type
     * @return \Exerp\Person\StructType\JournalNoteParameters
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Person\EnumType\JournalNoteType::valueIsValid($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Person\EnumType\JournalNoteType', is_array($type) ? implode(', ', $type) : var_export($type, true), implode(', ', \Exerp\Person\EnumType\JournalNoteType::getValidValues())), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
