<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for changeSuspensionStatusParameters StructType
 * @subpackage Structs
 */
class ChangeSuspensionStatusParameters extends AbstractStructBase
{
    /**
     * The internalComment
     * @var string|null
     */
    protected ?string $internalComment = null;
    /**
     * The messageToMember
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $messageToMember = null;
    /**
     * The personKey
     * @var \Exerp\Person\StructType\ApiPersonKey|null
     */
    protected ?\Exerp\Person\StructType\ApiPersonKey $personKey = null;
    /**
     * The status
     * @var string|null
     */
    protected ?string $status = null;
    /**
     * Constructor method for changeSuspensionStatusParameters
     * @uses ChangeSuspensionStatusParameters::setInternalComment()
     * @uses ChangeSuspensionStatusParameters::setMessageToMember()
     * @uses ChangeSuspensionStatusParameters::setPersonKey()
     * @uses ChangeSuspensionStatusParameters::setStatus()
     * @param string $internalComment
     * @param string $messageToMember
     * @param \Exerp\Person\StructType\ApiPersonKey $personKey
     * @param string $status
     */
    public function __construct(?string $internalComment = null, ?string $messageToMember = null, ?\Exerp\Person\StructType\ApiPersonKey $personKey = null, ?string $status = null)
    {
        $this
            ->setInternalComment($internalComment)
            ->setMessageToMember($messageToMember)
            ->setPersonKey($personKey)
            ->setStatus($status);
    }
    /**
     * Get internalComment value
     * @return string|null
     */
    public function getInternalComment(): ?string
    {
        return $this->internalComment;
    }
    /**
     * Set internalComment value
     * @param string $internalComment
     * @return \Exerp\Person\StructType\ChangeSuspensionStatusParameters
     */
    public function setInternalComment(?string $internalComment = null): self
    {
        // validation for constraint: string
        if (!is_null($internalComment) && !is_string($internalComment)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($internalComment, true), gettype($internalComment)), __LINE__);
        }
        $this->internalComment = $internalComment;
        
        return $this;
    }
    /**
     * Get messageToMember value
     * @return string|null
     */
    public function getMessageToMember(): ?string
    {
        return $this->messageToMember;
    }
    /**
     * Set messageToMember value
     * @param string $messageToMember
     * @return \Exerp\Person\StructType\ChangeSuspensionStatusParameters
     */
    public function setMessageToMember(?string $messageToMember = null): self
    {
        // validation for constraint: string
        if (!is_null($messageToMember) && !is_string($messageToMember)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($messageToMember, true), gettype($messageToMember)), __LINE__);
        }
        $this->messageToMember = $messageToMember;
        
        return $this;
    }
    /**
     * Get personKey value
     * @return \Exerp\Person\StructType\ApiPersonKey|null
     */
    public function getPersonKey(): ?\Exerp\Person\StructType\ApiPersonKey
    {
        return $this->personKey;
    }
    /**
     * Set personKey value
     * @param \Exerp\Person\StructType\ApiPersonKey $personKey
     * @return \Exerp\Person\StructType\ChangeSuspensionStatusParameters
     */
    public function setPersonKey(?\Exerp\Person\StructType\ApiPersonKey $personKey = null): self
    {
        $this->personKey = $personKey;
        
        return $this;
    }
    /**
     * Get status value
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }
    /**
     * Set status value
     * @uses \Exerp\Person\EnumType\PersonBlacklistedStatus::valueIsValid()
     * @uses \Exerp\Person\EnumType\PersonBlacklistedStatus::getValidValues()
     * @throws InvalidArgumentException
     * @param string $status
     * @return \Exerp\Person\StructType\ChangeSuspensionStatusParameters
     */
    public function setStatus(?string $status = null): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Person\EnumType\PersonBlacklistedStatus::valueIsValid($status)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Person\EnumType\PersonBlacklistedStatus', is_array($status) ? implode(', ', $status) : var_export($status, true), implode(', ', \Exerp\Person\EnumType\PersonBlacklistedStatus::getValidValues())), __LINE__);
        }
        $this->status = $status;
        
        return $this;
    }
}
