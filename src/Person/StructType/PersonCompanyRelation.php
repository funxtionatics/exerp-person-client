<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for personCompanyRelation StructType
 * @subpackage Structs
 */
class PersonCompanyRelation extends AbstractStructBase
{
    /**
     * The agreement
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\CompanyAgreement|null
     */
    protected ?\Exerp\Person\StructType\CompanyAgreement $agreement = null;
    /**
     * The companyName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $companyName = null;
    /**
     * Constructor method for personCompanyRelation
     * @uses PersonCompanyRelation::setAgreement()
     * @uses PersonCompanyRelation::setCompanyName()
     * @param \Exerp\Person\StructType\CompanyAgreement $agreement
     * @param string $companyName
     */
    public function __construct(?\Exerp\Person\StructType\CompanyAgreement $agreement = null, ?string $companyName = null)
    {
        $this
            ->setAgreement($agreement)
            ->setCompanyName($companyName);
    }
    /**
     * Get agreement value
     * @return \Exerp\Person\StructType\CompanyAgreement|null
     */
    public function getAgreement(): ?\Exerp\Person\StructType\CompanyAgreement
    {
        return $this->agreement;
    }
    /**
     * Set agreement value
     * @param \Exerp\Person\StructType\CompanyAgreement $agreement
     * @return \Exerp\Person\StructType\PersonCompanyRelation
     */
    public function setAgreement(?\Exerp\Person\StructType\CompanyAgreement $agreement = null): self
    {
        $this->agreement = $agreement;
        
        return $this;
    }
    /**
     * Get companyName value
     * @return string|null
     */
    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }
    /**
     * Set companyName value
     * @param string $companyName
     * @return \Exerp\Person\StructType\PersonCompanyRelation
     */
    public function setCompanyName(?string $companyName = null): self
    {
        // validation for constraint: string
        if (!is_null($companyName) && !is_string($companyName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($companyName, true), gettype($companyName)), __LINE__);
        }
        $this->companyName = $companyName;
        
        return $this;
    }
}
