<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for availableLanguagesForMembersResponse StructType
 * @subpackage Structs
 */
class AvailableLanguagesForMembersResponse extends AbstractStructBase
{
    /**
     * The languages
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\Languages|null
     */
    protected ?\Exerp\Person\StructType\Languages $languages = null;
    /**
     * Constructor method for availableLanguagesForMembersResponse
     * @uses AvailableLanguagesForMembersResponse::setLanguages()
     * @param \Exerp\Person\StructType\Languages $languages
     */
    public function __construct(?\Exerp\Person\StructType\Languages $languages = null)
    {
        $this
            ->setLanguages($languages);
    }
    /**
     * Get languages value
     * @return \Exerp\Person\StructType\Languages|null
     */
    public function getLanguages(): ?\Exerp\Person\StructType\Languages
    {
        return $this->languages;
    }
    /**
     * Set languages value
     * @param \Exerp\Person\StructType\Languages $languages
     * @return \Exerp\Person\StructType\AvailableLanguagesForMembersResponse
     */
    public function setLanguages(?\Exerp\Person\StructType\Languages $languages = null): self
    {
        $this->languages = $languages;
        
        return $this;
    }
}
