<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for extendedAttributes StructType
 * @subpackage Structs
 */
class ExtendedAttributes extends AbstractStructBase
{
    /**
     * The extendedAttribute
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\ExtendedAttribute[]
     */
    protected ?array $extendedAttribute = null;
    /**
     * Constructor method for extendedAttributes
     * @uses ExtendedAttributes::setExtendedAttribute()
     * @param \Exerp\Person\StructType\ExtendedAttribute[] $extendedAttribute
     */
    public function __construct(?array $extendedAttribute = null)
    {
        $this
            ->setExtendedAttribute($extendedAttribute);
    }
    /**
     * Get extendedAttribute value
     * @return \Exerp\Person\StructType\ExtendedAttribute[]
     */
    public function getExtendedAttribute(): ?array
    {
        return $this->extendedAttribute;
    }
    /**
     * This method is responsible for validating the values passed to the setExtendedAttribute method
     * This method is willingly generated in order to preserve the one-line inline validation within the setExtendedAttribute method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateExtendedAttributeForArrayConstraintsFromSetExtendedAttribute(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $extendedAttributesExtendedAttributeItem) {
            // validation for constraint: itemType
            if (!$extendedAttributesExtendedAttributeItem instanceof \Exerp\Person\StructType\ExtendedAttribute) {
                $invalidValues[] = is_object($extendedAttributesExtendedAttributeItem) ? get_class($extendedAttributesExtendedAttributeItem) : sprintf('%s(%s)', gettype($extendedAttributesExtendedAttributeItem), var_export($extendedAttributesExtendedAttributeItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The extendedAttribute property can only contain items of type \Exerp\Person\StructType\ExtendedAttribute, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set extendedAttribute value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\ExtendedAttribute[] $extendedAttribute
     * @return \Exerp\Person\StructType\ExtendedAttributes
     */
    public function setExtendedAttribute(?array $extendedAttribute = null): self
    {
        // validation for constraint: array
        if ('' !== ($extendedAttributeArrayErrorMessage = self::validateExtendedAttributeForArrayConstraintsFromSetExtendedAttribute($extendedAttribute))) {
            throw new InvalidArgumentException($extendedAttributeArrayErrorMessage, __LINE__);
        }
        $this->extendedAttribute = $extendedAttribute;
        
        return $this;
    }
    /**
     * Add item to extendedAttribute value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\ExtendedAttribute $item
     * @return \Exerp\Person\StructType\ExtendedAttributes
     */
    public function addToExtendedAttribute(\Exerp\Person\StructType\ExtendedAttribute $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Exerp\Person\StructType\ExtendedAttribute) {
            throw new InvalidArgumentException(sprintf('The extendedAttribute property can only contain items of type \Exerp\Person\StructType\ExtendedAttribute, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->extendedAttribute[] = $item;
        
        return $this;
    }
}
