<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for allowedCorporateRelationTypes StructType
 * @subpackage Structs
 */
class AllowedCorporateRelationTypes extends AbstractStructBase
{
    /**
     * The allowedCorporateRelationType
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * @var string[]
     */
    protected ?array $allowedCorporateRelationType = null;
    /**
     * Constructor method for allowedCorporateRelationTypes
     * @uses AllowedCorporateRelationTypes::setAllowedCorporateRelationType()
     * @param string[] $allowedCorporateRelationType
     */
    public function __construct(?array $allowedCorporateRelationType = null)
    {
        $this
            ->setAllowedCorporateRelationType($allowedCorporateRelationType);
    }
    /**
     * Get allowedCorporateRelationType value
     * @return string[]
     */
    public function getAllowedCorporateRelationType(): ?array
    {
        return $this->allowedCorporateRelationType;
    }
    /**
     * This method is responsible for validating the values passed to the setAllowedCorporateRelationType method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAllowedCorporateRelationType method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAllowedCorporateRelationTypeForArrayConstraintsFromSetAllowedCorporateRelationType(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $allowedCorporateRelationTypesAllowedCorporateRelationTypeItem) {
            // validation for constraint: enumeration
            if (!\Exerp\Person\EnumType\CorporateRelationType::valueIsValid($allowedCorporateRelationTypesAllowedCorporateRelationTypeItem)) {
                $invalidValues[] = is_object($allowedCorporateRelationTypesAllowedCorporateRelationTypeItem) ? get_class($allowedCorporateRelationTypesAllowedCorporateRelationTypeItem) : sprintf('%s(%s)', gettype($allowedCorporateRelationTypesAllowedCorporateRelationTypeItem), var_export($allowedCorporateRelationTypesAllowedCorporateRelationTypeItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Person\EnumType\CorporateRelationType', is_array($invalidValues) ? implode(', ', $invalidValues) : var_export($invalidValues, true), implode(', ', \Exerp\Person\EnumType\CorporateRelationType::getValidValues()));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set allowedCorporateRelationType value
     * @uses \Exerp\Person\EnumType\CorporateRelationType::valueIsValid()
     * @uses \Exerp\Person\EnumType\CorporateRelationType::getValidValues()
     * @throws InvalidArgumentException
     * @param string[] $allowedCorporateRelationType
     * @return \Exerp\Person\StructType\AllowedCorporateRelationTypes
     */
    public function setAllowedCorporateRelationType(?array $allowedCorporateRelationType = null): self
    {
        // validation for constraint: array
        if ('' !== ($allowedCorporateRelationTypeArrayErrorMessage = self::validateAllowedCorporateRelationTypeForArrayConstraintsFromSetAllowedCorporateRelationType($allowedCorporateRelationType))) {
            throw new InvalidArgumentException($allowedCorporateRelationTypeArrayErrorMessage, __LINE__);
        }
        $this->allowedCorporateRelationType = $allowedCorporateRelationType;
        
        return $this;
    }
    /**
     * Add item to allowedCorporateRelationType value
     * @uses \Exerp\Person\EnumType\CorporateRelationType::valueIsValid()
     * @uses \Exerp\Person\EnumType\CorporateRelationType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $item
     * @return \Exerp\Person\StructType\AllowedCorporateRelationTypes
     */
    public function addToAllowedCorporateRelationType(string $item): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Person\EnumType\CorporateRelationType::valueIsValid($item)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Person\EnumType\CorporateRelationType', is_array($item) ? implode(', ', $item) : var_export($item, true), implode(', ', \Exerp\Person\EnumType\CorporateRelationType::getValidValues())), __LINE__);
        }
        $this->allowedCorporateRelationType[] = $item;
        
        return $this;
    }
}
