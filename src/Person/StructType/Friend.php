<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for friend StructType
 * @subpackage Structs
 */
class Friend extends AbstractStructBase
{
    /**
     * The name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $name = null;
    /**
     * The personKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\CompositeKey|null
     */
    protected ?\Exerp\Person\StructType\CompositeKey $personKey = null;
    /**
     * Constructor method for friend
     * @uses Friend::setName()
     * @uses Friend::setPersonKey()
     * @param string $name
     * @param \Exerp\Person\StructType\CompositeKey $personKey
     */
    public function __construct(?string $name = null, ?\Exerp\Person\StructType\CompositeKey $personKey = null)
    {
        $this
            ->setName($name)
            ->setPersonKey($personKey);
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \Exerp\Person\StructType\Friend
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        
        return $this;
    }
    /**
     * Get personKey value
     * @return \Exerp\Person\StructType\CompositeKey|null
     */
    public function getPersonKey(): ?\Exerp\Person\StructType\CompositeKey
    {
        return $this->personKey;
    }
    /**
     * Set personKey value
     * @param \Exerp\Person\StructType\CompositeKey $personKey
     * @return \Exerp\Person\StructType\Friend
     */
    public function setPersonKey(?\Exerp\Person\StructType\CompositeKey $personKey = null): self
    {
        $this->personKey = $personKey;
        
        return $this;
    }
}
