<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for compositeSubKey StructType
 * @subpackage Structs
 */
class CompositeSubKey extends CompositeKey
{
    /**
     * The subId
     * @var int|null
     */
    protected ?int $subId = null;
    /**
     * Constructor method for compositeSubKey
     * @uses CompositeSubKey::setSubId()
     * @param int $subId
     */
    public function __construct(?int $subId = null)
    {
        $this
            ->setSubId($subId);
    }
    /**
     * Get subId value
     * @return int|null
     */
    public function getSubId(): ?int
    {
        return $this->subId;
    }
    /**
     * Set subId value
     * @param int $subId
     * @return \Exerp\Person\StructType\CompositeSubKey
     */
    public function setSubId(?int $subId = null): self
    {
        // validation for constraint: int
        if (!is_null($subId) && !(is_int($subId) || ctype_digit($subId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($subId, true), gettype($subId)), __LINE__);
        }
        $this->subId = $subId;
        
        return $this;
    }
}
