<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for compositeKey StructType
 * @subpackage Structs
 */
class CompositeKey extends AbstractStructBase
{
    /**
     * The center
     * @var int|null
     */
    protected ?int $center = null;
    /**
     * The id
     * @var int|null
     */
    protected ?int $id = null;
    /**
     * Constructor method for compositeKey
     * @uses CompositeKey::setCenter()
     * @uses CompositeKey::setId()
     * @param int $center
     * @param int $id
     */
    public function __construct(?int $center = null, ?int $id = null)
    {
        $this
            ->setCenter($center)
            ->setId($id);
    }
    /**
     * Get center value
     * @return int|null
     */
    public function getCenter(): ?int
    {
        return $this->center;
    }
    /**
     * Set center value
     * @param int $center
     * @return \Exerp\Person\StructType\CompositeKey
     */
    public function setCenter(?int $center = null): self
    {
        // validation for constraint: int
        if (!is_null($center) && !(is_int($center) || ctype_digit($center))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($center, true), gettype($center)), __LINE__);
        }
        $this->center = $center;
        
        return $this;
    }
    /**
     * Get id value
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }
    /**
     * Set id value
     * @param int $id
     * @return \Exerp\Person\StructType\CompositeKey
     */
    public function setId(?int $id = null): self
    {
        // validation for constraint: int
        if (!is_null($id) && !(is_int($id) || ctype_digit($id))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($id, true), gettype($id)), __LINE__);
        }
        $this->id = $id;
        
        return $this;
    }
}
