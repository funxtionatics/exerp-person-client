<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for person StructType
 * @subpackage Structs
 */
class Person extends AbstractStructBase
{
    /**
     * The address
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\Address|null
     */
    protected ?\Exerp\Person\StructType\Address $address = null;
    /**
     * The birthday
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $birthday = null;
    /**
     * The comment
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $comment = null;
    /**
     * The corporateRelationType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $corporateRelationType = null;
    /**
     * The employeeNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $employeeNumber = null;
    /**
     * The externalId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $externalId = null;
    /**
     * The firstName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $firstName = null;
    /**
     * The gender
     * @var string|null
     */
    protected ?string $gender = null;
    /**
     * The lastName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $lastName = null;
    /**
     * The nationalID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $nationalID = null;
    /**
     * The nickName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $nickName = null;
    /**
     * The passport
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\Passport|null
     */
    protected ?\Exerp\Person\StructType\Passport $passport = null;
    /**
     * The personId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\ApiPersonKey|null
     */
    protected ?\Exerp\Person\StructType\ApiPersonKey $personId = null;
    /**
     * The personType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $personType = null;
    /**
     * The relatedPersonId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\ApiPersonKey|null
     */
    protected ?\Exerp\Person\StructType\ApiPersonKey $relatedPersonId = null;
    /**
     * The residentID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $residentID = null;
    /**
     * The salutation
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $salutation = null;
    /**
     * The socialSecurityNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $socialSecurityNumber = null;
    /**
     * The status
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $status = null;
    /**
     * Constructor method for person
     * @uses Person::setAddress()
     * @uses Person::setBirthday()
     * @uses Person::setComment()
     * @uses Person::setCorporateRelationType()
     * @uses Person::setEmployeeNumber()
     * @uses Person::setExternalId()
     * @uses Person::setFirstName()
     * @uses Person::setGender()
     * @uses Person::setLastName()
     * @uses Person::setNationalID()
     * @uses Person::setNickName()
     * @uses Person::setPassport()
     * @uses Person::setPersonId()
     * @uses Person::setPersonType()
     * @uses Person::setRelatedPersonId()
     * @uses Person::setResidentID()
     * @uses Person::setSalutation()
     * @uses Person::setSocialSecurityNumber()
     * @uses Person::setStatus()
     * @param \Exerp\Person\StructType\Address $address
     * @param string $birthday
     * @param string $comment
     * @param string $corporateRelationType
     * @param string $employeeNumber
     * @param string $externalId
     * @param string $firstName
     * @param string $gender
     * @param string $lastName
     * @param string $nationalID
     * @param string $nickName
     * @param \Exerp\Person\StructType\Passport $passport
     * @param \Exerp\Person\StructType\ApiPersonKey $personId
     * @param string $personType
     * @param \Exerp\Person\StructType\ApiPersonKey $relatedPersonId
     * @param string $residentID
     * @param string $salutation
     * @param string $socialSecurityNumber
     * @param string $status
     */
    public function __construct(?\Exerp\Person\StructType\Address $address = null, ?string $birthday = null, ?string $comment = null, ?string $corporateRelationType = null, ?string $employeeNumber = null, ?string $externalId = null, ?string $firstName = null, ?string $gender = null, ?string $lastName = null, ?string $nationalID = null, ?string $nickName = null, ?\Exerp\Person\StructType\Passport $passport = null, ?\Exerp\Person\StructType\ApiPersonKey $personId = null, ?string $personType = null, ?\Exerp\Person\StructType\ApiPersonKey $relatedPersonId = null, ?string $residentID = null, ?string $salutation = null, ?string $socialSecurityNumber = null, ?string $status = null)
    {
        $this
            ->setAddress($address)
            ->setBirthday($birthday)
            ->setComment($comment)
            ->setCorporateRelationType($corporateRelationType)
            ->setEmployeeNumber($employeeNumber)
            ->setExternalId($externalId)
            ->setFirstName($firstName)
            ->setGender($gender)
            ->setLastName($lastName)
            ->setNationalID($nationalID)
            ->setNickName($nickName)
            ->setPassport($passport)
            ->setPersonId($personId)
            ->setPersonType($personType)
            ->setRelatedPersonId($relatedPersonId)
            ->setResidentID($residentID)
            ->setSalutation($salutation)
            ->setSocialSecurityNumber($socialSecurityNumber)
            ->setStatus($status);
    }
    /**
     * Get address value
     * @return \Exerp\Person\StructType\Address|null
     */
    public function getAddress(): ?\Exerp\Person\StructType\Address
    {
        return $this->address;
    }
    /**
     * Set address value
     * @param \Exerp\Person\StructType\Address $address
     * @return \Exerp\Person\StructType\Person
     */
    public function setAddress(?\Exerp\Person\StructType\Address $address = null): self
    {
        $this->address = $address;
        
        return $this;
    }
    /**
     * Get birthday value
     * @return string|null
     */
    public function getBirthday(): ?string
    {
        return $this->birthday;
    }
    /**
     * Set birthday value
     * @param string $birthday
     * @return \Exerp\Person\StructType\Person
     */
    public function setBirthday(?string $birthday = null): self
    {
        // validation for constraint: string
        if (!is_null($birthday) && !is_string($birthday)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($birthday, true), gettype($birthday)), __LINE__);
        }
        $this->birthday = $birthday;
        
        return $this;
    }
    /**
     * Get comment value
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }
    /**
     * Set comment value
     * @param string $comment
     * @return \Exerp\Person\StructType\Person
     */
    public function setComment(?string $comment = null): self
    {
        // validation for constraint: string
        if (!is_null($comment) && !is_string($comment)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($comment, true), gettype($comment)), __LINE__);
        }
        $this->comment = $comment;
        
        return $this;
    }
    /**
     * Get corporateRelationType value
     * @return string|null
     */
    public function getCorporateRelationType(): ?string
    {
        return $this->corporateRelationType;
    }
    /**
     * Set corporateRelationType value
     * @uses \Exerp\Person\EnumType\CorporateRelationType::valueIsValid()
     * @uses \Exerp\Person\EnumType\CorporateRelationType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $corporateRelationType
     * @return \Exerp\Person\StructType\Person
     */
    public function setCorporateRelationType(?string $corporateRelationType = null): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Person\EnumType\CorporateRelationType::valueIsValid($corporateRelationType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Person\EnumType\CorporateRelationType', is_array($corporateRelationType) ? implode(', ', $corporateRelationType) : var_export($corporateRelationType, true), implode(', ', \Exerp\Person\EnumType\CorporateRelationType::getValidValues())), __LINE__);
        }
        $this->corporateRelationType = $corporateRelationType;
        
        return $this;
    }
    /**
     * Get employeeNumber value
     * @return string|null
     */
    public function getEmployeeNumber(): ?string
    {
        return $this->employeeNumber;
    }
    /**
     * Set employeeNumber value
     * @param string $employeeNumber
     * @return \Exerp\Person\StructType\Person
     */
    public function setEmployeeNumber(?string $employeeNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($employeeNumber) && !is_string($employeeNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($employeeNumber, true), gettype($employeeNumber)), __LINE__);
        }
        $this->employeeNumber = $employeeNumber;
        
        return $this;
    }
    /**
     * Get externalId value
     * @return string|null
     */
    public function getExternalId(): ?string
    {
        return $this->externalId;
    }
    /**
     * Set externalId value
     * @param string $externalId
     * @return \Exerp\Person\StructType\Person
     */
    public function setExternalId(?string $externalId = null): self
    {
        // validation for constraint: string
        if (!is_null($externalId) && !is_string($externalId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($externalId, true), gettype($externalId)), __LINE__);
        }
        $this->externalId = $externalId;
        
        return $this;
    }
    /**
     * Get firstName value
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }
    /**
     * Set firstName value
     * @param string $firstName
     * @return \Exerp\Person\StructType\Person
     */
    public function setFirstName(?string $firstName = null): self
    {
        // validation for constraint: string
        if (!is_null($firstName) && !is_string($firstName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($firstName, true), gettype($firstName)), __LINE__);
        }
        $this->firstName = $firstName;
        
        return $this;
    }
    /**
     * Get gender value
     * @return string|null
     */
    public function getGender(): ?string
    {
        return $this->gender;
    }
    /**
     * Set gender value
     * @uses \Exerp\Person\EnumType\ApiGender::valueIsValid()
     * @uses \Exerp\Person\EnumType\ApiGender::getValidValues()
     * @throws InvalidArgumentException
     * @param string $gender
     * @return \Exerp\Person\StructType\Person
     */
    public function setGender(?string $gender = null): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Person\EnumType\ApiGender::valueIsValid($gender)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Person\EnumType\ApiGender', is_array($gender) ? implode(', ', $gender) : var_export($gender, true), implode(', ', \Exerp\Person\EnumType\ApiGender::getValidValues())), __LINE__);
        }
        $this->gender = $gender;
        
        return $this;
    }
    /**
     * Get lastName value
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }
    /**
     * Set lastName value
     * @param string $lastName
     * @return \Exerp\Person\StructType\Person
     */
    public function setLastName(?string $lastName = null): self
    {
        // validation for constraint: string
        if (!is_null($lastName) && !is_string($lastName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($lastName, true), gettype($lastName)), __LINE__);
        }
        $this->lastName = $lastName;
        
        return $this;
    }
    /**
     * Get nationalID value
     * @return string|null
     */
    public function getNationalID(): ?string
    {
        return $this->nationalID;
    }
    /**
     * Set nationalID value
     * @param string $nationalID
     * @return \Exerp\Person\StructType\Person
     */
    public function setNationalID(?string $nationalID = null): self
    {
        // validation for constraint: string
        if (!is_null($nationalID) && !is_string($nationalID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($nationalID, true), gettype($nationalID)), __LINE__);
        }
        $this->nationalID = $nationalID;
        
        return $this;
    }
    /**
     * Get nickName value
     * @return string|null
     */
    public function getNickName(): ?string
    {
        return $this->nickName;
    }
    /**
     * Set nickName value
     * @param string $nickName
     * @return \Exerp\Person\StructType\Person
     */
    public function setNickName(?string $nickName = null): self
    {
        // validation for constraint: string
        if (!is_null($nickName) && !is_string($nickName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($nickName, true), gettype($nickName)), __LINE__);
        }
        $this->nickName = $nickName;
        
        return $this;
    }
    /**
     * Get passport value
     * @return \Exerp\Person\StructType\Passport|null
     */
    public function getPassport(): ?\Exerp\Person\StructType\Passport
    {
        return $this->passport;
    }
    /**
     * Set passport value
     * @param \Exerp\Person\StructType\Passport $passport
     * @return \Exerp\Person\StructType\Person
     */
    public function setPassport(?\Exerp\Person\StructType\Passport $passport = null): self
    {
        $this->passport = $passport;
        
        return $this;
    }
    /**
     * Get personId value
     * @return \Exerp\Person\StructType\ApiPersonKey|null
     */
    public function getPersonId(): ?\Exerp\Person\StructType\ApiPersonKey
    {
        return $this->personId;
    }
    /**
     * Set personId value
     * @param \Exerp\Person\StructType\ApiPersonKey $personId
     * @return \Exerp\Person\StructType\Person
     */
    public function setPersonId(?\Exerp\Person\StructType\ApiPersonKey $personId = null): self
    {
        $this->personId = $personId;
        
        return $this;
    }
    /**
     * Get personType value
     * @return string|null
     */
    public function getPersonType(): ?string
    {
        return $this->personType;
    }
    /**
     * Set personType value
     * @uses \Exerp\Person\EnumType\PersonType::valueIsValid()
     * @uses \Exerp\Person\EnumType\PersonType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $personType
     * @return \Exerp\Person\StructType\Person
     */
    public function setPersonType(?string $personType = null): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Person\EnumType\PersonType::valueIsValid($personType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Person\EnumType\PersonType', is_array($personType) ? implode(', ', $personType) : var_export($personType, true), implode(', ', \Exerp\Person\EnumType\PersonType::getValidValues())), __LINE__);
        }
        $this->personType = $personType;
        
        return $this;
    }
    /**
     * Get relatedPersonId value
     * @return \Exerp\Person\StructType\ApiPersonKey|null
     */
    public function getRelatedPersonId(): ?\Exerp\Person\StructType\ApiPersonKey
    {
        return $this->relatedPersonId;
    }
    /**
     * Set relatedPersonId value
     * @param \Exerp\Person\StructType\ApiPersonKey $relatedPersonId
     * @return \Exerp\Person\StructType\Person
     */
    public function setRelatedPersonId(?\Exerp\Person\StructType\ApiPersonKey $relatedPersonId = null): self
    {
        $this->relatedPersonId = $relatedPersonId;
        
        return $this;
    }
    /**
     * Get residentID value
     * @return string|null
     */
    public function getResidentID(): ?string
    {
        return $this->residentID;
    }
    /**
     * Set residentID value
     * @param string $residentID
     * @return \Exerp\Person\StructType\Person
     */
    public function setResidentID(?string $residentID = null): self
    {
        // validation for constraint: string
        if (!is_null($residentID) && !is_string($residentID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($residentID, true), gettype($residentID)), __LINE__);
        }
        $this->residentID = $residentID;
        
        return $this;
    }
    /**
     * Get salutation value
     * @return string|null
     */
    public function getSalutation(): ?string
    {
        return $this->salutation;
    }
    /**
     * Set salutation value
     * @param string $salutation
     * @return \Exerp\Person\StructType\Person
     */
    public function setSalutation(?string $salutation = null): self
    {
        // validation for constraint: string
        if (!is_null($salutation) && !is_string($salutation)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($salutation, true), gettype($salutation)), __LINE__);
        }
        $this->salutation = $salutation;
        
        return $this;
    }
    /**
     * Get socialSecurityNumber value
     * @return string|null
     */
    public function getSocialSecurityNumber(): ?string
    {
        return $this->socialSecurityNumber;
    }
    /**
     * Set socialSecurityNumber value
     * @param string $socialSecurityNumber
     * @return \Exerp\Person\StructType\Person
     */
    public function setSocialSecurityNumber(?string $socialSecurityNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($socialSecurityNumber) && !is_string($socialSecurityNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($socialSecurityNumber, true), gettype($socialSecurityNumber)), __LINE__);
        }
        $this->socialSecurityNumber = $socialSecurityNumber;
        
        return $this;
    }
    /**
     * Get status value
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }
    /**
     * Set status value
     * @uses \Exerp\Person\EnumType\PersonStatus::valueIsValid()
     * @uses \Exerp\Person\EnumType\PersonStatus::getValidValues()
     * @throws InvalidArgumentException
     * @param string $status
     * @return \Exerp\Person\StructType\Person
     */
    public function setStatus(?string $status = null): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Person\EnumType\PersonStatus::valueIsValid($status)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Person\EnumType\PersonStatus', is_array($status) ? implode(', ', $status) : var_export($status, true), implode(', ', \Exerp\Person\EnumType\PersonStatus::getValidValues())), __LINE__);
        }
        $this->status = $status;
        
        return $this;
    }
}
