<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for transferPersonInfo StructType
 * @subpackage Structs
 */
class TransferPersonInfo extends AbstractStructBase
{
    /**
     * The person
     * @var \Exerp\Person\StructType\Person|null
     */
    protected ?\Exerp\Person\StructType\Person $person = null;
    /**
     * The subscriptions
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\Subscription[]
     */
    protected ?array $subscriptions = null;
    /**
     * Constructor method for transferPersonInfo
     * @uses TransferPersonInfo::setPerson()
     * @uses TransferPersonInfo::setSubscriptions()
     * @param \Exerp\Person\StructType\Person $person
     * @param \Exerp\Person\StructType\Subscription[] $subscriptions
     */
    public function __construct(?\Exerp\Person\StructType\Person $person = null, ?array $subscriptions = null)
    {
        $this
            ->setPerson($person)
            ->setSubscriptions($subscriptions);
    }
    /**
     * Get person value
     * @return \Exerp\Person\StructType\Person|null
     */
    public function getPerson(): ?\Exerp\Person\StructType\Person
    {
        return $this->person;
    }
    /**
     * Set person value
     * @param \Exerp\Person\StructType\Person $person
     * @return \Exerp\Person\StructType\TransferPersonInfo
     */
    public function setPerson(?\Exerp\Person\StructType\Person $person = null): self
    {
        $this->person = $person;
        
        return $this;
    }
    /**
     * Get subscriptions value
     * @return \Exerp\Person\StructType\Subscription[]
     */
    public function getSubscriptions(): ?array
    {
        return $this->subscriptions;
    }
    /**
     * This method is responsible for validating the values passed to the setSubscriptions method
     * This method is willingly generated in order to preserve the one-line inline validation within the setSubscriptions method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateSubscriptionsForArrayConstraintsFromSetSubscriptions(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $transferPersonInfoSubscriptionsItem) {
            // validation for constraint: itemType
            if (!$transferPersonInfoSubscriptionsItem instanceof \Exerp\Person\StructType\Subscription) {
                $invalidValues[] = is_object($transferPersonInfoSubscriptionsItem) ? get_class($transferPersonInfoSubscriptionsItem) : sprintf('%s(%s)', gettype($transferPersonInfoSubscriptionsItem), var_export($transferPersonInfoSubscriptionsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The subscriptions property can only contain items of type \Exerp\Person\StructType\Subscription, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set subscriptions value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\Subscription[] $subscriptions
     * @return \Exerp\Person\StructType\TransferPersonInfo
     */
    public function setSubscriptions(?array $subscriptions = null): self
    {
        // validation for constraint: array
        if ('' !== ($subscriptionsArrayErrorMessage = self::validateSubscriptionsForArrayConstraintsFromSetSubscriptions($subscriptions))) {
            throw new InvalidArgumentException($subscriptionsArrayErrorMessage, __LINE__);
        }
        $this->subscriptions = $subscriptions;
        
        return $this;
    }
    /**
     * Add item to subscriptions value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\Subscription $item
     * @return \Exerp\Person\StructType\TransferPersonInfo
     */
    public function addToSubscriptions(\Exerp\Person\StructType\Subscription $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Exerp\Person\StructType\Subscription) {
            throw new InvalidArgumentException(sprintf('The subscriptions property can only contain items of type \Exerp\Person\StructType\Subscription, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->subscriptions[] = $item;
        
        return $this;
    }
}
