<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TransferException StructType
 * Meta information extracted from the WSDL
 * - type: tns:TransferException
 * @subpackage Structs
 */
class TransferException extends AbstractStructBase
{
    /**
     * The availableProductsInfo
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\AvailableProduct[]
     */
    protected ?array $availableProductsInfo = null;
    /**
     * The errorCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $errorCode = null;
    /**
     * The errorDetail
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\ErrorDetail[]
     */
    protected ?array $errorDetail = null;
    /**
     * The errorMessage
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $errorMessage = null;
    /**
     * The message
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $message = null;
    /**
     * The missingSubscriptionsInfo
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\MissingSubscriptionInfo[]
     */
    protected ?array $missingSubscriptionsInfo = null;
    /**
     * Constructor method for TransferException
     * @uses TransferException::setAvailableProductsInfo()
     * @uses TransferException::setErrorCode()
     * @uses TransferException::setErrorDetail()
     * @uses TransferException::setErrorMessage()
     * @uses TransferException::setMessage()
     * @uses TransferException::setMissingSubscriptionsInfo()
     * @param \Exerp\Person\StructType\AvailableProduct[] $availableProductsInfo
     * @param string $errorCode
     * @param \Exerp\Person\StructType\ErrorDetail[] $errorDetail
     * @param string $errorMessage
     * @param string $message
     * @param \Exerp\Person\StructType\MissingSubscriptionInfo[] $missingSubscriptionsInfo
     */
    public function __construct(?array $availableProductsInfo = null, ?string $errorCode = null, ?array $errorDetail = null, ?string $errorMessage = null, ?string $message = null, ?array $missingSubscriptionsInfo = null)
    {
        $this
            ->setAvailableProductsInfo($availableProductsInfo)
            ->setErrorCode($errorCode)
            ->setErrorDetail($errorDetail)
            ->setErrorMessage($errorMessage)
            ->setMessage($message)
            ->setMissingSubscriptionsInfo($missingSubscriptionsInfo);
    }
    /**
     * Get availableProductsInfo value
     * @return \Exerp\Person\StructType\AvailableProduct[]
     */
    public function getAvailableProductsInfo(): ?array
    {
        return $this->availableProductsInfo;
    }
    /**
     * This method is responsible for validating the values passed to the setAvailableProductsInfo method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAvailableProductsInfo method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAvailableProductsInfoForArrayConstraintsFromSetAvailableProductsInfo(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $transferExceptionAvailableProductsInfoItem) {
            // validation for constraint: itemType
            if (!$transferExceptionAvailableProductsInfoItem instanceof \Exerp\Person\StructType\AvailableProduct) {
                $invalidValues[] = is_object($transferExceptionAvailableProductsInfoItem) ? get_class($transferExceptionAvailableProductsInfoItem) : sprintf('%s(%s)', gettype($transferExceptionAvailableProductsInfoItem), var_export($transferExceptionAvailableProductsInfoItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The availableProductsInfo property can only contain items of type \Exerp\Person\StructType\AvailableProduct, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set availableProductsInfo value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\AvailableProduct[] $availableProductsInfo
     * @return \Exerp\Person\StructType\TransferException
     */
    public function setAvailableProductsInfo(?array $availableProductsInfo = null): self
    {
        // validation for constraint: array
        if ('' !== ($availableProductsInfoArrayErrorMessage = self::validateAvailableProductsInfoForArrayConstraintsFromSetAvailableProductsInfo($availableProductsInfo))) {
            throw new InvalidArgumentException($availableProductsInfoArrayErrorMessage, __LINE__);
        }
        $this->availableProductsInfo = $availableProductsInfo;
        
        return $this;
    }
    /**
     * Add item to availableProductsInfo value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\AvailableProduct $item
     * @return \Exerp\Person\StructType\TransferException
     */
    public function addToAvailableProductsInfo(\Exerp\Person\StructType\AvailableProduct $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Exerp\Person\StructType\AvailableProduct) {
            throw new InvalidArgumentException(sprintf('The availableProductsInfo property can only contain items of type \Exerp\Person\StructType\AvailableProduct, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->availableProductsInfo[] = $item;
        
        return $this;
    }
    /**
     * Get errorCode value
     * @return string|null
     */
    public function getErrorCode(): ?string
    {
        return $this->errorCode;
    }
    /**
     * Set errorCode value
     * @uses \Exerp\Person\EnumType\ErrorCode::valueIsValid()
     * @uses \Exerp\Person\EnumType\ErrorCode::getValidValues()
     * @throws InvalidArgumentException
     * @param string $errorCode
     * @return \Exerp\Person\StructType\TransferException
     */
    public function setErrorCode(?string $errorCode = null): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Person\EnumType\ErrorCode::valueIsValid($errorCode)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Person\EnumType\ErrorCode', is_array($errorCode) ? implode(', ', $errorCode) : var_export($errorCode, true), implode(', ', \Exerp\Person\EnumType\ErrorCode::getValidValues())), __LINE__);
        }
        $this->errorCode = $errorCode;
        
        return $this;
    }
    /**
     * Get errorDetail value
     * @return \Exerp\Person\StructType\ErrorDetail[]
     */
    public function getErrorDetail(): ?array
    {
        return $this->errorDetail;
    }
    /**
     * This method is responsible for validating the values passed to the setErrorDetail method
     * This method is willingly generated in order to preserve the one-line inline validation within the setErrorDetail method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateErrorDetailForArrayConstraintsFromSetErrorDetail(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $transferExceptionErrorDetailItem) {
            // validation for constraint: itemType
            if (!$transferExceptionErrorDetailItem instanceof \Exerp\Person\StructType\ErrorDetail) {
                $invalidValues[] = is_object($transferExceptionErrorDetailItem) ? get_class($transferExceptionErrorDetailItem) : sprintf('%s(%s)', gettype($transferExceptionErrorDetailItem), var_export($transferExceptionErrorDetailItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The errorDetail property can only contain items of type \Exerp\Person\StructType\ErrorDetail, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set errorDetail value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\ErrorDetail[] $errorDetail
     * @return \Exerp\Person\StructType\TransferException
     */
    public function setErrorDetail(?array $errorDetail = null): self
    {
        // validation for constraint: array
        if ('' !== ($errorDetailArrayErrorMessage = self::validateErrorDetailForArrayConstraintsFromSetErrorDetail($errorDetail))) {
            throw new InvalidArgumentException($errorDetailArrayErrorMessage, __LINE__);
        }
        $this->errorDetail = $errorDetail;
        
        return $this;
    }
    /**
     * Add item to errorDetail value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\ErrorDetail $item
     * @return \Exerp\Person\StructType\TransferException
     */
    public function addToErrorDetail(\Exerp\Person\StructType\ErrorDetail $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Exerp\Person\StructType\ErrorDetail) {
            throw new InvalidArgumentException(sprintf('The errorDetail property can only contain items of type \Exerp\Person\StructType\ErrorDetail, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->errorDetail[] = $item;
        
        return $this;
    }
    /**
     * Get errorMessage value
     * @return string|null
     */
    public function getErrorMessage(): ?string
    {
        return $this->errorMessage;
    }
    /**
     * Set errorMessage value
     * @param string $errorMessage
     * @return \Exerp\Person\StructType\TransferException
     */
    public function setErrorMessage(?string $errorMessage = null): self
    {
        // validation for constraint: string
        if (!is_null($errorMessage) && !is_string($errorMessage)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($errorMessage, true), gettype($errorMessage)), __LINE__);
        }
        $this->errorMessage = $errorMessage;
        
        return $this;
    }
    /**
     * Get message value
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }
    /**
     * Set message value
     * @param string $message
     * @return \Exerp\Person\StructType\TransferException
     */
    public function setMessage(?string $message = null): self
    {
        // validation for constraint: string
        if (!is_null($message) && !is_string($message)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($message, true), gettype($message)), __LINE__);
        }
        $this->message = $message;
        
        return $this;
    }
    /**
     * Get missingSubscriptionsInfo value
     * @return \Exerp\Person\StructType\MissingSubscriptionInfo[]
     */
    public function getMissingSubscriptionsInfo(): ?array
    {
        return $this->missingSubscriptionsInfo;
    }
    /**
     * This method is responsible for validating the values passed to the setMissingSubscriptionsInfo method
     * This method is willingly generated in order to preserve the one-line inline validation within the setMissingSubscriptionsInfo method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateMissingSubscriptionsInfoForArrayConstraintsFromSetMissingSubscriptionsInfo(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $transferExceptionMissingSubscriptionsInfoItem) {
            // validation for constraint: itemType
            if (!$transferExceptionMissingSubscriptionsInfoItem instanceof \Exerp\Person\StructType\MissingSubscriptionInfo) {
                $invalidValues[] = is_object($transferExceptionMissingSubscriptionsInfoItem) ? get_class($transferExceptionMissingSubscriptionsInfoItem) : sprintf('%s(%s)', gettype($transferExceptionMissingSubscriptionsInfoItem), var_export($transferExceptionMissingSubscriptionsInfoItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The missingSubscriptionsInfo property can only contain items of type \Exerp\Person\StructType\MissingSubscriptionInfo, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set missingSubscriptionsInfo value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\MissingSubscriptionInfo[] $missingSubscriptionsInfo
     * @return \Exerp\Person\StructType\TransferException
     */
    public function setMissingSubscriptionsInfo(?array $missingSubscriptionsInfo = null): self
    {
        // validation for constraint: array
        if ('' !== ($missingSubscriptionsInfoArrayErrorMessage = self::validateMissingSubscriptionsInfoForArrayConstraintsFromSetMissingSubscriptionsInfo($missingSubscriptionsInfo))) {
            throw new InvalidArgumentException($missingSubscriptionsInfoArrayErrorMessage, __LINE__);
        }
        $this->missingSubscriptionsInfo = $missingSubscriptionsInfo;
        
        return $this;
    }
    /**
     * Add item to missingSubscriptionsInfo value
     * @throws InvalidArgumentException
     * @param \Exerp\Person\StructType\MissingSubscriptionInfo $item
     * @return \Exerp\Person\StructType\TransferException
     */
    public function addToMissingSubscriptionsInfo(\Exerp\Person\StructType\MissingSubscriptionInfo $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Exerp\Person\StructType\MissingSubscriptionInfo) {
            throw new InvalidArgumentException(sprintf('The missingSubscriptionsInfo property can only contain items of type \Exerp\Person\StructType\MissingSubscriptionInfo, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->missingSubscriptionsInfo[] = $item;
        
        return $this;
    }
}
