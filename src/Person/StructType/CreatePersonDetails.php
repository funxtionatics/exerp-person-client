<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for createPersonDetails StructType
 * @subpackage Structs
 */
class CreatePersonDetails extends AbstractStructBase
{
    /**
     * The person
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\Person|null
     */
    protected ?\Exerp\Person\StructType\Person $person = null;
    /**
     * The personCommunication
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\PersonCommunication|null
     */
    protected ?\Exerp\Person\StructType\PersonCommunication $personCommunication = null;
    /**
     * The password
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $password = null;
    /**
     * The pincode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $pincode = null;
    /**
     * The extendedAttributes
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\ExtendedAttributes|null
     */
    protected ?\Exerp\Person\StructType\ExtendedAttributes $extendedAttributes = null;
    /**
     * The extendedAttributeMIMEs
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\ExtendedAttributeMIMEs|null
     */
    protected ?\Exerp\Person\StructType\ExtendedAttributeMIMEs $extendedAttributeMIMEs = null;
    /**
     * Constructor method for createPersonDetails
     * @uses CreatePersonDetails::setPerson()
     * @uses CreatePersonDetails::setPersonCommunication()
     * @uses CreatePersonDetails::setPassword()
     * @uses CreatePersonDetails::setPincode()
     * @uses CreatePersonDetails::setExtendedAttributes()
     * @uses CreatePersonDetails::setExtendedAttributeMIMEs()
     * @param \Exerp\Person\StructType\Person $person
     * @param \Exerp\Person\StructType\PersonCommunication $personCommunication
     * @param string $password
     * @param string $pincode
     * @param \Exerp\Person\StructType\ExtendedAttributes $extendedAttributes
     * @param \Exerp\Person\StructType\ExtendedAttributeMIMEs $extendedAttributeMIMEs
     */
    public function __construct(?\Exerp\Person\StructType\Person $person = null, ?\Exerp\Person\StructType\PersonCommunication $personCommunication = null, ?string $password = null, ?string $pincode = null, ?\Exerp\Person\StructType\ExtendedAttributes $extendedAttributes = null, ?\Exerp\Person\StructType\ExtendedAttributeMIMEs $extendedAttributeMIMEs = null)
    {
        $this
            ->setPerson($person)
            ->setPersonCommunication($personCommunication)
            ->setPassword($password)
            ->setPincode($pincode)
            ->setExtendedAttributes($extendedAttributes)
            ->setExtendedAttributeMIMEs($extendedAttributeMIMEs);
    }
    /**
     * Get person value
     * @return \Exerp\Person\StructType\Person|null
     */
    public function getPerson(): ?\Exerp\Person\StructType\Person
    {
        return $this->person;
    }
    /**
     * Set person value
     * @param \Exerp\Person\StructType\Person $person
     * @return \Exerp\Person\StructType\CreatePersonDetails
     */
    public function setPerson(?\Exerp\Person\StructType\Person $person = null): self
    {
        $this->person = $person;
        
        return $this;
    }
    /**
     * Get personCommunication value
     * @return \Exerp\Person\StructType\PersonCommunication|null
     */
    public function getPersonCommunication(): ?\Exerp\Person\StructType\PersonCommunication
    {
        return $this->personCommunication;
    }
    /**
     * Set personCommunication value
     * @param \Exerp\Person\StructType\PersonCommunication $personCommunication
     * @return \Exerp\Person\StructType\CreatePersonDetails
     */
    public function setPersonCommunication(?\Exerp\Person\StructType\PersonCommunication $personCommunication = null): self
    {
        $this->personCommunication = $personCommunication;
        
        return $this;
    }
    /**
     * Get password value
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }
    /**
     * Set password value
     * @param string $password
     * @return \Exerp\Person\StructType\CreatePersonDetails
     */
    public function setPassword(?string $password = null): self
    {
        // validation for constraint: string
        if (!is_null($password) && !is_string($password)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($password, true), gettype($password)), __LINE__);
        }
        $this->password = $password;
        
        return $this;
    }
    /**
     * Get pincode value
     * @return string|null
     */
    public function getPincode(): ?string
    {
        return $this->pincode;
    }
    /**
     * Set pincode value
     * @param string $pincode
     * @return \Exerp\Person\StructType\CreatePersonDetails
     */
    public function setPincode(?string $pincode = null): self
    {
        // validation for constraint: string
        if (!is_null($pincode) && !is_string($pincode)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($pincode, true), gettype($pincode)), __LINE__);
        }
        $this->pincode = $pincode;
        
        return $this;
    }
    /**
     * Get extendedAttributes value
     * @return \Exerp\Person\StructType\ExtendedAttributes|null
     */
    public function getExtendedAttributes(): ?\Exerp\Person\StructType\ExtendedAttributes
    {
        return $this->extendedAttributes;
    }
    /**
     * Set extendedAttributes value
     * @param \Exerp\Person\StructType\ExtendedAttributes $extendedAttributes
     * @return \Exerp\Person\StructType\CreatePersonDetails
     */
    public function setExtendedAttributes(?\Exerp\Person\StructType\ExtendedAttributes $extendedAttributes = null): self
    {
        $this->extendedAttributes = $extendedAttributes;
        
        return $this;
    }
    /**
     * Get extendedAttributeMIMEs value
     * @return \Exerp\Person\StructType\ExtendedAttributeMIMEs|null
     */
    public function getExtendedAttributeMIMEs(): ?\Exerp\Person\StructType\ExtendedAttributeMIMEs
    {
        return $this->extendedAttributeMIMEs;
    }
    /**
     * Set extendedAttributeMIMEs value
     * @param \Exerp\Person\StructType\ExtendedAttributeMIMEs $extendedAttributeMIMEs
     * @return \Exerp\Person\StructType\CreatePersonDetails
     */
    public function setExtendedAttributeMIMEs(?\Exerp\Person\StructType\ExtendedAttributeMIMEs $extendedAttributeMIMEs = null): self
    {
        $this->extendedAttributeMIMEs = $extendedAttributeMIMEs;
        
        return $this;
    }
}
