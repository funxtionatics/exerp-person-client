<?php

declare(strict_types=1);

namespace Exerp\Person\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for createLeadParameters StructType
 * @subpackage Structs
 */
class CreateLeadParameters extends AbstractStructBase
{
    /**
     * The centerId
     * @var int|null
     */
    protected ?int $centerId = null;
    /**
     * The firstName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $firstName = null;
    /**
     * The middleName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $middleName = null;
    /**
     * The lastName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $lastName = null;
    /**
     * The nickName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $nickName = null;
    /**
     * The socialSecurityNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $socialSecurityNumber = null;
    /**
     * The homePhone
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $homePhone = null;
    /**
     * The mobilePhone
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $mobilePhone = null;
    /**
     * The email
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $email = null;
    /**
     * The birthday
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $birthday = null;
    /**
     * The gender
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $gender = null;
    /**
     * The personType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $personType = null;
    /**
     * The comment
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $comment = null;
    /**
     * The employeeNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $employeeNumber = null;
    /**
     * The nationalID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $nationalID = null;
    /**
     * The residentID
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $residentID = null;
    /**
     * The passport
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\Passport|null
     */
    protected ?\Exerp\Person\StructType\Passport $passport = null;
    /**
     * The extendedAttributes
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\ExtendedAttributes|null
     */
    protected ?\Exerp\Person\StructType\ExtendedAttributes $extendedAttributes = null;
    /**
     * The relatedPersonId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\CompositeKey|null
     */
    protected ?\Exerp\Person\StructType\CompositeKey $relatedPersonId = null;
    /**
     * The address
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Person\StructType\Address|null
     */
    protected ?\Exerp\Person\StructType\Address $address = null;
    /**
     * Constructor method for createLeadParameters
     * @uses CreateLeadParameters::setCenterId()
     * @uses CreateLeadParameters::setFirstName()
     * @uses CreateLeadParameters::setMiddleName()
     * @uses CreateLeadParameters::setLastName()
     * @uses CreateLeadParameters::setNickName()
     * @uses CreateLeadParameters::setSocialSecurityNumber()
     * @uses CreateLeadParameters::setHomePhone()
     * @uses CreateLeadParameters::setMobilePhone()
     * @uses CreateLeadParameters::setEmail()
     * @uses CreateLeadParameters::setBirthday()
     * @uses CreateLeadParameters::setGender()
     * @uses CreateLeadParameters::setPersonType()
     * @uses CreateLeadParameters::setComment()
     * @uses CreateLeadParameters::setEmployeeNumber()
     * @uses CreateLeadParameters::setNationalID()
     * @uses CreateLeadParameters::setResidentID()
     * @uses CreateLeadParameters::setPassport()
     * @uses CreateLeadParameters::setExtendedAttributes()
     * @uses CreateLeadParameters::setRelatedPersonId()
     * @uses CreateLeadParameters::setAddress()
     * @param int $centerId
     * @param string $firstName
     * @param string $middleName
     * @param string $lastName
     * @param string $nickName
     * @param string $socialSecurityNumber
     * @param string $homePhone
     * @param string $mobilePhone
     * @param string $email
     * @param string $birthday
     * @param string $gender
     * @param string $personType
     * @param string $comment
     * @param string $employeeNumber
     * @param string $nationalID
     * @param string $residentID
     * @param \Exerp\Person\StructType\Passport $passport
     * @param \Exerp\Person\StructType\ExtendedAttributes $extendedAttributes
     * @param \Exerp\Person\StructType\CompositeKey $relatedPersonId
     * @param \Exerp\Person\StructType\Address $address
     */
    public function __construct(?int $centerId = null, ?string $firstName = null, ?string $middleName = null, ?string $lastName = null, ?string $nickName = null, ?string $socialSecurityNumber = null, ?string $homePhone = null, ?string $mobilePhone = null, ?string $email = null, ?string $birthday = null, ?string $gender = null, ?string $personType = null, ?string $comment = null, ?string $employeeNumber = null, ?string $nationalID = null, ?string $residentID = null, ?\Exerp\Person\StructType\Passport $passport = null, ?\Exerp\Person\StructType\ExtendedAttributes $extendedAttributes = null, ?\Exerp\Person\StructType\CompositeKey $relatedPersonId = null, ?\Exerp\Person\StructType\Address $address = null)
    {
        $this
            ->setCenterId($centerId)
            ->setFirstName($firstName)
            ->setMiddleName($middleName)
            ->setLastName($lastName)
            ->setNickName($nickName)
            ->setSocialSecurityNumber($socialSecurityNumber)
            ->setHomePhone($homePhone)
            ->setMobilePhone($mobilePhone)
            ->setEmail($email)
            ->setBirthday($birthday)
            ->setGender($gender)
            ->setPersonType($personType)
            ->setComment($comment)
            ->setEmployeeNumber($employeeNumber)
            ->setNationalID($nationalID)
            ->setResidentID($residentID)
            ->setPassport($passport)
            ->setExtendedAttributes($extendedAttributes)
            ->setRelatedPersonId($relatedPersonId)
            ->setAddress($address);
    }
    /**
     * Get centerId value
     * @return int|null
     */
    public function getCenterId(): ?int
    {
        return $this->centerId;
    }
    /**
     * Set centerId value
     * @param int $centerId
     * @return \Exerp\Person\StructType\CreateLeadParameters
     */
    public function setCenterId(?int $centerId = null): self
    {
        // validation for constraint: int
        if (!is_null($centerId) && !(is_int($centerId) || ctype_digit($centerId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($centerId, true), gettype($centerId)), __LINE__);
        }
        $this->centerId = $centerId;
        
        return $this;
    }
    /**
     * Get firstName value
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }
    /**
     * Set firstName value
     * @param string $firstName
     * @return \Exerp\Person\StructType\CreateLeadParameters
     */
    public function setFirstName(?string $firstName = null): self
    {
        // validation for constraint: string
        if (!is_null($firstName) && !is_string($firstName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($firstName, true), gettype($firstName)), __LINE__);
        }
        $this->firstName = $firstName;
        
        return $this;
    }
    /**
     * Get middleName value
     * @return string|null
     */
    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }
    /**
     * Set middleName value
     * @param string $middleName
     * @return \Exerp\Person\StructType\CreateLeadParameters
     */
    public function setMiddleName(?string $middleName = null): self
    {
        // validation for constraint: string
        if (!is_null($middleName) && !is_string($middleName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($middleName, true), gettype($middleName)), __LINE__);
        }
        $this->middleName = $middleName;
        
        return $this;
    }
    /**
     * Get lastName value
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }
    /**
     * Set lastName value
     * @param string $lastName
     * @return \Exerp\Person\StructType\CreateLeadParameters
     */
    public function setLastName(?string $lastName = null): self
    {
        // validation for constraint: string
        if (!is_null($lastName) && !is_string($lastName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($lastName, true), gettype($lastName)), __LINE__);
        }
        $this->lastName = $lastName;
        
        return $this;
    }
    /**
     * Get nickName value
     * @return string|null
     */
    public function getNickName(): ?string
    {
        return $this->nickName;
    }
    /**
     * Set nickName value
     * @param string $nickName
     * @return \Exerp\Person\StructType\CreateLeadParameters
     */
    public function setNickName(?string $nickName = null): self
    {
        // validation for constraint: string
        if (!is_null($nickName) && !is_string($nickName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($nickName, true), gettype($nickName)), __LINE__);
        }
        $this->nickName = $nickName;
        
        return $this;
    }
    /**
     * Get socialSecurityNumber value
     * @return string|null
     */
    public function getSocialSecurityNumber(): ?string
    {
        return $this->socialSecurityNumber;
    }
    /**
     * Set socialSecurityNumber value
     * @param string $socialSecurityNumber
     * @return \Exerp\Person\StructType\CreateLeadParameters
     */
    public function setSocialSecurityNumber(?string $socialSecurityNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($socialSecurityNumber) && !is_string($socialSecurityNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($socialSecurityNumber, true), gettype($socialSecurityNumber)), __LINE__);
        }
        $this->socialSecurityNumber = $socialSecurityNumber;
        
        return $this;
    }
    /**
     * Get homePhone value
     * @return string|null
     */
    public function getHomePhone(): ?string
    {
        return $this->homePhone;
    }
    /**
     * Set homePhone value
     * @param string $homePhone
     * @return \Exerp\Person\StructType\CreateLeadParameters
     */
    public function setHomePhone(?string $homePhone = null): self
    {
        // validation for constraint: string
        if (!is_null($homePhone) && !is_string($homePhone)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($homePhone, true), gettype($homePhone)), __LINE__);
        }
        $this->homePhone = $homePhone;
        
        return $this;
    }
    /**
     * Get mobilePhone value
     * @return string|null
     */
    public function getMobilePhone(): ?string
    {
        return $this->mobilePhone;
    }
    /**
     * Set mobilePhone value
     * @param string $mobilePhone
     * @return \Exerp\Person\StructType\CreateLeadParameters
     */
    public function setMobilePhone(?string $mobilePhone = null): self
    {
        // validation for constraint: string
        if (!is_null($mobilePhone) && !is_string($mobilePhone)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($mobilePhone, true), gettype($mobilePhone)), __LINE__);
        }
        $this->mobilePhone = $mobilePhone;
        
        return $this;
    }
    /**
     * Get email value
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }
    /**
     * Set email value
     * @param string $email
     * @return \Exerp\Person\StructType\CreateLeadParameters
     */
    public function setEmail(?string $email = null): self
    {
        // validation for constraint: string
        if (!is_null($email) && !is_string($email)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($email, true), gettype($email)), __LINE__);
        }
        $this->email = $email;
        
        return $this;
    }
    /**
     * Get birthday value
     * @return string|null
     */
    public function getBirthday(): ?string
    {
        return $this->birthday;
    }
    /**
     * Set birthday value
     * @param string $birthday
     * @return \Exerp\Person\StructType\CreateLeadParameters
     */
    public function setBirthday(?string $birthday = null): self
    {
        // validation for constraint: string
        if (!is_null($birthday) && !is_string($birthday)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($birthday, true), gettype($birthday)), __LINE__);
        }
        $this->birthday = $birthday;
        
        return $this;
    }
    /**
     * Get gender value
     * @return string|null
     */
    public function getGender(): ?string
    {
        return $this->gender;
    }
    /**
     * Set gender value
     * @uses \Exerp\Person\EnumType\ApiGender::valueIsValid()
     * @uses \Exerp\Person\EnumType\ApiGender::getValidValues()
     * @throws InvalidArgumentException
     * @param string $gender
     * @return \Exerp\Person\StructType\CreateLeadParameters
     */
    public function setGender(?string $gender = null): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Person\EnumType\ApiGender::valueIsValid($gender)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Person\EnumType\ApiGender', is_array($gender) ? implode(', ', $gender) : var_export($gender, true), implode(', ', \Exerp\Person\EnumType\ApiGender::getValidValues())), __LINE__);
        }
        $this->gender = $gender;
        
        return $this;
    }
    /**
     * Get personType value
     * @return string|null
     */
    public function getPersonType(): ?string
    {
        return $this->personType;
    }
    /**
     * Set personType value
     * @uses \Exerp\Person\EnumType\PersonType::valueIsValid()
     * @uses \Exerp\Person\EnumType\PersonType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $personType
     * @return \Exerp\Person\StructType\CreateLeadParameters
     */
    public function setPersonType(?string $personType = null): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Person\EnumType\PersonType::valueIsValid($personType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Person\EnumType\PersonType', is_array($personType) ? implode(', ', $personType) : var_export($personType, true), implode(', ', \Exerp\Person\EnumType\PersonType::getValidValues())), __LINE__);
        }
        $this->personType = $personType;
        
        return $this;
    }
    /**
     * Get comment value
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }
    /**
     * Set comment value
     * @param string $comment
     * @return \Exerp\Person\StructType\CreateLeadParameters
     */
    public function setComment(?string $comment = null): self
    {
        // validation for constraint: string
        if (!is_null($comment) && !is_string($comment)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($comment, true), gettype($comment)), __LINE__);
        }
        $this->comment = $comment;
        
        return $this;
    }
    /**
     * Get employeeNumber value
     * @return string|null
     */
    public function getEmployeeNumber(): ?string
    {
        return $this->employeeNumber;
    }
    /**
     * Set employeeNumber value
     * @param string $employeeNumber
     * @return \Exerp\Person\StructType\CreateLeadParameters
     */
    public function setEmployeeNumber(?string $employeeNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($employeeNumber) && !is_string($employeeNumber)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($employeeNumber, true), gettype($employeeNumber)), __LINE__);
        }
        $this->employeeNumber = $employeeNumber;
        
        return $this;
    }
    /**
     * Get nationalID value
     * @return string|null
     */
    public function getNationalID(): ?string
    {
        return $this->nationalID;
    }
    /**
     * Set nationalID value
     * @param string $nationalID
     * @return \Exerp\Person\StructType\CreateLeadParameters
     */
    public function setNationalID(?string $nationalID = null): self
    {
        // validation for constraint: string
        if (!is_null($nationalID) && !is_string($nationalID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($nationalID, true), gettype($nationalID)), __LINE__);
        }
        $this->nationalID = $nationalID;
        
        return $this;
    }
    /**
     * Get residentID value
     * @return string|null
     */
    public function getResidentID(): ?string
    {
        return $this->residentID;
    }
    /**
     * Set residentID value
     * @param string $residentID
     * @return \Exerp\Person\StructType\CreateLeadParameters
     */
    public function setResidentID(?string $residentID = null): self
    {
        // validation for constraint: string
        if (!is_null($residentID) && !is_string($residentID)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($residentID, true), gettype($residentID)), __LINE__);
        }
        $this->residentID = $residentID;
        
        return $this;
    }
    /**
     * Get passport value
     * @return \Exerp\Person\StructType\Passport|null
     */
    public function getPassport(): ?\Exerp\Person\StructType\Passport
    {
        return $this->passport;
    }
    /**
     * Set passport value
     * @param \Exerp\Person\StructType\Passport $passport
     * @return \Exerp\Person\StructType\CreateLeadParameters
     */
    public function setPassport(?\Exerp\Person\StructType\Passport $passport = null): self
    {
        $this->passport = $passport;
        
        return $this;
    }
    /**
     * Get extendedAttributes value
     * @return \Exerp\Person\StructType\ExtendedAttributes|null
     */
    public function getExtendedAttributes(): ?\Exerp\Person\StructType\ExtendedAttributes
    {
        return $this->extendedAttributes;
    }
    /**
     * Set extendedAttributes value
     * @param \Exerp\Person\StructType\ExtendedAttributes $extendedAttributes
     * @return \Exerp\Person\StructType\CreateLeadParameters
     */
    public function setExtendedAttributes(?\Exerp\Person\StructType\ExtendedAttributes $extendedAttributes = null): self
    {
        $this->extendedAttributes = $extendedAttributes;
        
        return $this;
    }
    /**
     * Get relatedPersonId value
     * @return \Exerp\Person\StructType\CompositeKey|null
     */
    public function getRelatedPersonId(): ?\Exerp\Person\StructType\CompositeKey
    {
        return $this->relatedPersonId;
    }
    /**
     * Set relatedPersonId value
     * @param \Exerp\Person\StructType\CompositeKey $relatedPersonId
     * @return \Exerp\Person\StructType\CreateLeadParameters
     */
    public function setRelatedPersonId(?\Exerp\Person\StructType\CompositeKey $relatedPersonId = null): self
    {
        $this->relatedPersonId = $relatedPersonId;
        
        return $this;
    }
    /**
     * Get address value
     * @return \Exerp\Person\StructType\Address|null
     */
    public function getAddress(): ?\Exerp\Person\StructType\Address
    {
        return $this->address;
    }
    /**
     * Set address value
     * @param \Exerp\Person\StructType\Address $address
     * @return \Exerp\Person\StructType\CreateLeadParameters
     */
    public function setAddress(?\Exerp\Person\StructType\Address $address = null): self
    {
        $this->address = $address;
        
        return $this;
    }
}
