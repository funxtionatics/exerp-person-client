<?php

declare(strict_types=1);

namespace Exerp\Person\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Find ServiceType
 * @subpackage Services
 */
class Find extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named findPersons
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\FindPerson $parameters
     * @return \Exerp\Person\ArrayType\PersonArray|bool
     */
    public function findPersons(\Exerp\Person\StructType\FindPerson $parameters)
    {
        try {
            $this->setResult($resultFindPersons = $this->getSoapClient()->__soapCall('findPersons', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultFindPersons;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \Exerp\Person\ArrayType\PersonArray
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
