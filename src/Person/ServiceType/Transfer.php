<?php

declare(strict_types=1);

namespace Exerp\Person\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Transfer ServiceType
 * @subpackage Services
 */
class Transfer extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named transferPerson
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\TransferPersonParameters $parameters
     * @return \Exerp\Person\StructType\TransferPersonInfo|bool
     */
    public function transferPerson(\Exerp\Person\StructType\TransferPersonParameters $parameters)
    {
        try {
            $this->setResult($resultTransferPerson = $this->getSoapClient()->__soapCall('transferPerson', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultTransferPerson;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \Exerp\Person\StructType\TransferPersonInfo
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
