<?php

declare(strict_types=1);

namespace Exerp\Person\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Set ServiceType
 * @subpackage Services
 */
class Set extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named setPersonReferrer
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\ApiPersonKey $personKey
     * @param \Exerp\Person\StructType\ApiPersonKey $referrerKey
     * @return void|bool
     */
    public function setPersonReferrer(\Exerp\Person\StructType\ApiPersonKey $personKey, \Exerp\Person\StructType\ApiPersonKey $referrerKey)
    {
        try {
            $this->setResult($resultSetPersonReferrer = $this->getSoapClient()->__soapCall('setPersonReferrer', [
                $personKey,
                $referrerKey,
            ], [], [], $this->outputHeaders));
        
            return $resultSetPersonReferrer;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return void
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
