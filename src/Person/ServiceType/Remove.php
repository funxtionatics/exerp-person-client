<?php

declare(strict_types=1);

namespace Exerp\Person\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Remove ServiceType
 * @subpackage Services
 */
class Remove extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named removePersonPreferredCenter
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\ApiPersonKey $personKey
     * @param string $centerKey
     * @return \Exerp\Person\StructType\PersonPreferredCentersResponse|bool
     */
    public function removePersonPreferredCenter(\Exerp\Person\StructType\ApiPersonKey $personKey, $centerKey)
    {
        try {
            $this->setResult($resultRemovePersonPreferredCenter = $this->getSoapClient()->__soapCall('removePersonPreferredCenter', [
                $personKey,
                $centerKey,
            ], [], [], $this->outputHeaders));
        
            return $resultRemovePersonPreferredCenter;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named removePersonReferrer
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\ApiPersonKey $personKey
     * @return void|bool
     */
    public function removePersonReferrer(\Exerp\Person\StructType\ApiPersonKey $personKey)
    {
        try {
            $this->setResult($resultRemovePersonReferrer = $this->getSoapClient()->__soapCall('removePersonReferrer', [
                $personKey,
            ], [], [], $this->outputHeaders));
        
            return $resultRemovePersonReferrer;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return void|\Exerp\Person\StructType\PersonPreferredCentersResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
