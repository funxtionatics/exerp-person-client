<?php

declare(strict_types=1);

namespace Exerp\Person\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Update ServiceType
 * @subpackage Services
 */
class Update extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named updateDetails
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\Person $person
     * @return \Exerp\Person\StructType\Person|bool
     */
    public function updateDetails(\Exerp\Person\StructType\Person $person)
    {
        try {
            $this->setResult($resultUpdateDetails = $this->getSoapClient()->__soapCall('updateDetails', [
                $person,
            ], [], [], $this->outputHeaders));
        
            return $resultUpdateDetails;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named updateExtendedAttributeMIME
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\ApiPersonKey $personKey
     * @param string $attributeName
     * @param \Exerp\Person\StructType\MimeDocument $value
     * @return void|bool
     */
    public function updateExtendedAttributeMIME(\Exerp\Person\StructType\ApiPersonKey $personKey, $attributeName, \Exerp\Person\StructType\MimeDocument $value)
    {
        try {
            $this->setResult($resultUpdateExtendedAttributeMIME = $this->getSoapClient()->__soapCall('updateExtendedAttributeMIME', [
                $personKey,
                $attributeName,
                $value,
            ], [], [], $this->outputHeaders));
        
            return $resultUpdateExtendedAttributeMIME;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named updateExtendedAttributeText
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\ApiPersonKey $personKey
     * @param string $attributeName
     * @param string $value
     * @return void|bool
     */
    public function updateExtendedAttributeText(\Exerp\Person\StructType\ApiPersonKey $personKey, $attributeName, $value)
    {
        try {
            $this->setResult($resultUpdateExtendedAttributeText = $this->getSoapClient()->__soapCall('updateExtendedAttributeText', [
                $personKey,
                $attributeName,
                $value,
            ], [], [], $this->outputHeaders));
        
            return $resultUpdateExtendedAttributeText;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named updateCommunicationDetails
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\PersonCommunication $personCommunication
     * @return void|bool
     */
    public function updateCommunicationDetails(\Exerp\Person\StructType\PersonCommunication $personCommunication)
    {
        try {
            $this->setResult($resultUpdateCommunicationDetails = $this->getSoapClient()->__soapCall('updateCommunicationDetails', [
                $personCommunication,
            ], [], [], $this->outputHeaders));
        
            return $resultUpdateCommunicationDetails;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return void|\Exerp\Person\StructType\Person
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
