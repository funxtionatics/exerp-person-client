<?php

declare(strict_types=1);

namespace Exerp\Person\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Get ServiceType
 * @subpackage Services
 */
class Get extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named getCommunicationDetails
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\ApiPersonKey $personKey
     * @return \Exerp\Person\StructType\PersonCommunication|bool
     */
    public function getCommunicationDetails(\Exerp\Person\StructType\ApiPersonKey $personKey)
    {
        try {
            $this->setResult($resultGetCommunicationDetails = $this->getSoapClient()->__soapCall('getCommunicationDetails', [
                $personKey,
            ], [], [], $this->outputHeaders));
        
            return $resultGetCommunicationDetails;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named getPersonPreferredCenters
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\ApiPersonKey $personKey
     * @return \Exerp\Person\StructType\PersonPreferredCentersResponse|bool
     */
    public function getPersonPreferredCenters(\Exerp\Person\StructType\ApiPersonKey $personKey)
    {
        try {
            $this->setResult($resultGetPersonPreferredCenters = $this->getSoapClient()->__soapCall('getPersonPreferredCenters', [
                $personKey,
            ], [], [], $this->outputHeaders));
        
            return $resultGetPersonPreferredCenters;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named getPersonRelations
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\PersonRelationParameters $personRelation
     * @return \Exerp\Person\ArrayType\RelatedPersonArray|bool
     */
    public function getPersonRelations(\Exerp\Person\StructType\PersonRelationParameters $personRelation)
    {
        try {
            $this->setResult($resultGetPersonRelations = $this->getSoapClient()->__soapCall('getPersonRelations', [
                $personRelation,
            ], [], [], $this->outputHeaders));
        
            return $resultGetPersonRelations;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named getTypeAndStatus
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\ApiPersonKey $personKey
     * @return \Exerp\Person\StructType\PersonTypeAndStatus|bool
     */
    public function getTypeAndStatus(\Exerp\Person\StructType\ApiPersonKey $personKey)
    {
        try {
            $this->setResult($resultGetTypeAndStatus = $this->getSoapClient()->__soapCall('getTypeAndStatus', [
                $personKey,
            ], [], [], $this->outputHeaders));
        
            return $resultGetTypeAndStatus;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named getUnsignedJournalNoteDocuments
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\ApiPersonKey $personKey
     * @return \Exerp\Person\StructType\JournalNoteResponse|bool
     */
    public function getUnsignedJournalNoteDocuments(\Exerp\Person\StructType\ApiPersonKey $personKey)
    {
        try {
            $this->setResult($resultGetUnsignedJournalNoteDocuments = $this->getSoapClient()->__soapCall('getUnsignedJournalNoteDocuments', [
                $personKey,
            ], [], [], $this->outputHeaders));
        
            return $resultGetUnsignedJournalNoteDocuments;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named getPassportCountries
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @return \Exerp\Person\ArrayType\PassportCountryArray|bool
     */
    public function getPassportCountries()
    {
        try {
            $this->setResult($resultGetPassportCountries = $this->getSoapClient()->__soapCall('getPassportCountries', [], [], [], $this->outputHeaders));
        
            return $resultGetPassportCountries;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named getPersonDetail
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\ApiPersonKey $personKey
     * @return \Exerp\Person\StructType\PersonDetail|bool
     */
    public function getPersonDetail(\Exerp\Person\StructType\ApiPersonKey $personKey)
    {
        try {
            $this->setResult($resultGetPersonDetail = $this->getSoapClient()->__soapCall('getPersonDetail', [
                $personKey,
            ], [], [], $this->outputHeaders));
        
            return $resultGetPersonDetail;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named getAvailableLanguagesForMembers
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\GetAvailableLanguagesForMembersParameters $getAvailableLanguagesForMembersParameters
     * @return \Exerp\Person\StructType\AvailableLanguagesForMembersResponse|bool
     */
    public function getAvailableLanguagesForMembers(\Exerp\Person\StructType\GetAvailableLanguagesForMembersParameters $getAvailableLanguagesForMembersParameters)
    {
        try {
            $this->setResult($resultGetAvailableLanguagesForMembers = $this->getSoapClient()->__soapCall('getAvailableLanguagesForMembers', [
                $getAvailableLanguagesForMembersParameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetAvailableLanguagesForMembers;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named getExtendedAttributeText
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\ApiPersonKey $personKey
     * @param string $attributeName
     * @return string|bool
     */
    public function getExtendedAttributeText(\Exerp\Person\StructType\ApiPersonKey $personKey, $attributeName)
    {
        try {
            $this->setResult($resultGetExtendedAttributeText = $this->getSoapClient()->__soapCall('getExtendedAttributeText', [
                $personKey,
                $attributeName,
            ], [], [], $this->outputHeaders));
        
            return $resultGetExtendedAttributeText;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named getAvailableSalutations
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\GetAvailableSalutationsParameters $getAvailableSalutationsParameters
     * @return \Exerp\Person\StructType\AvailableSalutationsResponse|bool
     */
    public function getAvailableSalutations(\Exerp\Person\StructType\GetAvailableSalutationsParameters $getAvailableSalutationsParameters)
    {
        try {
            $this->setResult($resultGetAvailableSalutations = $this->getSoapClient()->__soapCall('getAvailableSalutations', [
                $getAvailableSalutationsParameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetAvailableSalutations;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named getPersonDetailByLogin
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param string $email
     * @param string $password
     * @return \Exerp\Person\StructType\PersonDetail|bool
     */
    public function getPersonDetailByLogin($email, $password)
    {
        try {
            $this->setResult($resultGetPersonDetailByLogin = $this->getSoapClient()->__soapCall('getPersonDetailByLogin', [
                $email,
                $password,
            ], [], [], $this->outputHeaders));
        
            return $resultGetPersonDetailByLogin;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named getJournalNotes
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\JournalNoteParameters $parameters
     * @return \Exerp\Person\ArrayType\JournalNoteArray|bool
     */
    public function getJournalNotes(\Exerp\Person\StructType\JournalNoteParameters $parameters)
    {
        try {
            $this->setResult($resultGetJournalNotes = $this->getSoapClient()->__soapCall('getJournalNotes', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetJournalNotes;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named getScope
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param string $scopeType
     * @param string $scopeId
     * @return \Exerp\Person\StructType\Scope|bool
     */
    public function getScope($scopeType, $scopeId)
    {
        try {
            $this->setResult($resultGetScope = $this->getSoapClient()->__soapCall('getScope', [
                $scopeType,
                $scopeId,
            ], [], [], $this->outputHeaders));
        
            return $resultGetScope;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named getJournalNoteDocument
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param string $journalNoteId
     * @return \Exerp\Person\StructType\JournalNoteDocument|bool
     */
    public function getJournalNoteDocument($journalNoteId)
    {
        try {
            $this->setResult($resultGetJournalNoteDocument = $this->getSoapClient()->__soapCall('getJournalNoteDocument', [
                $journalNoteId,
            ], [], [], $this->outputHeaders));
        
            return $resultGetJournalNoteDocument;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named getCurrentPersonId
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\ApiPersonKey $personKey
     * @return \Exerp\Person\StructType\ApiPersonKey|bool
     */
    public function getCurrentPersonId(\Exerp\Person\StructType\ApiPersonKey $personKey)
    {
        try {
            $this->setResult($resultGetCurrentPersonId = $this->getSoapClient()->__soapCall('getCurrentPersonId', [
                $personKey,
            ], [], [], $this->outputHeaders));
        
            return $resultGetCurrentPersonId;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named getCitiesForZipcode
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param string $countryId
     * @param string $zipCode
     * @return \Exerp\Person\ArrayType\StringArray|bool
     */
    public function getCitiesForZipcode($countryId, $zipCode)
    {
        try {
            $this->setResult($resultGetCitiesForZipcode = $this->getSoapClient()->__soapCall('getCitiesForZipcode', [
                $countryId,
                $zipCode,
            ], [], [], $this->outputHeaders));
        
            return $resultGetCitiesForZipcode;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named getDetails
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\ApiPersonKey $personKey
     * @return \Exerp\Person\StructType\Person|bool
     */
    public function getDetails(\Exerp\Person\StructType\ApiPersonKey $personKey)
    {
        try {
            $this->setResult($resultGetDetails = $this->getSoapClient()->__soapCall('getDetails', [
                $personKey,
            ], [], [], $this->outputHeaders));
        
            return $resultGetDetails;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named getExtendedAttributeMIME
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\ApiPersonKey $personKey
     * @param string $attributeName
     * @return \Exerp\Person\StructType\MimeDocument|bool
     */
    public function getExtendedAttributeMIME(\Exerp\Person\StructType\ApiPersonKey $personKey, $attributeName)
    {
        try {
            $this->setResult($resultGetExtendedAttributeMIME = $this->getSoapClient()->__soapCall('getExtendedAttributeMIME', [
                $personKey,
                $attributeName,
            ], [], [], $this->outputHeaders));
        
            return $resultGetExtendedAttributeMIME;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return string|\Exerp\Person\ArrayType\JournalNoteArray|\Exerp\Person\ArrayType\PassportCountryArray|\Exerp\Person\ArrayType\RelatedPersonArray|\Exerp\Person\ArrayType\StringArray|\Exerp\Person\StructType\ApiPersonKey|\Exerp\Person\StructType\AvailableLanguagesForMembersResponse|\Exerp\Person\StructType\AvailableSalutationsResponse|\Exerp\Person\StructType\JournalNoteDocument|\Exerp\Person\StructType\JournalNoteResponse|\Exerp\Person\StructType\MimeDocument|\Exerp\Person\StructType\Person|\Exerp\Person\StructType\PersonCommunication|\Exerp\Person\StructType\PersonDetail|\Exerp\Person\StructType\PersonPreferredCentersResponse|\Exerp\Person\StructType\PersonTypeAndStatus|\Exerp\Person\StructType\Scope
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
