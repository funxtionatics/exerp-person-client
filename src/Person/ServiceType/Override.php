<?php

declare(strict_types=1);

namespace Exerp\Person\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Override ServiceType
 * @subpackage Services
 */
class Override extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named
     * overridePartnerBenefitActivationAuthorizationCode
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\OverridePartnerBenefitActivationAuthorizationCodeParameters $parameters
     * @return void|bool
     */
    public function overridePartnerBenefitActivationAuthorizationCode(\Exerp\Person\StructType\OverridePartnerBenefitActivationAuthorizationCodeParameters $parameters)
    {
        try {
            $this->setResult($resultOverridePartnerBenefitActivationAuthorizationCode = $this->getSoapClient()->__soapCall('overridePartnerBenefitActivationAuthorizationCode', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultOverridePartnerBenefitActivationAuthorizationCode;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return void
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
