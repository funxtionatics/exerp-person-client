<?php

declare(strict_types=1);

namespace Exerp\Person\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Delete ServiceType
 * @subpackage Services
 */
class Delete extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named deleteExtendedAttribute
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\ApiPersonKey $personKey
     * @param string $attributeName
     * @return boolean|bool
     */
    public function deleteExtendedAttribute(\Exerp\Person\StructType\ApiPersonKey $personKey, $attributeName)
    {
        try {
            $this->setResult($resultDeleteExtendedAttribute = $this->getSoapClient()->__soapCall('deleteExtendedAttribute', [
                $personKey,
                $attributeName,
            ], [], [], $this->outputHeaders));
        
            return $resultDeleteExtendedAttribute;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return boolean
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
