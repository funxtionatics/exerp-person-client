<?php

declare(strict_types=1);

namespace Exerp\Person\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Add ServiceType
 * @subpackage Services
 */
class Add extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named addSignedDocumentToJournalNote
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\AddSignedDocumentToJournalNoteParameters $parameters
     * @return void|bool
     */
    public function addSignedDocumentToJournalNote(\Exerp\Person\StructType\AddSignedDocumentToJournalNoteParameters $parameters)
    {
        try {
            $this->setResult($resultAddSignedDocumentToJournalNote = $this->getSoapClient()->__soapCall('addSignedDocumentToJournalNote', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultAddSignedDocumentToJournalNote;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named addPersonPreferredCenter
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\ApiPersonKey $personKey
     * @param string $centerKey
     * @return \Exerp\Person\StructType\PersonPreferredCentersResponse|bool
     */
    public function addPersonPreferredCenter(\Exerp\Person\StructType\ApiPersonKey $personKey, $centerKey)
    {
        try {
            $this->setResult($resultAddPersonPreferredCenter = $this->getSoapClient()->__soapCall('addPersonPreferredCenter', [
                $personKey,
                $centerKey,
            ], [], [], $this->outputHeaders));
        
            return $resultAddPersonPreferredCenter;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return void|\Exerp\Person\StructType\PersonPreferredCentersResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
