<?php

declare(strict_types=1);

namespace Exerp\Person\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Change ServiceType
 * @subpackage Services
 */
class Change extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named changePasswordWithToken
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\ApiPersonKey $personKey
     * @param string $newPassword
     * @param string $token
     * @return void|bool
     */
    public function changePasswordWithToken(\Exerp\Person\StructType\ApiPersonKey $personKey, $newPassword, $token)
    {
        try {
            $this->setResult($resultChangePasswordWithToken = $this->getSoapClient()->__soapCall('changePasswordWithToken', [
                $personKey,
                $newPassword,
                $token,
            ], [], [], $this->outputHeaders));
        
            return $resultChangePasswordWithToken;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named changeSuspensionStatus
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\ChangeSuspensionStatusParameters $parameters
     * @return void|bool
     */
    public function changeSuspensionStatus(\Exerp\Person\StructType\ChangeSuspensionStatusParameters $parameters)
    {
        try {
            $this->setResult($resultChangeSuspensionStatus = $this->getSoapClient()->__soapCall('changeSuspensionStatus', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultChangeSuspensionStatus;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named changePersonType
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\ChangePersonTypeParameter $parameters
     * @return void|bool
     */
    public function changePersonType(\Exerp\Person\StructType\ChangePersonTypeParameter $parameters)
    {
        try {
            $this->setResult($resultChangePersonType = $this->getSoapClient()->__soapCall('changePersonType', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultChangePersonType;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return void
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
