<?php

declare(strict_types=1);

namespace Exerp\Person\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Send ServiceType
 * @subpackage Services
 */
class Send extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named sendPasswordToken
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\ApiPersonKey $personKey
     * @return void|bool
     */
    public function sendPasswordToken(\Exerp\Person\StructType\ApiPersonKey $personKey)
    {
        try {
            $this->setResult($resultSendPasswordToken = $this->getSoapClient()->__soapCall('sendPasswordToken', [
                $personKey,
            ], [], [], $this->outputHeaders));
        
            return $resultSendPasswordToken;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named sendPassword
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\ApiPersonKey $personKey
     * @return void|bool
     */
    public function sendPassword(\Exerp\Person\StructType\ApiPersonKey $personKey)
    {
        try {
            $this->setResult($resultSendPassword = $this->getSoapClient()->__soapCall('sendPassword', [
                $personKey,
            ], [], [], $this->outputHeaders));
        
            return $resultSendPassword;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return void
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
