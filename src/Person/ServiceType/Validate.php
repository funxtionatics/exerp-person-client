<?php

declare(strict_types=1);

namespace Exerp\Person\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Validate ServiceType
 * @subpackage Services
 */
class Validate extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named validatePhoneNumber
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param string $countryId
     * @param string $phoneNumber
     * @return void|bool
     */
    public function validatePhoneNumber($countryId, $phoneNumber)
    {
        try {
            $this->setResult($resultValidatePhoneNumber = $this->getSoapClient()->__soapCall('validatePhoneNumber', [
                $countryId,
                $phoneNumber,
            ], [], [], $this->outputHeaders));
        
            return $resultValidatePhoneNumber;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named validatePerson
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param string $centerId
     * @param \Exerp\Person\StructType\Person $person
     * @return \Exerp\Person\StructType\ValidationResult|bool
     */
    public function validatePerson($centerId, \Exerp\Person\StructType\Person $person)
    {
        try {
            $this->setResult($resultValidatePerson = $this->getSoapClient()->__soapCall('validatePerson', [
                $centerId,
                $person,
            ], [], [], $this->outputHeaders));
        
            return $resultValidatePerson;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return void|\Exerp\Person\StructType\ValidationResult
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
