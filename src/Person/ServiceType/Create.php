<?php

declare(strict_types=1);

namespace Exerp\Person\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Create ServiceType
 * @subpackage Services
 */
class Create extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named createToDo
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\CreateToDoParameter $parameters
     * @return \Exerp\Person\StructType\ToDo|bool
     */
    public function createToDo(\Exerp\Person\StructType\CreateToDoParameter $parameters)
    {
        try {
            $this->setResult($resultCreateToDo = $this->getSoapClient()->__soapCall('createToDo', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultCreateToDo;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named createCustomJournalDocument
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\CreateCustomJournalDocumentParameters $parameters
     * @return \Exerp\Person\StructType\CreateCustomJournalDocumentResponse|bool
     */
    public function createCustomJournalDocument(\Exerp\Person\StructType\CreateCustomJournalDocumentParameters $parameters)
    {
        try {
            $this->setResult($resultCreateCustomJournalDocument = $this->getSoapClient()->__soapCall('createCustomJournalDocument', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultCreateCustomJournalDocument;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named createDoctorNote
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\ApiPersonKey $personKey
     * @param \Exerp\Person\StructType\JournalNote $journalNote
     * @return void|bool
     */
    public function createDoctorNote(\Exerp\Person\StructType\ApiPersonKey $personKey, \Exerp\Person\StructType\JournalNote $journalNote)
    {
        try {
            $this->setResult($resultCreateDoctorNote = $this->getSoapClient()->__soapCall('createDoctorNote', [
                $personKey,
                $journalNote,
            ], [], [], $this->outputHeaders));
        
            return $resultCreateDoctorNote;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named createLead
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\CreateLeadParameters $createLeadParameters
     * @return \Exerp\Person\StructType\Person|bool
     */
    public function createLead(\Exerp\Person\StructType\CreateLeadParameters $createLeadParameters)
    {
        try {
            $this->setResult($resultCreateLead = $this->getSoapClient()->__soapCall('createLead', [
                $createLeadParameters,
            ], [], [], $this->outputHeaders));
        
            return $resultCreateLead;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named createJournalNote
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Person\StructType\ApiPersonKey $personKey
     * @param \Exerp\Person\StructType\JournalNote $journalNote
     * @return void|bool
     */
    public function createJournalNote(\Exerp\Person\StructType\ApiPersonKey $personKey, \Exerp\Person\StructType\JournalNote $journalNote)
    {
        try {
            $this->setResult($resultCreateJournalNote = $this->getSoapClient()->__soapCall('createJournalNote', [
                $personKey,
                $journalNote,
            ], [], [], $this->outputHeaders));
        
            return $resultCreateJournalNote;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named createPerson
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param string $centerId
     * @param \Exerp\Person\StructType\CreatePersonDetails $fullPersonCommunication
     * @return \Exerp\Person\StructType\CreatePersonDetails|bool
     */
    public function createPerson($centerId, \Exerp\Person\StructType\CreatePersonDetails $fullPersonCommunication)
    {
        try {
            $this->setResult($resultCreatePerson = $this->getSoapClient()->__soapCall('createPerson', [
                $centerId,
                $fullPersonCommunication,
            ], [], [], $this->outputHeaders));
        
            return $resultCreatePerson;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return void|\Exerp\Person\StructType\CreateCustomJournalDocumentResponse|\Exerp\Person\StructType\CreatePersonDetails|\Exerp\Person\StructType\Person|\Exerp\Person\StructType\ToDo
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
